#include<stdio.h>
#include<stdlib.h>

typedef struct dnode
{
  int data;
  struct dnode *next;
  struct dnode *prev;
}dnode;

void display(dnode * , dnode *);
void insert_beg(dnode ** , dnode ** , int);
void insert_end(dnode ** , dnode ** , int);
void insert_p(dnode ** , dnode ** , int);
void delete_beg(dnode ** , dnode **);
void delete_end(dnode ** , dnode **);
void delete_p(dnode ** , dnode ** , int);
void freelist(dnode ** , dnode **);

int main()
{
  dnode *head=NULL;
  dnode *tail=NULL;
  int ch,item;
  while(1)
  {
	printf("*****MENU*****\n1.Insert at beg\n2.Insert at end\n3.Insert at proper place\n4.Delete from beg\n5.Delete from end\n6.Delete particular\n7.Display the list\n8.Free the list\n9.Exit\n");
	scanf("%d",&ch);
	switch(ch)
	{
		case 1:
		printf("Enter the item to be added : ");
		scanf("%d",&item);
		insert_beg(&head , &tail , item);
		break;
		case 2:
		printf("Enter the item to be added : ");
		scanf("%d",&item);
		insert_end(&head , &tail , item);
		break;
		case 3:
		printf("Enter the item to be added : ");
		scanf("%d",&item);
		insert_p(&head , &tail , item);
		break;
		case 4:
		delete_beg(&head , &tail);
		break;
		case 5:
		delete_end(&head , &tail);
		break;
		case 6:
		printf("Enter the item to be deleted : ");
		scanf("%d",&item);
		delete_p(&head , &tail , item);
		break;
		case 7:
		display(head , tail);
		break;
		case 8:
		freelist(&head , &tail);
		break;
		case 9:
		exit(0);
		default:
		printf("Wrong option\n");
		break;
	}
  }
  return 0;
}

void display(dnode *head , dnode *tail)
{
  if(NULL==head)
  {
	printf("List empty\n");
	return ;
  }
  dnode *p=head;
  printf("The list is :\n");
  while(p!=NULL)
  {
	printf("<=> %d ",p->data);
	p=p->next;
  }
  printf("\n");
}

void insert_beg(dnode **head , dnode **tail , int item)
{
  dnode *new=(dnode *)malloc(sizeof(dnode));
  if(NULL==new)
  {
	printf("Node not created, memory not available\n");
	return ;
  }
  new->data=item;
  new->prev=NULL;
  if(NULL==(*head))
  {
	new->next=NULL;
	(*tail)=new;
  }
  else
  {
	new->next=(*head);
	(*head)->prev=new;
  }
  (*head)=new;
  printf("Item added successfully at beg\n");
}

void insert_end(dnode **head , dnode **tail , int item)
{
  dnode *new=(dnode *)malloc(sizeof(dnode));
  if(NULL==new)
  {
        printf("Node not created, memory not available\n");
        return ;
  }
  new->data=item;
  new->next=NULL;
  if(NULL==(*head))
  {
	(*head)=new;
	new->prev=NULL;
  }
  else
  {
	(*tail)->next=new;
	new->prev=(*tail);
  }
  (*tail)=new;
  printf("Item added successfully at end\n");
}

void insert_p(dnode **head , dnode **tail , int item)
{
  dnode *new=(dnode *)malloc(sizeof(dnode));
  if(NULL==new)
  {
        printf("Node not created, memory not available\n");
        return ;
  }
  new->data=item;
  if(NULL==(*head))
  {
	new->next=NULL;
	new->prev=NULL;
	(*head)=new;
	(*tail)=new;
	printf("Item added successfully at proper place\n");
	return ;
  }
  if(item<=(*head)->data)
  {
	insert_beg(head , tail , item);
	return ;
  }
  if(item>=(*tail)->data)
  {
        insert_end(head , tail , item);
        return ;
  }
  dnode *p=(*head);
  while(p->data<=item&&p->next!=NULL)
  {
	p=p->next;
  }
  dnode *q=p->prev;
  new->next=p;
  new->prev=q;
  q->next=new;
  p->prev=new;
  printf("Item added successfully at proper place\n");
}

void delete_beg(dnode **head , dnode **tail)
{
  int i;
  if(NULL==(*head))
  {
	printf("Cant delete, list empty\n");
	return;
  }
  if((*head)==(*tail))
  {
	i=(*head)->data;
	free(*head);
	(*head)=NULL;
	(*tail)=NULL;
	printf("%d deleted successfully from beg\n",i);
	return;
  }
  i=(*head)->data;
  dnode *p=(*head);
  (*head)=p->next;
  (*head)->prev=NULL;
  free(p);
  p=NULL;
  printf("%d deleted successfully from beg\n",i);
}

void delete_end(dnode **head , dnode **tail)
{
  int i;
  if(NULL==(*head))
  {
	printf("Cant delete, list empty\n");
	return;
  }
  if((*head)==(*tail))
  {
	i=(*head)->data;
	free(*head);
	(*head)=NULL;
	(*tail)=NULL;
	printf("%d deleted successfully from end\n",i);
	return;
  }
  i=(*tail)->data;
  dnode *p=(*tail);
  (*tail)=p->prev;
  (*tail)->next=NULL;
  free(p);
  p=NULL;
  printf("%d deleted successfully from end\n",i);
}

void delete_p(dnode **head , dnode ** tail , int item)
{
  if(NULL==(*head))
  {
	printf("Cant delete, list empty\n");
	return;
  }
  if((*head)->data==item)
  {
	delete_beg(head,tail);
	return ;
  }
  if((*tail)->data==item)
  {
        delete_end(head,tail);
        return ;
  }
  dnode *p=(*head)->next;
  while(p->next!=NULL)
  {
	if(p->data==item)
	{
		dnode *q=p->prev , *r=p->next;
		q->next=r;
		r->prev=q;
		free(p);
		p=NULL;
		printf("%d deleted successfully\n",item);
		return;
	}
	p=p->next;
  }
  printf("Item not found\n");
  return;
}

void freelist(dnode **head , dnode **tail)
{
  if(NULL==(*head))
  {
	printf("List already empty\n");
	return ;
  }
  if((*head)==(*tail))
  {
	free(*head);
	*head=NULL;
	*tail=NULL;
	printf("List freed\n");
	return ;
  }
  dnode *p=(*head);
  while(p!=NULL)
  {
	(*head)=(*head)->next;
	free(p);
	p=NULL;
	p=(*head);
  }
  (*tail)=NULL;
  printf("List freed\n");
}
