#include<stdio.h>
void main()
{
	char ch='y',inp;
	system("clear");
	printf(" Vowel Constant Checker ");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\n\t Enter the Letter need to be checked : ");
		scanf(" %c",&inp);

		switch(inp)
		{
			case 'A':
			case 'E':
			case 'I':
			case 'O':
			case 'U':
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				printf("\n\t %c is a vowel.",inp);
				break;
			default:
				printf("\n\t %c is Constant.",inp);
		}

		printf("\n\n\t Want to check more ? (y/n) : ");
		scanf(" %c",&ch);
	}
	
	printf("\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
	
}

/*	Output

Vowel Constant Checker 


	 Enter the Letter need to be checked : a

	 a is a vowel.

	 Want to check more ? (y/n) : y



	 Enter the Letter need to be checked : e

	 e is a vowel.

	 Want to check more ? (y/n) : y



	 Enter the Letter need to be checked : i

	 i is a vowel.

	 Want to check more ? (y/n) : y



	 Enter the Letter need to be checked : n

	 n is Constant.

	 Want to check more ? (y/n) : n

 All rights are reserverd. Copyright © Abhilash Kant.

*/
