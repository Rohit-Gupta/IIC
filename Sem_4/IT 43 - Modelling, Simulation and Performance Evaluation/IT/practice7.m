% To solve the system
% dx1/dt=x2
% dx2/dt=-x1-0.2*x2
% with Euler’s method
clear all
% Clear all variables
nn=1000;
% Number of time steps
h=0.05;
% Time step
x1(1)=1.0;
% Initial x1
x2(1)=0.0;
% Initial x2
t(1)=0;
% Initial t
for n=1:nn-1
% Time loop
fun1=x2(n);
% Get RHS of 1st eq at old time
fun2=-x1(n)-0.2*x2(n);
% Get RHS of 2nd eq at old time
x1(n+1)=x1(n)+h*fun1;
% Get new x1
x2(n+1)=x2(n)+h*fun2;
% Get new x2
t(n+1)=t(n)+h;
% get new t
end
plot(t,x1)
% Plot x1 vs t
title('CFB'); xlabel('t'), ylabel('x1')