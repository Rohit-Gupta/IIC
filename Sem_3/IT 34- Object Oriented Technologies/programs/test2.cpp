#include <iostream>
using namespace std;

class result
{
	char name[30];
	int roll_no;
	float marks[5];
	float per;
	char grade;
public:
	void putdata();
	void getdata();
};
void result::putdata()
{
	float sum = 0.0;
	cout<<"  Enter name : ";
	cin>>name;
	cout<<"  Enter roll no : ";
	cin>>roll_no;
	cout<<"  Enter marks of five subjects : "<<endl;
	for(int i = 0; i < 5; i++)
	{
		cout<<"  Subject "<<i+1<<" = ";
		cin>>marks[i];
		sum = sum + marks[i];
	}
	per = sum/5;
	if(per<100 && per>=90)
	{
		grade = 'A';
	}
	else if(per<90 && per>=75)
	{
			grade = 'B';
	}
	else if(per<75 && per>=60)
	{
		grade = 'C';
	}
	else if(per<50 && per>=60)
	{
		grade = 'D';
	}
	else if(per<50)
	{
		grade = 'F';
	}
}
void result::getdata()
{
	cout<<"  Name of the student : "<<name<<endl;
	cout<<"  Roll No. of student : "<<roll_no<<endl;
	cout<<"  Marks : "<<endl;
	for(int i = 0; i < 5; i++ )
	{
		cout<<"   Subject "<<i+1<<" = "<<marks[i]<<endl;
	}
	cout<<"  Percentage : "<<per<<endl<<endl;
	cout<<"  Grade : "<<grade<<endl<<endl;
}

int main()
{
	int n;
	cout<<"  Enter the number of student : ";
	cin>>n;
	result r[n];
	for( int i = 0; i < n; i++ )
	{
		cout<<"  Student : "<<i+1<<endl;
		r[i].putdata();
	}
	for( int i = 0; i < n; i++ )
	{
		cout<<"  Detail of Student : "<<i+1<<endl<<endl;
		r[i].getdata();
	}
	return 0;
}