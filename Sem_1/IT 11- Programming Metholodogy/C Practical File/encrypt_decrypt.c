/*	Q. Write functions to encrypt text and to decrypt the encrypted text.
	 Perform the encryption so that every character is replicated by next character. For example
	a by b, b by c and so on, replace z by a.
	 Perform decryption for above case
	Example:
	Entered text: program
	Encrypted text: qsphsbn
	Decrypted text: program		*/

#include<stdio.h>
#include<stdlib.h>

void encrypt(char *str)
{
	int i=0;
	while(str[i]!='\n')
	{
		if(str[i]=='z')
			str[i]='a',i++;
		else
			str[i]+=1,i++;
	}
}

void decrypt(char *str)
{
	int i=0;
	while(str[i]!='\n')
	{
		if(str[i]=='a')
			str[i]='z',i++;
		else
			str[i]-=1,i++;
	}
}

void main()
{
	char ch='y',scrpt[100];
	system("clear");
	printf("\n\t\t ENCRYPTION & DECRYPTION ");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\t Enter Data that need to be Encrypt : ");
		fgets(scrpt,100,stdin);
		printf("\n\t Encrypting Data..\n");
		sleep(1);
		encrypt(scrpt);
		printf("\n\t Data after Encryption : %s",scrpt);
		printf("\n\n\t Now Decrypting Data..\n");
		sleep(1);
		decrypt(scrpt);
		printf("\n\t Data afer Decryption : %s",scrpt);
		
		printf("\n\n\t Want to Encrypt more..?? (y/n) : ");
		scanf(" %c",&ch);
	}
}

