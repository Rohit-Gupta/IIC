% create a spike train
sf=1000; % sampling frequency
x=0:.25*sf; % sample for 1/4 sec
y=zeros(size(x));
l=length(y);
r=randperm(l);
y(r(1:fix(.25*l)))=1; % spike train
% compute interspike interval and
% instantaneous firing frq
 ix=find(y);
 v=[0 sf./diff(ix)];
 % ... and create a simple plot
 stairs(x(ix),v,'r');
 hold on;
 stem(x,.2*sf*y,'.'); % scale for disp
 xlabel('time [ms]');
 ylabel('events/inst firing frq [hz]');
