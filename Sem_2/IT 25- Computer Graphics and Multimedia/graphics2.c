/*	DDA line*/
#include <stdio.h>
#include <graphics.h>
#define ABS(x) (x>0?x:-(x))
#define ROUND(x) ((int) (x+.5))
#define VALIDX(x) ((x)>=0 && (x)<=getmaxx())
#define VALIDY(y) ((y)>=0 && (y)<=getmaxy())
void myline(int x1,int y1,int x2,int y2);
int main()
{
	int gd=DETECT,gm;
	int x1,y1,x2,y2;   
    printf("Enter initial point (x,y) : \n");
    scanf("%d%*c%d",&x1,&y1);
    printf("Enter end point (x,y) : \n");
    scanf("%d%*c%d",&x2,&y2);
	initgraph(&gd,&gm,NULL);
	myline(x1,y1,x2,y2);
	getch();
	//delay(5000);
	closegraph();
	return 0;
}
void myline(int x1,int y1,int x2,int y2)
{
	if(!VALIDX(x1) || !VALIDY(y1)){
		printf("Out of range : Pixel (%d,%d) is not in range\n",x1,y1);
		return;
	}
	if(!VALIDX(x2) || !VALIDY(y2)){
		printf("Out of range : Pixel (%d,%d) is not in range\n",x2,y2);
		return;
	}
	int dy=y2-y1, dx=x2-x1, steps,k;
	float x=x1,y=y1,x_inc,y_inc;
	if ( ABS(dx) > ABS(dy) ) steps=ABS(dx);
	else steps=ABS(dy);
	x_inc=dx/(float) steps,y_inc=dy/(float) steps;
			
	for(k=0;k<steps;x+=x_inc,y+=y_inc,k++) 
		putpixel(ROUND(x),ROUND(y),13);
	putpixel(ROUND(x),ROUND(y),13);
}
