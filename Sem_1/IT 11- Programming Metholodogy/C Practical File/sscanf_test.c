/*	Q. String library function sscanf is used convert strings into different data types.
		WAP to convert strings to integer and float value ?
*/

#include<stdio.h>
#include<stdlib.h>

void main()
{
	char ch='y',ch1,str[50];
	int x;
	float y;
	system("clear");
	printf("\n\t\t\t Sscanf Test Program ");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\t\t MENU \n\n\t a. String-Int Conversion. \n\t b. String-Float Conversion. \n\n\t Plz enter your choice : ");
		scanf(" %c",&ch1);
		switch(ch1)
		{
			case 'a':	system("clear");
				printf("\n\t\t\t STRING-INTEGER CONVERSION");
				printf("\n\n\t Plz enter the string to be converted : ");
				scanf(" %s",str);
				printf("\n\t Conversion taking place.\n");
				sleep(1);
				sscanf(str,"%d",&x);
				printf("\n\t Conversion Completed, Now the integer is %d & its increment is %d.",x,x+1);
				break;

			case 'b':	system("clear");
				printf("\n\t\t\t STRING-FLOAT CONVERSION");
				printf("\n\n\t Plz enter the string to be converted : ");
				scanf(" %s",str);
				printf("\n\t Conversion taking place.\n");
				sleep(1);
				sscanf(str,"%f",&y);
				printf("\n\t Conversion Completed, Now the float value is %f & its increment is %f.",y,++y);
		}
		printf("\n\n\n\t Want to convert more (y/n) : ");
		scanf(" %c",&ch); 
	}

	printf("\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
}
