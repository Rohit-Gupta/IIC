import socket

host = socket.gethostname()
port = 2004
BUFFER_SIZE = 2000

str=input("enter the number of clients: ")
while str>0:
    tcpClientA = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcpClientA.connect((host, port))

    data = tcpClientA.recv(BUFFER_SIZE)
    print "received data:", data
    str-=1
    tcpClientA.close()
