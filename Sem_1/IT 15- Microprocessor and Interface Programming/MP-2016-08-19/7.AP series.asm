
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

mov ax,2000h
mov ds,ax
mov ax,0004h ;first term
mov dx,0005h ;common difference
mov cx,0005h ;number of terms
mov [0208h],ax
mov [0204h],ax
xor ax,ax
mov [0206h],ax
mov [020ah],ax
mov [0200h],dx
mov [0202h],cx
cmp cx,0001h
jz end
dec cx
mov bx,020ch
again:mov ax,[0204h]
      add ax,dx
      mov [bx],ax
      mov [0204h],ax
      ;mov ax,[0206h]
      ;adc ax,0000h
      ;mov [0206h],ax
      add bx,0002h
      mov [bx],ax
      add bx,0002h
      loop again
  end:hlt
 

ret




