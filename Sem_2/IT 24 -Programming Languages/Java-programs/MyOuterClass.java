
public class MyOuterClass {
	public MyInner ob ;
	private class MyInner{
		private int innerInt;
		
		public MyInner()
		{
			this.innerInt = 50;
			System.out.println("Inner Class's (\"MyInner\") Constructor Intialised");
		}
	}
	private int outerInt;
	public MyOuterClass(){
		System.out.println("Outer Class's (\"MyOuterClass\") Constructor Called");
		this.outerInt=120;
		System.out.println("Calling Inner Class(\"MyInner\")");
		ob = new MyInner();
	}
	
	public void display(){
		System.out.println("Outer Class Variable =" + outerInt
				+ "\nInner Class Varaible = " + this.ob.innerInt);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			MyOuterClass oc = new MyOuterClass();
			oc.display();
	}

}
