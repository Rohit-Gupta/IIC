import socket

host = socket.gethostname()
port = 2004
BUFFER_SIZE = 2000
MESSAGE = raw_input("ClientC: Enter message")

tcpClientA = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpClientA.connect((host, port))

tcpClientA.send(MESSAGE)
data = tcpClientA.recv(BUFFER_SIZE)
print " ClientB received data:", data

tcpClientA.close()
