#include <iostream>
using namespace std;

class employee
{
	int empid;
	char empnm[30];
public:
	friend istream & operator >>(istream &in, employee &e1)
	{
		cout<<"\nEnter employee id :";
		in>>e1.empid;
		cout<<"\nEnter employee name :";
		in>>e1.empnm;
		return in;
	}
	friend ostream & operator <<(ostream &out, const employee &e1)
	{
		out<<"\nemployee id:"<<e1.empid;
		out<<"\nemployee name:"<<e1.empnm<<endl;
		return out;
	}
};

int main(int argc, char const *argv[])
{
	employee e;
	cin>>e;
	cout<<e;
	return 0;
}