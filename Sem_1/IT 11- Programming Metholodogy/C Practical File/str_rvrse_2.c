/*	Write a function reverseStr() that takes a string as argument and reverses it. Write another
		function reverse() that reverses only iportion of the string.*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void reverseStr(char *str)
{
	char temp;
	int len,i,j;
	len=strlen(str);
	j=len-1;
	for(i=0;i<len/2;i++)
	{
		temp=str[i];
		str[i]=str[j];
		str[j]=temp;		
		j--;
	}
	printf("\n\n\t\t Reverse Process Taking Place \n");
	sleep(1);
	printf("\n\t\t String after reverse operation is : %s",str);
}

char* reverse(char *str, char *ele)
{
	int i=0,j=0,len;
	len=strlen(ele);
	
	while(str[i]!=ele[0] && str[i]!='\0' && ele[i]!='\0')
		i++;
	if(str[i]=='\0')
	{	printf("\n\n\t\t String that need to be swapped is not present inside the main string");	
		exit(0);
	}
	else
	{
		while(str[i]==ele[j])
		{
			str[i]=ele[--len];
			i++;
			j++;
		}
	}

	printf("\n\n\t\t Reverse Process Taking Place \n");
	sleep(1);
	printf("\n\n\t\t String after reverse operation : %s",str);
}

void main()
{
	char ch='y',ch1,str_input[100],str_chck[50];
	system("clear");
	printf("\n\t\t\t\t STRING REVERSE PROGRAM ");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\t\t Menu \n\n\t a. Reverse complete String. \n\t b. Reverse portion of a String.\n\n\t Plz enter your choice : ");
		scanf(" %c",&ch1);
		switch(ch1)
		{
			case 'a' :	system("clear");
				printf("\n\t\t\t\t Complete String Reverse ");
				printf("\n\n\t\t Enter the String : ");
				scanf(" %s",str_input);
				reverseStr(str_input);
				break;
			
			case 'b' :	system("clear");
				printf("\n\t\t\t\t String Portion Reverse ");
				printf("\n\n\t\t Enter the String : ");
				scanf(" %s",str_input);
				printf("\n\n\t\t Enter the portion to be reversed : ");
				scanf(" %s",str_chck);
				reverse(str_input,str_chck);
				break;
	
			default:
				printf("\n\n\t\t Not a Valid Choice.");
		}
		printf("\n\n\t\t Want to perform more string reversion ?? (y/n) : ");
		scanf(" %c",&ch);
	}
}
