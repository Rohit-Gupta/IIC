#include<stdio.h>
#include<string.h>

int strCASEcmp(char string1[10],char string2[10])
{	int i=0;
	for(;string1[i]!='\n';i++)
	{	if(string1[i]>64 && string1[i]<97)
		   string1[i]+=32;
		if(string2[i]>64 && string2[i]<97)
		   string2[i]+=32;
	}
	return strcmp(string1,string2);
}

void main()
{	char str1[10],str2[10];
	printf("Enter First String: ");
	fgets(str1,10,stdin);
	printf("Enter Second String: ");
	fgets(str2,10,stdin);
	if(!strCASEcmp(str1,str2))
	   printf("Strings are Same.\n");
	else
	   printf("Strings are not Same.\n");
}
