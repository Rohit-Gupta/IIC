#include<stdio.h>
void main()
{
	int arr[30],temp,size,i=0,small,pos=0;
	char ch='y';
	system("clear");
	printf(" Program Find Smallest element of an array and place it at 0th position ");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\t Enter size of the array : ");
		scanf("%d",&size);
		printf("\n\t Enter Array. \n");
		for(;i<size;i++)
		{
			printf("\t Element %d : ",(i+1));
			scanf("%d",&arr[i]);
		}
		
		small=arr[0];

		for(i=1;i<size;i++)
		{
			if(arr[i]<small)
			{
				small=arr[i];
				pos=i;
			}
		}

		temp=arr[0];
		arr[0]=arr[pos];
		arr[pos]=temp;

		printf("\n\t Array after smallest element swap. \n");
		for(i=0;i<size;i++)
		{
			printf("\t Element %d is : %d\n",(i+1),arr[i]);
		}

		printf("\n Want to Test again ?? (y/n) : ");
		scanf(" %c",&ch);
	}

	printf("\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
}

/*	Output

Program Find Smallest element of an array and place it at 0th position 

	 Enter size of the array : 10

	 Enter Array. 
	 Element 1 : 10
	 Element 2 : 9
	 Element 3 : 8
	 Element 4 : 7
	 Element 5 : 6
	 Element 6 : 5
	 Element 7 : 4
	 Element 8 : 3
	 Element 9 : 2
	 Element 10 : 1

	 Array after smallest element swap. 
	 Element 1 is : 1
	 Element 2 is : 9
	 Element 3 is : 8
	 Element 4 is : 7
	 Element 5 is : 6
	 Element 6 is : 5
	 Element 7 is : 4
	 Element 8 is : 3
	 Element 9 is : 2
	 Element 10 is : 10

 Want to Test again ?? (y/n) : n

 All rights are reserverd. Copyright © Abhilash Kant.

*/
