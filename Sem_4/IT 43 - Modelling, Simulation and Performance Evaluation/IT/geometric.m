x = [1:10];
y1 = geopdf(x,0.1);   % For p = 0.1
y2 = geopdf(x,0.25);  % For p = 0.25
y3 = geopdf(x,0.75);  % For p = 0.75

figure;
plot(x,y1,'k*')
hold on
plot(x,y2,'r*')
plot(x,y3,'b*')
legend({'p = 0.1','p = 0.25','p = 0.75'})
hold off

