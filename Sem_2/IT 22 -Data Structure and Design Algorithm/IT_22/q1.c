#include<stdio.h>
#include<stdlib.h>
int **mat,rows,cols,i,j;
void umin(void);
void ump(void);
void smc(void);
void ufree(void);
void main()
{
	umin();
	ump();
	smc();
	ufree();
}
void umin(void)
{
	printf("Please input the matrix :\nEnter the number of rows and columns :\n");
	scanf("%d%d",&rows,&cols);
	if(rows!=cols)
	{
		printf("For magic square matrix, the matrix should be a square matrix\n");
		exit(0);
	}
	mat=(int **)malloc(rows*sizeof(int *));
	if(mat==NULL)
	{
		printf("Memory not available\n");
		exit(0);
	}
	for(i=0;i<rows;i++)
	{
		mat[i]=(int *)malloc(cols*sizeof(int));
		if(mat[i]==NULL)
		{
			printf("Memory not available\n");
			exit(0);
		}
	}
	printf("Matrix created, enter the elements :\n");
	for(i=0;i<rows;i++)
	{
		for(j=0;j<cols;j++)
		scanf("%d",&mat[i][j]);
	}
}
void ump(void)
{
	printf("The matrix is :\n");
	for(i=0;i<rows;i++)
	{
		for(j=0;j<cols;j++)
			printf("%d\t",mat[i][j]);
		printf("\n");
	}
}
void smc(void)
{
	int rsum[rows],csum[cols],dsum[2],k;
	dsum[0]=0;
	for(i=0;i<rows;i++)
	{
		rsum[i]=0;
		for(j=0;j<cols;j++)
		{
			rsum[i]=rsum[i]+mat[i][j];
			if(i==j)
				dsum[0]=dsum[0]+mat[i][j];
		}
		printf("Sum of row%d = %d\n",i+1,rsum[i]);
	}
	for(i=0;i<cols;i++)
	{
		csum[i]=0;
		for(j=0;j<rows;j++)
		{
			csum[i]=csum[i]+mat[j][i];
		}
		printf("Sum of col%d = %d\n",i+1,csum[i]);
	}
	dsum[1]=0;
	for(i=0,j=cols-1;i<rows,j>=0;i++,j--)
	{
		dsum[1]=dsum[1]+mat[i][j];
	}
	for(k=0;k<2;k++)
		printf("Sum of diag%d = %d\n",k+1,dsum[k]);
	if(dsum[0]==dsum[1])
	{
		for(i=0;i<rows;i++)
		{
			for(j=0;j<cols;j++)
			{
				if(rsum[i]!=csum[j])
				{
					printf("The matrix is not a magic square\n");
					return ;
				}
			}
		}
		printf("The matrix is a magic square\n");
	}
	else
		printf("The matrix is not a magic square\n");
}
void ufree(void)
{
	for(i=0;i<rows;i++)
	{
		free(mat[i]);
		mat[i]=NULL;
	}
	free(mat);
	mat=NULL;
	printf("Memory freed\n");
}
