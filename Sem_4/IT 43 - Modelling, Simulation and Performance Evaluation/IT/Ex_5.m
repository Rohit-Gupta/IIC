%
%                 MEASURES OF VARIATION (OR) DISPERSION
%
% A good measure of dispersion should have the following characteristics:
%
%!. It should be easy to understand and calculate
%
%2. It should be based on all observations.
%
%3. It should should be readily comprehensible
%
% Its computation procedure should be simple.
%
%
% RANGE
%
% Range is the simplest measure of dispersion. It is the difference between
% the value of maximum and the value of minimum of the distribution.
%
%              Range = Maximum value - Minimum value
%
%
%            Coeff. of Range = (Maximum  value - Minimum value)/
%                              (Maximum  value + Minimum value)
%
%
%                            INDIVIDUAL SERIES
%
%EX: Find the range and coefficient of range of eights of 10 wstudents from
%the following data:
%___________________________________________________
%    41  20  15  65  73  84  53  35  71  55
%_____________________________________________________

A = [ 41  20  15  65  73  84  53  35  71  55 ];
N = length(A);
MAXIMUM = max(A)
MINIMUM = min(A)
Range = MAXIMUM -MINIMUM
COEFF = Range/(MAXIMUM +MINIMUM)


%                                DISCRETE SERIES
%
%EX: From the following data calculate the range and coefficient of range
%__________________________________________________________
% x    :  11  18  29  33  37  39  40  42  43
%__________________________________________________________
% f    :  100  105  91  82  61  32  70  88  67
%___________________________________________________________

X = [11  18  29  33  37  39  40  42  43 ];

N = length(X);
MAXIMUM = max(X);
MINIMUM = min(X)
Range = MAXIMUM -MINIMUM;
COEFF = Range/(MAXIMUM +MINIMUM)

%
%                                CONTINUOUS SERIES
%
%EX: From the data given below calculate the coefficient of range
%__________________________________________________________________
% Marks          :  10-20  20-30  30-40  40-50  50-60
%__________________________________________________________________
%No. of students :    22    28      44     43     49
%__________________________________________________________________

%
% In case of continuous series, range can be determined to find dofference
% between the upper limit of the highest class and the lowest limit of the
% lowest class
X1 = [10  20  30 40  50];
X2 = [20  30  40  50 60];

MIN_X1= min(X1);
MAX_X2= max(X2);
Range = MAX_X2 - MIN_X1;
Coeff = Range/(MAX_X2 + MIN_X1)

%
%                                MEAN DEVIATION
%
%                              INDIVIDUAL SERIES
%
% Let N denote the number of observation X. Let bar_X = Mean(X);
%
% Then mean deviation (MD_X) is defined as:
%
%           MD_X = SUM(X(i)-bar_X)/N
%
%
%EX: Find the mean deviation about mean of the marks obtained by 10
%students of 10th class in Mathematics in an examination. The marks
%obtained are 25, 30 , 21 , 55 , 47 , 10, 15, 17 , 45 , 35.

%
%
X = [ 25 30  21  55  47  10 15 17  45  35];

N = length(X);

bar_X = mean(X)

sum =0.0;
for i = 1: N
    dev = abs(X(i)-bar_X);
    sum = sum + dev;
end

MD_X = sum/N

%
%                            STANDARD DEVIATION
%
% The idea of standard deviation was first proposed by Karl Pearson in
% 1893. This measure is widely used for studying dispersion. Standard
% deviation is the positive square root of the average of squared
% deviations taken from arithmatic mean. It is generally denoted by the
% Greek alphabet $\sigma$ or by SD. 
%
% Le X be a random variate which takes on N values , namely X(1), X(2), 
%.. , X(N), then SD of these N observations is given by
%
%
%                    SD = SQRT[ SUM(X(i)*X(i))/N -(bar_X)^2]
%

%EX: find the standard deviation and its coefficient of SD  for the
%following data:
%___________________________________________________________
% X    :  11  13  14  15  17  18  19  20
%___________________________________________________________

X2 = [11  13  14  15  17  18  19  20];
N = length(X2);
bar_X2 = mean(X2)
sum = 0.0;
for i = 1:N
    sum = sum + (X2(i)*X2(i));
end
sum1 = sum/N;
term = sum1 - bar_X2 * bar_X2;
SD_1 = sqrt(term)
COEFF_SD = SD_1/bar_X2

%
%                      DISCRETE SERIES
% Find the standard deviation and its coefficient from the following data
%________________________________________________________________
%Size of item :  10  11  12  13  14  15  16
%________________________________________________________________
%Frequency    :   2  7   11  15  10   4   1
%________________________________________________________________

X = [10  11  12  13  14  15  16];
f = [2  7   11  15  10   4   1];
N = length(X);

PRODUCT = dot(X,f);

sum_f = 0.0;
sum_fx2 =0.0;
for i= 1 : N
    sum_f = sum_f + f(i);
    sum_fx2 = sum_fx2 + f(i)*X(i)*X(i);
end

bar_X = PRODUCT/sum_f 

sum1 = sum_fx2/sum_f
term = sum1 - bar_X * bar_X
SD_1 = sqrt(term)
COEFF_SD = SD_1/bar_X2

%
%                           CONTINUOUS SERIES
%
%EX: Calculate standard deviation and its coefficient for the following
%data.
%_______________________________________________________________
% Prifits(Rs)   : 20-30  30-40  40-50  50-60  60-70  70-80  80-90  90-100
%_______________________________________________________________________
%No. of Companies:  30    58      62     85    112     70    57      26
%_______________________________________________________________________

X1 = [20 30 40 50 60 70 80 90];
X2 =[30 40 50 60 70 80 90 100];
x = 0.5* ( X1 + X2)
f = [30    58      62     85    112     70    57      26];
N = length(X1);
%
% Calculate the mean
%

  s1 = 0.;
  s2 = 0.;
  
  for i  = 1: N
      s1 = s1 + x(i)*f(i);
      s2 = s2 + f(i);
     
  end
  bar_X = s1/s2
  
  sum_f = s2;
sum_fx2 =0.0;
for i= 1 : N
    
    sum_fx2 = sum_fx2 + f(i)*x(i)*x(i);
end



sum1 = sum_fx2/sum_f;
term = sum1 - bar_X * bar_X;
SD_1 = sqrt(term)
COEFF_SD = SD_1/bar_X

% 
%                     Coefficient of variation
% 
%It is a relative measure of dispersion . It is denoted by CV.
%
%                   CV = COEFF_SD * 100
%
%
%EX: The following are the runs scored by two batsman A and B in ten
%innings.
%__________________________________________________________________
%     A   :  101  27  0  36  82  45  7  13  65  14
%__________________________________________________________________
%     B   :   97  12  40  96  13  8  85  8  56  15
%__________________________________________________________________
%
%Who is more consistent?
%
A = [101  27  0  36  82  45  7  13  65  14];

B = [97  12  40  96  13  8  85  8  56  15];

N = length(A)

s1 = 0.0;
s2 = 0.0;
s3 = 0.0;
s4 = 0.0;
for i = 1: N
    s1 = s1 + A(i);
    s2 = s2 + A(i)*A(i);
    s3 = s3 + B(i);
    s4 = s4 + B(i)*B(i);
end
bar_A = s1/N;
bar_B = s3/N;

sum1_A = s2/N;
term_A = sum1_A - bar_A * bar_A;
SD_A = sqrt(term_A);
COEFF_SD_A = SD_A/bar_A;
CV_A = COEFF_SD_A*100

sum1_B = s4/N;
term_B = sum1_B - bar_B * bar_B;
SD_B = sqrt(term_B);
COEFF_SD_B = SD_B/bar_B;
CV_B = COEFF_SD_B*100
CV = [CV_A CV_B]

% Therefore batsman A is more consistent since CV_A > CV_B

  
  
  







% Graphs of frequency distribution
%
% Statistical nature of grouped frequency distribution is represented
% graphically , it is called graph of frequency  distribution. It can be
% represented graphically in four ways.
%
% Histogram
%
% Frequency polygon
%
% Frequency curve
%
% Ogive curve
%
% (1) Histogram
%
% Draw a histogram for the following distribution
%______________________________________________________________
% Class      0-10    10-20   20-30   30-40  40-50  50-60  60-70
%______________________________________________________________
% Frequency   5        11      19      21     16     10     8
%______________________________________________________________