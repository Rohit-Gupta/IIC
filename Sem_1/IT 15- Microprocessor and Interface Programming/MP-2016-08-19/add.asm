
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h                        


dataseg segment
oper1 dw 1002h
oper2 dw 1003h
result dw ?
dataseg  ends
codeseg segment
assume cs:code segment,ds:data segment
start: mov ax,dataseg
       mov ds,ax
       mov ax,oper1 
       add ax,oper2
       mov result,ax 
       hlt
       end start
codeseg ends  
       





