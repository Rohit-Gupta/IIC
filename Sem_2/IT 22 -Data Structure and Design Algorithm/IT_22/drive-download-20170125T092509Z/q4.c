#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int main()
{
struct employee{
char id[4];
char name[30];
char dept[30];
char phno[11];
}emp[10],e;
long size=sizeof(struct employee);
int n,i,flag=0,x;
printf("Enter the number of records you want to enter : ");
scanf("%d",&n);
for(i=0;i<n;i++)
{
printf("Enter the details(id,name,dept,phno) for record%d\n",i+1);
scanf("%s%s%s%s",emp[i].id,emp[i].name,emp[i].dept,emp[i].phno);
}
printf("%d records saved in the structure\n",i);
FILE *f;
if((f=fopen("recordfile.txt","rb+"))==NULL)
{
printf("Error in opening file for storing records\n");
exit(0);
}
fwrite(emp,sizeof(struct employee),n,f);
printf("Records stored in the file\n");
fseek(f,0l,0);
printf("Contents of the file :\nID\tNAME\tDEPT\tPH.NO.\n");
while(fread(&e,sizeof(struct employee),1,f)==1)
printf("%s\t%s\t%s\t%s\n",e.id,e.name,e.dept,e.phno);
char ch;
do
{
printf("What do you want to do :\n1.Get the details of a record by specifying the name of the employee\n2.Edit the ph.no. of an employee by specifying record number\n");
int choice;
scanf("%d",&choice);
switch(choice)
{
case 1:
fseek(f,0l,0);
printf("Enter the name of which you want to see the details : ");
char name[30];
scanf("%s",name);
for(i=0;i<n;i++)
{
fread(&e,sizeof(struct employee),1,f);
if(strcmp(name,e.name)==0)
{
printf("Details of the name '%s' :\nID\tNAME\tDEPT\tPH.NO.\n%s\t%s\t%s\t%s\n",name,e.id,e.name,e.dept,e.phno);
flag=1;
break;
}
}
if(flag==0)
printf("Name not found\n");
printf("Do you want to see the menu again (y/n) : ");
while('\n'!=getchar());
scanf("%c",&ch);
break;
case 2:
printf("Enter the record no. you want to edit : ");
scanf("%d",&x);
if(x>0&&x<=n)
{
printf("Enter the new ph.no. for the record :  ");
char phno[11];
scanf("%s",phno);
fseek(f,(x-1)*size,0);
fread(&e,size,1,f);
strcpy(e.phno,phno);
fseek(f,-size,1);
fwrite(&e,sizeof(struct employee),1,f);
fseek(f,0l,0);
printf("New contents of the file :\nID\tNAME\tDEPT\tPH.NO.\n");
while(fread(&e,sizeof(struct employee),1,f)==1)
printf("%s\t%s\t%s\t%s\n",e.id,e.name,e.dept,e.phno);
}
else
printf("Record not found\n");
printf("Do you want to see the menu again (y/n) : ");
while('\n'!=getchar());
scanf("%c",&ch);
break;
}
}while(ch=='y');
fclose(f);
return 0;
}
