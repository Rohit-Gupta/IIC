#include <iostream>
using namespace std;

class base
{
protected:
	int x;
	int y;
public:
	base()
	{
		x=y=0;
	}
	base(int c, int d)
	{
		x=c;
		y=d;
	}
	void show()
	{
		cout<<"\nContects of base class";
		cout<<"\bx:"<<x;
		cout<<"\ny:"<<y;
	}
};

class derived:public base
{
	int a;
	int b;
public:
	derived(int c, int d)
	{
		a=c;
		b=d;
		cout<<"initilaizatio done";
		base b1(c,d);
		b1.show();
	}
	void show()
	{
		cout<<"\nContects of base class";
		cout<<"\ba:"<<a;
		cout<<"\nb:"<<b<<endl;
	}
};

int main(int argc, char const *argv[])
{
	derived d(2,3);
	d.show();
	return 0;
}