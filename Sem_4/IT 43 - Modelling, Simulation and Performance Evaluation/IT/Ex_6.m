%
%                             CORRELATION
%
% Many statistical analyses are concerned with problems in which items
% consist of pairs of measurement.  It is frequently desirable to know
% whether a relationship exists between the measured variables or not. 
% A relationship between two such sets of measurement is described as
% CORRELATION.

%                             Definition
%
% the change in one variable produces corresponding change in the other
% variable then the two variables are said to be correlated and the
% relationship between them is called correlation. For example ( rainfall,
% yield), (income , expenditure).

%                             Properties 
% 1. The correlation coefficient, $r$, is a pure number and lies between -1
%    and +1
%
% 2. If $r$ = 0, then there is no correlation between the two variables
%


%                               COVARIANCE
%
% Let the values of two variables X and Y be as follows:
%
%  (X(1), Y(1)) , (X(2),Y(2)), .........., (X(N),Y(N)).
%
% If bar_X and bar_Y denotes the mean of the variable X and Y respectively,
% then
%
%     COV(X,Y) = SUM [( X(i)-bar_X)* (Y(i)-bar_Y)]/N
%

%                   Karl Pearson's (KP) coefficient of correlation
%
%          r = COV(X,Y)/sqrt(Var(x)*VAR(Y))
%
%
% EX: Calculate KP coefficient for the following data:
% _______________________________________________________________
%   X   : 1  2  4  5  7  8  10
%________________________________________________________________
%   Y   : 2  6  8  10  14  16  20
%________________________________________________________________
X = [1  2  4  5  7  8  10];
Y = [2  6  8  10  14  16  20];
N = length(X)
bar_X = mean(X)
bar_Y = mean(Y)
s1 = 0.0;
s2 = 0.0;
s3 = 0.0;
for i = 1:N
    s1 = s1 + (X(i)-bar_X)*(Y(i)-bar_Y);
    s2 = s2 + (X(i)-bar_X)*(X(i)-bar_X);
    s3 = s3 + (Y(i)-bar_Y)*(Y(i)-bar_Y);
end
cov_x_y= s1/N ;
var_X = s2/N ;
var_Y = s3/N ;
    r_1 = cov_x_y/(sqrt(var_X) *sqrt(var_Y));
    
% Alternative lengthy formula
    

s1 = 0.0;
s2 = 0.0;
s3 = 0.0;
s4 = 0.0;
s5 = 0.0;
for i = 1:N
    s1 = s1 + (X(i)*Y(i));
    s2 = s2 + (X(i)*X(i));
    s3 = s3 + (Y(i)*Y(i));
    s4 = s4 + X(i);
    s5 = s5 + Y(i);
end
s =[s4 s5 s2 s3 s1 N];
NUM = s1 - (s4*s5/N);
DENOM1 = s2 - (s4*s4)/N;
DENOM2 = s3 - (s5*s5)/N;
DENOM_F = sqrt(DENOM1)*sqrt(DENOM2);
r_2 = NUM/DENOM_F;
r = [r_1 r_2]

%
%                          Rank Correlation
%
% Certain characteristics like honesty , beauty , proficiency in music
% etc., cannot be numerically measured. But , the individual possessing
% these qualitative characteristics can be arranged in order of merrit ; in
% other words , they can be ranked with respect to the level of possessing
% these attributes when the items of data are ranked like this.
%
%
% the correlation between the ranks of these items is called the rank
% correlation. A numerically measure of such correlation is called
% Spearman's rank Correlation Coefficient given by
%
% rho = 1 - (6*sum(d(i)*d(i))/N(N^2 -1))
%
% where d(i) = difference between the two corresponding ranks
% N : Number of samples or observations or values
%
%EX: Ten competitors in a beauty contest are ranked by three judges in the
%following orders:
%__________________________________________________________________
%First Judge    :  1  6  5  10  3  2  4  9  7  8
%___________________________________________________________________
% Second Judge  :  3  5  8  4  7  10  2  1  6  9
%___________________________________________________________________
% Third Judge   :  6  4  9  8  1  2  3  10  5  7
%____________________________________________________________________
%
% Use the correlation coefficient to determine which pair of judges has the
% nearest approach to common taste in beauty.
%
R1 =[1  6  5  10  3  2  4  9  7  8]; % rank given by the first judge
R2 =[3  5  8  4  7  10  2  1  6  9]; %rank given by the second judge
R3 =[6  4  9  8  1  2  3  10  5  7]; %rank given by the second judge
d_12 = R1 - R2 % the difference of ranks of ith judge and the j-th judge
d_23 = R2 - R3
d_13 = R1 - R3
sum_12 = 0.0;
sum_23 = 0.0;
sum_13 = 0.0 ;
N = length(R1)
for i = 1:N
    sum_12 = sum_12 + d_12(i)*d_12(i);
    sum_23 = sum_23 + d_23(i)*d_23(i);
    sum_13 = sum_13 + d_13(i)*d_13(i);
end
s = [sum_12 sum_23 sum_13];
rho1 = 1 - (6*s(1)/(N*(N*N -1)));
rho2 = 1 - (6*s(2)/(N*(N*N -1)));
rho3 = 1 - (6*s(3)/(N*(N*N -1)));
rho = [rho1 rho2 rho3]
    
