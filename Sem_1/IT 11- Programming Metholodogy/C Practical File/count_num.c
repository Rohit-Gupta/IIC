#include<stdio.h>
void main()
{
	int num,copy,count=0;
	char ch='y';
	system("clear");
	printf(" Count the Numbers. ");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\t Enter the number : ");
		scanf("%d",&num);
		copy=num;
		count=0;
		while(num>0)
		{
			num=num/10;
			count++;
		}
		printf("\n\t No of digits presnt in %d are %d.",copy,count);
		printf("\n\n\t Want to continue more ?? (y/n) : ");
		scanf(" %c",&ch);
	}
	printf("\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
}

/*	Output

 Count the Numbers. 

	 Enter the number : 123

	 No of digits presnt in 123 are 3.

	 Want to continue more ?? (y/n) : y


	 Enter the number : 3546

	 No of digits presnt in 3546 are 4.

	 Want to continue more ?? (y/n) : y


	 Enter the number : 457362

	 No of digits presnt in 457362 are 6.

	 Want to continue more ?? (y/n) : n 

 All rights are reserverd. Copyright © Abhilash Kant.

*/
