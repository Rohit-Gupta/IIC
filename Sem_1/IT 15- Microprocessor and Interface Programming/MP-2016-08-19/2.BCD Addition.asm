
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

bcd1 dw 0301h
bcd2 dw 3400h
result equ [200h]
mov cx,bcd1
mov dx,bcd2
mov bx,2000h
mov ds,bx
mov bx,01h
mov al,cl
mov al,dl
daa
mov result,al
mov al,ch
add al,dh
daa
mov result+[bx],al

ret




