
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h 
BCD1 EQU 34H
BCD4 EQU 18H
BCD2 EQU 89H
BCD5 EQU 27H
MOV AL,BCD1
ADD AL,BCD2
DAA
MOV [2000H],AL
MOV AL,BCD4
ADC AL,BCD5
DAA
MOV [2002H],AL
HLT

ret




