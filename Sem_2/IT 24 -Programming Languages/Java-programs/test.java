class test implements Runnable 
{
    	public volatile int count = 0;
    	public void run() 
	{
        	Thread current = Thread.currentThread();
        	try 
		{
	            	while(count < 5) 
			{
                		System.out.println(current.getName() + " count = " + count++);
                		current.sleep(1000);
            		}
        	}
        	catch(Exception e) 
		{
            		e.printStackTrace();
        	}
    	}
    	public static void main(String[] args) 
	{
        	Thread one = new Thread(new test(), "one");
	        try 
		{
	            one.start();
	        }
	        catch(Exception e) 
		{
	            e.printStackTrace();
	        }
	}
} 
