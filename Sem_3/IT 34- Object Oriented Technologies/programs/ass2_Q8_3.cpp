#include <iostream>
#include <cstring>
#include <cstdlib>
using namespace std;

class staff
{
	int code;
	char name[30];
public:
	staff()
	{
		code=0;
		strcpy(name,"null");
	}
	friend istream & operator >> (istream &in, staff &s)
	{
		cout<<"\nEnter the code : ";
		in>>s.code;
		cout<<"\nEnter the name : ";
		in>>s.name;
		return in;
	}
	void showdata_s()
	{
		cout<<"\nCode : "<<code;
		cout<<"\nName : "<<name;
	}
};

class teacher: public staff
{
	char subject[30];
	char publication[30];
public:
	teacher()
	{
		strcpy(subject,"null");
		strcpy(publication,"null");
	}
	friend istream & operator >> (istream &in, teacher &t)
	{
		cout<<"\nEnter the subject : ";
		in>>t.subject;
		cout<<"\nEnter the publication : ";
		in>>t.publication;
		return in;
	}
	void showdata_t()
	{
		showdata_s();
		cout<<"\nSubject : "<<subject;
		cout<<"\nPublication : "<<publication;
	}
};

class typist:public staff
{
	int speed;
public:
	typist()
	{
		speed=0;
	}
	friend istream & operator >> (istream &in, typist ty)
	{
		cout<<"\nEnter the speed : ";
		in>>ty.speed;
		return in;
	}
	void showdata_ty()
	{
		showdata_s();
		cout<<"\nSpeed : "<<speed;
	}
};

class officer:public staff
{
	char grade[23];
public:
	officer()
	{
		strcpy(grade,"null");
	}
	friend istream & operator >> (istream &in, officer o)
	{
		cout<<"\nEnter the grade : ";		
		in>>o.grade;
		return in;
	}
	void showdata_o()
	{
		showdata_s();
		cout<<"\ngrade : "<<grade;
	}
};

class regular:public typist
{
public:
	void showdata_r()
	{
		showdata_ty();
	}
};

class casual:public typist
{
	int daily_wages;
public:
	casual()
	{
		daily_wages=0;
	}
	friend istream & operator >> (istream &in, casual &c)
	{
		cout<<"\nEnter the daily wages : ";
		in>>c.daily_wages;
		return in;
	}
	void showdata_c()
	{
		showdata_ty();
		cout<<"\n Daily Wages : "<<daily_wages;
	}
};

int main(int argc, char const *argv[])
{
	staff s;
	teacher t;
	typist ty;
	officer o;
	regular r;
	casual c;
	int ch,ch1;
	cin>>s;
	cout<<"\nMenu";
	cout<<"\n1. Enter as teacher ";
	cout<<"\n2. Enter as Regular typist ";
	cout<<"\n3. Enter as casual typist ";
	cout<<"\n4. Enter as officer ";
	cout<<"\nEnter your choice";
	cin>>ch;
	cout<<"\nPress 1 to Enter data and 2 to see data ";
	cin>>ch1;
	if(ch==1 && ch1==1)
		cin>>t;
	else if (ch==1 && ch1==2)
	{
		t.showdata_t();
	}
	else if(ch==2 && ch1==1)
		cout<<"\nYou dont have the right to enter data ";
	else if (ch==2 && ch1==2)
	{
		r.showdata_r();
	}
	else if(ch==3 && ch1==1)
		cin>>c;
	else if (ch==3 && ch1==2)
	{
		c.showdata_c();
	}
	else if(ch==4 && ch1==1)
		cin>>o;
	else if (ch==4 && ch1==2)
	{
		o.showdata_o();
	}
	return 0;
}