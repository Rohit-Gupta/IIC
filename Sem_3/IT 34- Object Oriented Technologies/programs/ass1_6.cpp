#include<iostream>
using namespace std;

class POUNDS
{
	public:
	int u;
	int o;
	void show()
	{
		cout<<u<<" Pounds"<<endl;
		cout<<o<<" Ounces";
	}
};

class KILOS
{
        public:
        int k;
        int g;
        friend POUNDS cal(KILOS &, POUNDS &);
};

POUNDS cal(KILOS &i, POUNDS &p)
{
	POUNDS r,e;
	i.g+=1000*i.k;
	r.u=i.g/448;
	r.o=(i.g-(p.u*448))/28;
	e.o=r.o+p.o;
	e.u=r.u+(e.o/16);
	e.o=e.o%16;
	e.show();
}

int main()
{
	KILOS i;
	POUNDS p;
	
	cout<<"Enter the Weight in Kilo :";
	cin>>i.k;
	cout<<"\nEnter the Weight in grams :";
	cin>>i.g;

	cout<<"Enter the Weight in Pounds :";
	cin>>p.o;
	cout<<"\nEnter the Weight in Ounces :";
	cin>>p.u;


	POUNDS ans;
	ans=cal(i,p);
	return 0;
}
