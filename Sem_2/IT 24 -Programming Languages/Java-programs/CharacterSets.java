import java.util.HashSet;
import java.util.Set;
public class CharacterSets 
{
	public static void main(String[] args) 
	{
		int numArgs = args.length;
		Set<Character> encountered = new HashSet<Character>();
		for (String argument : args) 
		{
			Set<Character> characters = new HashSet<Character>();
			int size = argument.length();
			for (int j=0; j<size; j++)
				characters.add(argument.charAt(j));
			Set<Character> commonSubset = new HashSet<Character>(encountered);
			commonSubset.retainAll(characters);
			boolean areDisjunct = commonSubset.size()==0;
			if (areDisjunct) 
			{
					System.out.println(characters + " and " + encountered + " are disjunct.");
			} 
			else 
			{
				boolean isSubset = encountered.containsAll(characters);
				boolean isSuperset = characters.containsAll(encountered);
				if (isSubset && isSuperset)
					System.out.println(characters + " is equivalent to " + encountered);
				else if (isSubset)
					System.out.println(characters + " is a subset of " + encountered);
				else if (isSuperset)
					System.out.println(characters + " is a superset of " + encountered);
				else
					System.out.println(characters + " and " + encountered + " have " +commonSubset + " in common.");
			}
			encountered.addAll(characters);
		}
	}
}
