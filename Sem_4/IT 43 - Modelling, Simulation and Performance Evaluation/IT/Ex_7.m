%
%                             REGRESSION
%
% The numerical measure of the average relationship between the two
% variables is called regression. Regression means stepping back towards
% the averages.
%
%                         LINES OF REGRESSION
%
% If the two variavle of a bivariate distribution are correlated , the
% points in the scattered diagram will cluster around a curve. This curve
% is called the curve of regression  and such a regression is known as
% non-linear regression or curvilinear regression. In particular if the
% curve is a straight line, it is called the line of regression and such a
% regression is known as linear regression.

% Regression equation of X on Y is
%
%        ( X - bar_X) = r * (SD_X/SD_Y)*( Y - bar_Y)
%
% Regression equation of Y on X is
%
%        ( Y - bar_Y) = r * (SD_Y/SD_X)*( X - bar_X)
%
%   where     r is the correlation coefficient between X and Y
%

%EX: THe   following data relates to the ages of husbands and wives
%___________________________________________________________________
% Age of Husbands :  25  28  30  32  35  36  38  39  42  45
%___________________________________________________________________
% Age of wives    :  20  26  29  30  25  18  26  34  35  46
%___________________________________________________________________
 X = [ 25  28  30  32  35  36  38  39  42  45]; %husbands age
 Y = [20  26  29  30  25  18  26  34  35  46]; %wives age
 N = length(X)
 %
 % standard deviation of X and Y
 %
 bar_X = mean(X)
 bar_Y = mean(Y)
 s1 = 0.0;
 s2 = 0.0;
 s3 = 0.0;
 for i = 1:N
     s1 = s1 + (X(i)-bar_X)*(X(i)-bar_X);
     s2 = s2 + (Y(i)-bar_Y)*(Y(i)-bar_Y);
     s3 = s3 + (X(i)-bar_X)*(Y(i)-bar_Y);
 end
 sd_X = sqrt(s1/N);
 sd_Y = sqrt(s2/N);
 
 cov_x_y= s3/N ;

 r = cov_x_y/(sd_X *sd_Y);
 
 b_XY = r * sd_X/sd_Y % slope/the regression coefficient of X on Y
 b_YX = r * sd_Y/sd_X  % slope/the regression coefficient of Y on X
 
 % Regression equation X on Y
 
 % X = bar_X + b_XY *(Y - bar_Y)
 
  C_XY = -b_XY * bar_Y +bar_X % Intercept of regression line of X on Y
  C_YX = -b_YX * bar_X + bar_Y % Intercept of regression line of Y on X
 %
 % Likely age of husband for age wife  is given by
 %
 %   X = b_XY *Y + C_XY
 %
 % If wife is of 30 yrs then age of husband is
     x = b_XY*30 + C_XY
     
 % % Likely age of husband for age wife  is given by
 %
 %   X = b_XY *Y + C_XY
 %
 % If husband is of 30 yrs then age of wife is
     y = b_YX*30 + C_YX
  
     figure()
  plot(X,Y,'*')