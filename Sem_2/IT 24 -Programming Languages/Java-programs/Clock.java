package clock;
import java.awt.event.*;
import javax.swing.*;
import java.text.*;
import java.util.*;
import java.awt.*;

public class Clock extends JPanel implements Runnable,ActionListener {
    public static JMenu jMenu1=new JMenu();
    public static JMenu jMenu2=new JMenu();
    public static JMenu jMenu3=new JMenu();
    public static JMenuBar jMenuBar1=new JMenuBar();
    public static JMenuItem jMenuItem1=new JMenuItem();
    public static JMenuItem jMenuItem2=new JMenuItem();
    public static JMenuItem jMenuItem3=new JMenuItem();
    public static JMenuItem jMenuItem4=new JMenuItem();
    public static JMenuItem jMenuItem5=new JMenuItem();
    public static JMenuItem jMenuItem6=new JMenuItem();
    public static JMenuItem jMenuItem7=new JMenuItem();
    public static JMenuItem jMenuItem8=new JMenuItem();
    public static JMenuItem jMenuItem9=new JMenuItem();
    public static String [] array = {"Europe/Copenhagen","US/Michigan","Asia/Istanbul","Iran","Asia/Singapore","Australia/LHI"};
    public static JComboBox dropdown = new JComboBox(array); 
    public static JLabel label = new JLabel("Label");   

    public static int seconds,minutes,hours;
    public static String timezone =  "Europe/Copenhagen";
    Thread thread = null;
    SimpleDateFormat formatter = new SimpleDateFormat("s", Locale.getDefault());
    Date currentDate;
    int xcenter = 175, ycenter = 175, lastxs = 0, lastys = 0, lastxm = 0, lastym = 0, lastxh = 0,lastyh = 0;

    private void drawStructure(Graphics g)
    {
        Color customColor = new Color(240,240,240);
        g.setColor(customColor);
        g.drawRect(10, 0, 600, 600);
        g.fillRect(0, 0, 600, 600);
        g.setFont(new Font("TimesRoman", Font.BOLD, 20));
        g.setColor(Color.WHITE);
        g.fillOval(xcenter - 170, ycenter - 170, 300, 300);
        g.setColor(Color.BLACK);
        g.drawString("12", xcenter - 30, ycenter - 150);
        g.drawString(".",xcenter + 50,ycenter - 140);
        g.drawString(".",xcenter + 100, ycenter - 90);
        g.drawString("3", xcenter + 115, ycenter - 20);
        g.drawString(".",xcenter + 100, ycenter + 50);
        g.drawString(".", xcenter + 50, ycenter + 100);
        g.drawString("6", xcenter - 30, ycenter + 125);
        g.drawString(".",xcenter - 90, ycenter + 100);
        g.drawString(".",xcenter - 140,ycenter +50);
        g.drawString("9", xcenter - 165, ycenter -20);
        g.drawString(".", xcenter - 140, ycenter -90);
        g.drawString(".",xcenter - 90,ycenter -140);
    }

    public void paint(Graphics g)
    {
        int xhour, yhour, xminute, yminute, xsecond, ysecond, second, minute, hour;
        drawStructure(g);
        currentDate = offsetTimeZone(new Date(), "Asia/Kolkata", timezone);
        //System.out.println("date:"+currentDate);
        formatter.applyPattern("s");
        second = Integer.parseInt(formatter.format(currentDate));
        formatter.applyPattern("m");
        minute = Integer.parseInt(formatter.format(currentDate));
        formatter.applyPattern("h");
        hour = Integer.parseInt(formatter.format(currentDate));
        //System.out.println("second="+second+",minute="+minute+",hour="+hour);
        seconds = second;
        minutes = minute;
        hours = hour;
        this.label.setBackground(Color.red);
        label.setText(hours+" : "+minutes+" : "+seconds);
        this.label.setBackground(Color.red);
        xsecond = (int) (Math.cos(second * 3.14f / 30 - 3.14f / 2) * 120 + xcenter-20);
        ysecond = (int) (Math.sin(second * 3.14f / 30 - 3.14f / 2) * 120 + ycenter-20);
        xminute = (int) (Math.cos(minute * 3.14f / 30 - 3.14f / 2) * 100 + xcenter-20);
        yminute = (int) (Math.sin(minute * 3.14f / 30 - 3.14f / 2) * 100 + ycenter-20);
        xhour = (int) (Math.cos((hour * 30 + minute / 2) * 3.14f / 180 - 3.14f / 2) * 80 + xcenter-20);
        yhour = (int) (Math.sin((hour * 30 + minute / 2) * 3.14f / 180 - 3.14f / 2) * 80 + ycenter-20);
        // Erase if necessary, and redraw
        g.setColor(Color.magenta);
        if (xsecond != lastxs || ysecond != lastys)
            g.drawLine(xcenter-20, ycenter-20, lastxs, lastys);
        if (xminute != lastxm || yminute != lastym)
        {
            g.drawLine(xcenter-20, ycenter - 21, lastxm, lastym);
            g.drawLine(xcenter - 21, ycenter-20, lastxm, lastym);
        }
        if (xhour != lastxh || yhour != lastyh)
        {
            g.drawLine(xcenter-20, ycenter - 21, lastxh, lastyh);
            g.drawLine(xcenter - 21, ycenter-20, lastxh, lastyh);
        }
        g.setColor(Color.black);
        g.drawLine(xcenter-20, ycenter-20, xsecond, ysecond);
        g.setColor(Color.BLACK);
        g.drawLine(xcenter-20, ycenter - 21, xminute, yminute);
        g.drawLine(xcenter - 21, ycenter-20, xminute, yminute);
        g.setColor(Color.BLACK);
        g.drawLine(xcenter-20, ycenter - 21, xhour, yhour);
        g.drawLine(xcenter - 21, ycenter-20, xhour, yhour);
        lastxs = xsecond;
        lastys = ysecond;
        lastxm = xminute;
        lastym = yminute;
        lastxh = xhour;
        lastyh = yhour;
    }

    public void start()
    {
        if (thread == null)
        {
            thread = new Thread(this);
            thread.start();
        }
    }

    public void stop() 
    {
        thread = null;
    }

    public void run()
    {
        while (thread != null)
        {
            try
            {
                Thread.sleep(100);
            }
            catch (InterruptedException e){}
            repaint();
        }
        thread = null;
    }

    public void update(Graphics g)
    {
        paint(g);
    }

    private static Date offsetTimeZone(Date date, String fromTZ, String toTZ)
    {
        // Construct FROM and TO TimeZone instances
        TimeZone fromTimeZone = TimeZone.getTimeZone(fromTZ);
        TimeZone toTimeZone = TimeZone.getTimeZone(toTZ);

        // Get a Calendar instance using the default time zone and locale.
        Calendar calendar = Calendar.getInstance();

        // Set the calendar's time with the given date
        calendar.setTimeZone(fromTimeZone);
        calendar.setTime(date);

        //System.out.println("Input: " + calendar.getTime() + " in " + fromTimeZone.getDisplayName());

        // FROM TimeZone to UTC
        calendar.add(Calendar.MILLISECOND, fromTimeZone.getRawOffset() * -1);

        if (fromTimeZone.inDaylightTime(calendar.getTime()))
            calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);

        // UTC to TO TimeZone
        calendar.add(Calendar.MILLISECOND, toTimeZone.getRawOffset());

        if (toTimeZone.inDaylightTime(calendar.getTime()))
            calendar.add(Calendar.MILLISECOND, toTimeZone.getDSTSavings());

        return calendar.getTime();
    }

    public static void main(String[] args) {
        // TODO code application logic here
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(350, 500);
        window.setLayout(new GridBagLayout());
        Clock clock = new Clock();
         GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
    c.ipady = 20;      //make this component tall
    //c.weightx = 0.0;
    //c.gridwidth = 100;
        c.ipadx=190;
    c.gridx = 0;
    c.gridy = 0;
    window.setJMenuBar(jMenuBar1);
        window.add(dropdown,c);
        //window.add(Constants.p1);
        //label.setSize(500, 100);
//        label.setBackground(Color.black);
//        Constants.p1.add(dropdown);
//        Constants.p1.add(label);
         c.fill = GridBagConstraints.HORIZONTAL;
    c.ipady = 20;      //make this component tall
    //c.weightx = 0.0;
    //c.gridwidth = 100;
    c.gridx = 0;
    c.gridy = 1;
        window.add(label,c);
         c.fill = GridBagConstraints.HORIZONTAL;
    c.ipady = 300;      //make this component tall
        c.ipadx=300;
    //c.weightx = 0.0;
    c.gridwidth = 1000;
    c.gridx = 0;
    c.gridy = 2;
        //window.add(dropdown,c);
        window.add(clock,c);
        //window.add(p2);
        window.setVisible(true);
        clock.start();
    }

   public Clock()
   {
       //Constants.p1.setSize(new Dimension(500, 500));
        jMenu1.setText("View");
        jMenu1.setToolTipText("");
        jMenu1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_1, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem1.setText("Tax Caculator");
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_2, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem2.setText("Length Calculator");
        jMenu1.add(jMenuItem2);
        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_3, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem3.setText("Currency Calculator");
        jMenu1.add(jMenuItem3);
        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem4.setText("Time Calculator");
        jMenu1.add(jMenuItem4);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setText("Cut");
        jMenu2.add(jMenuItem5);
        jMenuItem6.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem6.setText("Copy");
        jMenu2.add(jMenuItem6);
        jMenuItem7.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem7.setText("Paste");
        jMenu2.add(jMenuItem7);
        jMenuBar1.add(jMenu2);

        jMenu3.setText("Help");
        jMenuItem8.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem8.setText("Help");
        jMenu3.add(jMenuItem8);
        jMenuItem9.setText("About Calculator");
        jMenu3.add(jMenuItem9);
        jMenuBar1.add(jMenu3);

        dropdown.addActionListener(this);
   }

   @Override
   public void actionPerformed(ActionEvent e)
   {
       Object instance = dropdown.getSelectedItem();
       timezone = instance.toString();
       //System.out.println(timezone);
       repaint();
   }
}
