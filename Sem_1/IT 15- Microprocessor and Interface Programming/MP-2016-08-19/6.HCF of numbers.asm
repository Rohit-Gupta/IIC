
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

mov ax,2000h
mov ds,ax
mov ax,0004h
mov dx,0010h
mov cx,000Ch
mov bx,0200h
mov [0208h],bx
mov [bx],ax
add bx,0002h
mov [bx],dx
add bx,0002h
mov [bx],cx
mov bx,0200h
mov ax,[bx]
add bx,0002h
cmp ax,[bx]
jc noswap1
mov ax,[bx]
noswap1:add bx,0002h
        cmp ax,[bx]
        jc noswap2
        mov ax,[bx]
noswap2:mov [0206h],ax
        mov cx,ax
again:mov bx,0200h
      mov ax,[bx]
      xor dx,dx
      div cx
      cmp dx,0000h
      jnz decrease
      add bx,0002h
      mov ax,[bx]
      xor dx,dx
      div cx
      cmp dx,0000h
      jnz decrease
      add bx,0002h
      mov ax,[bx]
      xor dx,dx
      div cx
      cmp dx,0000h
      jnz decrease
      jmp halt
decrease:dec cx
         cmp cx,0001h
         jnz again
halt:mov [020ah],cx
     hlt
ret

ret




