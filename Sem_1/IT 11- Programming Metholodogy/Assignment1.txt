For all the problems first finallize the output in notebook.
After that run program on system to verify the result.

Q1

#include<stdio.h>
#define MSSG "Hello World\n"
int main(void)
{
	printf(MSSG);
	return 0;
}

Q2

#include<stdio.h>
int main(void)
{
	printf("Indian\b \n");  							
	printf("New\rDelhi\n");
	return 0;
}

Q3

#include<stdio.h>
int main(void)
{
	int a=11;
	printf("a=%d\t",a);
	printf("a=%o\t",a);
	printf("a=%x\t",a);
	printf("a=%X\n",a);
	return 0;
}

Q4

#include<stdio.h>
#include<limits.h>
int main(void)
{
	int a=4000000000;
	unsigned int b=4000000000;
	printf("a=%d, b=%u\n",a,b);
	printf("%d, %u\n",INT_MAX,UINT_MAX);
	return 0;
}

Q5

#include<stdio.h>
int main(void)
{
	char ch;
	printf("Enter a character : ");
	scanf("%c",&ch);
	printf("%d\n",ch);
	return 0;
}

Q6

#include<stdio.h>
int main(void)
{
	float b=123.1265;
	printf("%f\t",b);
	printf("%.2f\t",b);
	printf("%.3f\n",b);
	return 0;
}

Q7

#include<stdio.h>
int main(void)
{
	int a=625,b=2394,c=12345;
	printf("%5d,%5d,%5d\n",a,b,c);
	printf("%3d,%4d,%5d\n",a,b,c);
	return 0;
}

Q8

#include<stdio.h>
int main(void)
{
	int a=98;
	char ch='c';
	printf("%c,%d\n",a,ch);
	return 0;
}

Q9

/*E3.9*/
#include<stdio.h>
int main(void)
{
	float a1,b1,a2,b2,a3,b3;
	a1=2;
	b1=6.8;
	a2=4.2;
	b2=3.57;
	a3=9.82;
	b3=85.673;
	printf("%3.1f,%4.2f\n",a1,b1);
	printf("%5.1f,%6.2f\n",a2,b2);
	printf("%7.1f,%8.2f\n",a3,b3);
	return 0;
}

Q10

#include<stdio.h>
int main(void)
{
	printf("%10s\n","India");
	printf("%4s\n","India");
	printf("%.2s\n","India");
	printf("%5.2s\n","India");
	return 0;
}
