import java.io.*;
class Person
{
	String name;
	Person(String n)
	{
		name=n;
	}
	void display()
	{
		System.out.println("name="+name);
	}
}
class Student extends Person{
	int roll;
	Student(String na, int rr)
	{
		super(na);
		roll=rr;
	}
	void display(){
		super.display();
		System.out.println("roll no.="+roll);
	}
}
class Teacher extends Person
{
	int workex;
	Teacher(String ni, int wo)
	{
		super(ni);
		workex=wo;
	}
	void display()
	{
		super.display();
		System.out.println("workex="+workex);
	}
}
class Test
{
	public static void main(String args[])throws IOException
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Student sd[]=new Student[100];
		int tp=-1;
		Teacher tc[]=new Teacher[100];
		int sp=-1;
		int a;
		do
		{
			System.out.println("Enter your choice");
			System.out.println("1- EDIT STUDENT DATABASE");
			System.out.println("2-EDIT TEACHER DATABASE");
			System.out.println("3-Exit");
			int ch,cd;
			a= Integer.parseInt(br.readLine());
			switch(a)
			{

				case 1:
					{

						do
						{
							System.out.println("-------------------------");
							System.out.println("1- enter a record");
							System.out.println("2-delete record");
							System.out.println("3-search and display a record");
							System.out.println("4-display all records");
							System.out.println("5-Exit");
							System.out.println("-------------------------");
							ch=Integer.parseInt(br.readLine());
							switch(ch){
								case 1:
									System.out.println("Enter name");
									String p=br.readLine();

									System.out.println("Enter  roll no");
									int  d = Integer.parseInt(br.readLine());

									sd[++tp]=new Student(p,d);
									break;
								case 2:System.out.println("Enter the name whose record needs to be deleted");
								       String h=br.readLine();
								       int l=0;
								       int flag1=0;
								       while(l<=tp)
								       {
									       if(sd[l].name.equals(h)){
										       sd[l].name="";
										       sd[l].roll=0;
										       flag1=1;
									       }
									       l++;
								       }
								       if(flag1==0)
									       System.out.println("Record not found");


								       break;
								case 3:
								       System.out.println("Enter the name whose record needs to be displayed");
								       String g=br.readLine();
								       int k=0;
								       int flag=0;
								       while(k<=tp)
								       {
									       if(sd[k].name.equals(g)){
										       sd[k].display();
										       flag=1;
									       }
									       k++;
								       }
								       if(flag==0)
									       System.out.println("Record not found");

								       break;
								case 4:
								       int i;

								       for(i=0;i<=tp;i++)
								       {
									       if(!(sd[i].name.equals("")))
										       sd[i].display();
								       }
								       break;
								default:
								       System.out.println("Invalid choice");
							}
						}while(ch!=5);
						break;


					}
				case 2:
					{
						do
						{
							System.out.println("-------------------------");
							System.out.println("1- enter a record");
							System.out.println("2-delete record");
							System.out.println("3-search and display a record");
							System.out.println("4-display all records");
							System.out.println("5-Exit");
							System.out.println("-------------------------");

							cd=Integer.parseInt(br.readLine());
							switch(cd){
								case 1:
									System.out.println("Enter name");
									String p=br.readLine();
									System.out.println("Enter  work exp.");
									int  d = Integer.parseInt(br.readLine());


									sd[++tp]=new Student(p,d);
									break;
								case 2:System.out.println("Enter the name whose record needs to be deleted");
								       String h=br.readLine();
								       int l=0;
								       int flag1=0;
								       while(l<=tp)
								       {
									       if(sd[l].name.equals(h)){
										       sd[l].name="";
										       sd[l].roll=0;
										       flag1=1;
									       }
									       l++;
								       }
								       if(flag1==0)
									       System.out.println("Record not found");


								       break;
								case 3:
								       System.out.println("Enter the name whose record needs to be displayed");
								       String g=br.readLine();
								       int k=0;
								       int flag=0;
								       while(k<=tp)
								       {
									       if(sd[k].name.equals(g)){
										       sd[k].display();
										       flag=1;
									       }
									       k++;
								       }
								       if(flag==0)
									       System.out.println("Record not found");

								       break;
								case 4:
								       int i;

								       for(i=0;i<=tp;i++)
								       {
									       if(!(sd[i].name.equals("")))
										       sd[i].display();
								       }
								       break;
								default:
								       System.out.println("Invalid choice");
							}
						}while(cd!=5);
						break;
					}
				default:
					System.out.println("Invalid");
			}                   

		}while(a!=3);
	}
}
