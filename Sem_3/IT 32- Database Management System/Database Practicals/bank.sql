show databases;
use Bank;
show databases;
show tables;
desc table
use database;
mysql -u username -h hostname databasename


drop table tablename
drop database databasename
CREATE DATABASE IF NOT EXISTS temp_database;
DROP DATABASE IF EXISTS temp_database;
Alter tablename  Add column
Add Constraint constraintname
show columns from tablename



CREATE TABLE CUSTOMER (
cust_Name VARCHAR(50), 
cust_Street VARCHAR(50),
cust_City VARCHAR(50),
PRIMARY KEY (cust_Name),
KEY (cust_Name)
);


CREATE TABLE ACCOUNT (
accout_no INT(6),
branch_Name  VARCHAR(50),
balance INT(6),
PRIMARY KEY (accout_no),
KEY (accout_no),
FOREIGN KEY fk_branch(branch_Name)
references BRANCH(branch_Name)
);

CREATE TABLE BRANCH (
branch_Name VARCHAR(50),
cust_City VARCHAR(50),
assets INT(6),

PRIMARY KEY (branch_Name),
KEY (branch_Name)
);

CREATE TABLE LOAN (
loan_no INT(6),
branch_Name VARCHAR(50),
amount INT(6),
PRIMARY KEY (loan_no),
FOREIGN KEY fk_branch(branch_Name)
references BRANCH(branch_Name),
KEY (loan_no)

 );



CREATE TABLE DEPOSITOR (
cust_Name VARCHAR(50),
accout_no INT(6),
 
PRIMARY KEY (cust_Name, accout_no),
FOREIGN KEY fk_ACCOUNT (accout_no)
REFERENCES  ACCOUNT(accout_no) ,
FOREIGN KEY fk_CUSTOMER (cust_Name)
REFERENCES  CUSTOMER(cust_Name) 
);





CREATE TABLE BORROWER (
cust_Name VARCHAR(50),
loan_no INT(6),
PRIMARY KEY ( cust_Name,loan_no),
FOREIGN KEY fk_LOAN (loan_no)
REFERENCES  LOAN(loan_no) ,
FOREIGN KEY fk_CUSTOMER (cust_Name)
REFERENCES  CUSTOMER(cust_Name) 

);





INSERT INTO CUSTOMER (cust_Name, cust_Street, cust_City) VALUES ('Glenn', 'Sand Hill', 'Woodside');

INSERT INTO CUSTOMER (cust_Name, cust_Street, cust_City) VALUES ('Green', 'Walnut', 'Stamford');

INSERT INTO CUSTOMER (cust_Name, cust_Street, cust_City) VALUES ('Hayes', 'Main', 'Harrison');

INSERT INTO CUSTOMER (cust_Name, cust_Street, cust_City) VALUES ('jonson', 'Alma', 'Palo Alto');

INSERT INTO CUSTOMER (cust_Name, cust_Street, cust_City) VALUES ('jones', 'Main', 'Harrison');

INSERT INTO CUSTOMER (cust_Name, cust_Street, cust_City) VALUES ('Smith', 'North', 'Rye');

INSERT INTO CUSTOMER (cust_Name, cust_Street, cust_City) VALUES ('Turner', 'Putnam', 'Stamford');

INSERT INTO CUSTOMER (cust_Name, cust_Street, cust_City) VALUES ('Williams', 'Nassau', 'Princeton');

INSERT INTO CUSTOMER (cust_Name, cust_Street, cust_City) VALUES ('Lindsay', 'Park', 'Pitsfield');

INSERT INTO BRANCH (branch_Name, cust_City, assets) VALUES ('Brighton', 'Brooklyn', 710000);

INSERT INTO BRANCH (branch_Name, cust_City, assets) VALUES ('Downtown', 'Brooklyn', 900000);

INSERT INTO BRANCH (branch_Name, cust_City, assets) VALUES ('Mianus', 'HORSENECK', 40000);

INSERT INTO BRANCH (branch_Name, cust_City, assets) VALUES ('NorthTown', 'Rye', 370000);

INSERT INTO BRANCH (branch_Name, cust_City, assets) VALUES ('Perryridge', 'HORSENECK', 170000);

INSERT INTO BRANCH (branch_Name, cust_City, assets) VALUES ('Pownal', 'BENNIGTON', 30000);

INSERT INTO BRANCH (branch_Name, cust_City, assets) VALUES ('Redwood', 'Palo Alto', 210000);

INSERT INTO BRANCH (branch_Name, cust_City, assets) VALUES ('Round Hill', 'HORSENECK', 800000);

INSERT INTO LOAN (loan_No, branch_Name, amount) VALUES (11, 'ROUNDHILL', 900);


INSERT INTO LOAN (loan_No, branch_Name, amount) VALUES (14, 'Downtown', 1500);

INSERT INTO LOAN (loan_No, branch_Name, amount) VALUES (15, 'Perryridge', 1500);

INSERT INTO LOAN (loan_No, branch_Name, amount) VALUES (16, 'Perryridge', 1300);

INSERT INTO LOAN (loan_No, branch_Name, amount) VALUES (17, 'Downtown', 1000);

INSERT INTO LOAN (loan_No, branch_Name, amount) VALUES (23, 'Redwood', 2000);

INSERT INTO LOAN (loan_No, branch_Name, amount) VALUES (93, 'Mianus', 900);


INSERT INTO DEPOSITOR (cust_Name, accout_no) VALUES ('Hayes', 102);
INSERT INTO DEPOSITOR (cust_Name, accout_no) VALUES ('jonson', 101);

INSERT INTO DEPOSITOR (cust_Name, accout_no) VALUES ('jonson', 201);

INSERT INTO DEPOSITOR (cust_Name, accout_no) VALUES ('jones', 217);

INSERT INTO DEPOSITOR (cust_Name, accout_no) VALUES ('Lindsay', 222);

INSERT INTO DEPOSITOR (cust_Name, accout_no) VALUES ('Smith', 215);

INSERT INTO DEPOSITOR (cust_Name, accout_no) VALUES ('Turner', 305);

INSERT INTO BORROWER (cust_Name, loan_no) VALUES ('Adams', 16);

INSERT INTO BORROWER (cust_Name, loan_no) VALUES ('Curry', 93);

INSERT INTO BORROWER (cust_Name, loan_no) VALUES ('Hayes', 15);



INSERT INTO BORROWER (cust_Name, loan_no) VALUES ('jones', 17);

INSERT INTO BORROWER (cust_Name, loan_no) VALUES ('Smith', 11);

INSERT INTO BORROWER (cust_Name, loan_no) VALUES ('Smith', 23);

INSERT INTO BORROWER (cust_Name, loan_no) VALUES ('Williams', 17);

Select branch_Name
From LOAN;

Select distinct branch_Name
From loan;

Select all branch_Name
From LOAN;

select loan_No
	from LOAN
	where branch_Name = 'Perryridge'and amount > 800;



select loan_No
	from LOAN
	where branch_Name = ' Perryridge' ;


select loan_No
	from LOAN
	where amount between 900 and 1200;

select * from BORROWER, loan;


select cust_Name, BORROWER.loan_No, amount
           from BORROWER, loan
           where   BORROWER.loan_No = loan.loan_No  and
                         branch_name = 'Perryridge';



select cust_Name, BORROWER.loan_No as loan_id, amount
from BORROWER, loan
where BORROWER.loan_No = loan.loan_No ;



select cust_Name, T.loan_No, S.amount
           from BORROWER , loan 
           where  T.loan_No = S.loan_No ;



select cust_Name
	from CUSTOMER
	where cust_street like ‘%Main%’;


select cust_Name, amount
	from    BORROWER, loan
	where BORROWER.loan_No = loan.loan_NO and
	            branch_Name = 'Perryridge'
	order by cust_Name, amount;

select cust_Name, amount
	from    BORROWER, loan
    order by cust_Name, amount;




select distinct cust_Name
		from    BORROWER, LOAN
	where BORROWER.loan_No = LOAN.loan_NO and
	            branch_Name = 'Perryridge'
       order by cust_Name desc; 


(select cust_Name from DEPOSITOR)
union
(select cust_Name from BORROWER); 
(select cust_Name from DEPOSITOR)
intersect
(select cust_Name from BORROWER); 
(select cust_Name from DEPOSITOR)
except 
(select cust_Name from BORROWER); 

select avg (balance)
	from ACCOUNT
	where branch_Name ='Perryridge';

select count (*)
	from CUSTOMER; 

select count (distinct cust_Name)
	from DEPOSITOR;
select branch_Name, count (distinct cust_Name)
           from DEPOSITOR, ACCOUNT
           where DEPOSITOR.accout_No = ACCOUNT.accout_No
           group by branch_Name; 

select branch_Name, avg (balance)
           from ACCOUNT
           group by branch_Name
           having avg (balance) > 400 ;


select distinct cust_name
	from BORROWER
	where cust_name in (select cust_name
                                                       from DEPOSITOR ); 

select distinct cust_name
	from BORROWER
	where cust_name in (select cust_name
                                                       from DEPOSITOR ) ;


select distinct cust_name
	from BORROWER
	where cust_name not in (select cust_name
          from DEPOSITOR ); 

select branch_name
	from branch
	where assets <> some
	 	(select assets
	 	 from branch
		 where cust_city = ‘Brooklyn’); 

select R.branch_name
		from DEPOSITOR as T, ACCOUNT as R
		where T.account_number = R.account_number and
			T.cust_name = T.cust_name;


select distinct DEPOSITOR.cust_name
	from DEPOSITOR 
	where exists 
		(	select account.branch_name
		from DEPOSITOR , ACCOUNT 
		where DEPOSITOR.accont_no = account. accont_no);


select branch_name, amount,cust_name
from
LOAN inner join BORROWER on
LOAN.loan_no = BORROWER.loan_no;


select branch_name, amount,cust_name
from  loan left outer join BORROWER on
(.loan_no = BORROWER.loan_no); 

