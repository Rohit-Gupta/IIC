class School
{
	public static void main(String args[])
	{ 
		String name=args[0];
		float a=Float.parseFloat(args[1]);
		float b=Float.parseFloat(args[2]);
		float c=Float.parseFloat(args[3]);
		float avg=(a+b+c)/3;
		if(avg>90)
		System.out.println("Grade A");
		else if(avg>70 && avg<=90)
		System.out.println("Grade B");
		else if(avg>60 && avg<=70)
		System.out.println("Grade C");
		else if(avg>50 && avg<=60)
		System.out.println("Grade D");
		else
		System.out.println("Grade F");
	}
}