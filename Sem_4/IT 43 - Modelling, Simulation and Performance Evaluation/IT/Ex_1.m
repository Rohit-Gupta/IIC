% (1) plot a Pie chart
% Intermediate students of a cllege were asked to opt for different work
% experience. The details of these options are as under. Represent the data
% through a pie chart.
%
%  Photography   6 , Clay modelling  30,  Kitchen  gardening     48
% Doll making    12  , Bookbinding  24
%
X = [6 30 48 12 24];
labels={'Photography','Clay modelling','Kitchen gardening', 'Doll making', 'Bookbinding'};
figure()
pie3(X,labels)

% (2) Plot two pie chart side by side
X = [0.2 0.4 0.4];
labels = {'Taxes','Expenses','Profit'};
figure()
ax1 = subplot(1,2,1);
pie(ax1,X,labels)
title(ax1,'2012');

Y = [0.24 0.46 0.3];
ax2 = subplot(1,2,2);
pie(ax2,Y,labels)
title(ax2,'2013');

%(3) Bar chart
%
% Average wages of some firms is given below. Represent this by a simple
% bar diagram
% Firm:    A     B     C      D       E      F
%Av.Wage: 340    600   540    310     200    150
X=[ 340  600  540   310   200   150 ];
figure();
%width = 0.2;
bar(X)

%(4) Width of bar chart
% Horizontal graph
%X=[ 340  600  540   310   200   150 ];
figure();
width = 0.4;
barh(X,width)

%(5) Multiple bar diagram

% Birth and death rates in India are given below. Draw a suitable diagram.

%Period:     1951-60      1961-70     1971-80     1981-90      1991-2000     2001-10
%Birth rate:   45            42          35         40             35           30
%Death rate:   35            25          23         20             15           10



y = [45 35
     42 25
     35 23
     40 20
     35 15
     30 10];
figure();

bar(y,'group')
legend('Birth Rate','Death Rate')
set(gca,'xticklabel',{'1951-60','1961-70','1971-80','1981-90','1991-2000','2001-10'});

%set(gca,'xticklabel',{'A','B','C','D','E','E'});

title('Grouped Style')

%(6)Multiple bar diagram

% The data given below is the yield in million tonnes from 2007 to 2010.
% Draw a multiple bar diagram

%    Year        Rice       Sugar cane       Wheat
%    2007         45           30             25
%    2008         55           35             30
%    2009         60           35             35
%    2010         65           40             35

y = [45 30 25
     55 35 30
     60 35 35
     65 40 35
     ];
figure();

bar(y,'group')
legend('Rice','Sugar cane','Wheat','Location','NorthEastOutside')
set(gca,'xticklabel',{'2007','2008','2009','2010'});

%set(gca,'xticklabel',{'A','B','C','D','E','E'});
xlabel('Year(s)')
ylabel(' Yield in million tonnes')
title('Grouped Style')

%(7) Stacked /subdivide bar diagram
% Create data for childhood disease cases
measles = [38556 24472 14556 18060 19549 8122 28541 7880 3283 4135 7953 1884]';
mumps = [20178 23536 34561 37395 36072 32237 18597 9408 6005 6268 8963 13882]';
chickenPox = [37140 32169 37533 39103 33244 23269 16737 5411 3435 6052 12825 23332]';

% Create a stacked bar chart using the bar function
figure();

bar(1:12, [measles mumps chickenPox], 0.5, 'stack');
axis([0 13 0 100000]);
title('Childhood diseases by month');
xlabel('Month');
ylabel('Cases (in thousands)');
legend('Measles', 'Mumps', 'Chicken pox');

%(8) Represent the following data in a stacked/ subdivided bar diagram

%__________________________________________________________
%   Year             No. of Lectures           Total
%                 __________________________
%                  Science       Arts
%__________________________________________________________
%  1995              20           30            50
%  2000              30           40            70
%  2005              40           45            85
%  2010              40           60           100
%__________________________________________________________

science = [20 30 40 40]';
arts = [30 40 45 60]';

figure();

bar(1:4, [science arts],0.5,'stack');
axis ([0 5 0 100]);
set(gca,'xticklabel',{'1995','2000','2005','2010'});

title('Stacked Lecture diagram');
xlabel('Years');
ylabel('No. of lectures')
legend('Science', 'Arts');

%(9)Represent the following data in a stacked/ subdivided
%   horizontal bar diagram
figure();

barh(1:4,[science arts],0.2,'stack')
axis([0 100 0 5]);
set(gca,'yticklabel',{'1995','2000','2005','2010'});
title('Stacked Horizontal Lecture diagram');
xlabel('No. of Lectures');
ylabel('Years');
legend('Science','Arts')

%(10)Represent the following data in a stacked/subdivided
% horizontal bar diagram side by side

% Number of students in PG classes in a university is as follows:
%
%______________________________________________________________
%   Subject         2005         2006        2007
%______________________________________________________________
%   Science          240          280         340
%   Arts             560          610         570
%   Commerce         220          280         370
%______________________________________________________________
%   Total            1020         1170        1280
%______________________________________________________________
%
% Draw a subdivided bar diagram to the above data

Science =[240          280         340 ]';
Arts =[560          610         570]';
Commerce =[220          280         370]';

figure();
subplot(2,1,1);
barh(1:3,[Science Arts Commerce],0.4,'stack')
axis([0 1300 0 4]);
set(gca,'yticklabel',{'2005','2006','2007'});
title('Stacked Horizontal No. of PG classes diagram');
xlabel('No. of PG students');
ylabel('Years');
legend('Science','Arts','Commerce')

subplot(2,1,2);

bar(1:3,[Science Arts Commerce],0.4,'stack')
axis([0 4 0 1300]);
set(gca,'xticklabel',{'2005','2006','2007'});
title('Stacked  No. of PG classes bar-diagram');
xlabel('Years');
ylabel('No. of PG students');
legend('Science','Arts','Commerce')

%(11)Four different type of bar diagrams

x = [2,4];
y = [1,2,3,4;...
     5,6,7,8];

figure;
subplot(2,2,1);
barh(x,y,'grouped'); % groups by row
title('Grouped Style')

subplot(2,2,2);
barh(x,y,'stacked'); % stacks values in each row together
title('Stacked Style')

subplot(2,2,3);
barh(x,y,'hist'); % centers bars over x values
title('hist Style')

subplot(2,2,4);
barh(x,y,'histc'); % spans bars over x values
title('histc Style')

%(12) Percentage bar diagram:

% In percentage bar diagrams, the length of the bars is kept equal
% to 100 and the divisions of the bar correspond to the percentage of
% different  components. This diagram is called a percentage divided
% bar diagram.It is clear from the given figure that each component of the
% bar diagram indicates the percentage production of different items.

% Draw a percentage bar diagram for the following data.

%Chemical Composition of some plants

%_____________________________________________________________________
%  Plants      Water    Carbohydrate    Protein(g)   Fat(g)    Total
%             ontent(g)     (g)                               weight(g)
%_____________________________________________________________________
% Castor         4           3              4          18        29
% Pea            9           7              25          3        44
% Wheat          5           15             6           3        29
% Soyabean       6           7              25          7        44
%_____________________________________________________________________

% Computation of percentage  of constituents

% Water = W ;   Carbohydrate  = C ;   Protein = P ;  Fat = F

W = [(4/29)*100  (9/44)*100   (5/29)*100   (6/44)*100 ]';

C = [(3/29)*100  (7/44)*100   (15/29)*100   (7/44)*100 ]';

P = [(4/29)*100  (25/44)*100   (6/29)*100   (25/44)*100 ]';

F = [(18/29)*100  (3/44)*100   (3/29)*100   (7/44)*100 ]';

figure();
subplot(2,1,1);
barh(1:4,[W  C  P  F],0.4,'stack')
axis([0 110 0 5]);
set(gca,'yticklabel',{'Castor','Pea','Wheat','Soyabean'});
title('Percentage bar-diagram of Composition of some plants');
xlabel('Percentage compositions');
ylabel('Name of plants');
legend('W','C','P','F')
%
 subplot(2,1,2);
%
bar(1:4,[ W  C  P  F],0.4,'stack')
axis([0 5 0 110]);
set(gca,'xticklabel',{'Castor','Pea','Wheat','Soyabean'});
title('Percentage bar-diagram of Composition of some plants');
xlabel('Name of plants');
ylabel('Percentage compositions');
legend('W','C','P','F')
