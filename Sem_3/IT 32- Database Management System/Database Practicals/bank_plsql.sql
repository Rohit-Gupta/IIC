show triggers;
SET foreign_key_checks = 0




CREATE TABLE CUSTOMER_AUDIT ( 
    id int(11) NOT NULL AUTO_INCREMENT, 
    cust_Name VARCHAR(50) NOT NULL, 
    changedon datetime DEFAULT NULL, 
    action varchar(50) DEFAULT NULL, 
    PRIMARY KEY (id) 
 )




DELIMITER $$
CREATE TRIGGER before_customer_update 
    BEFORE UPDATE ON CUSTOMER
    FOR EACH ROW BEGIN
 
    INSERT INTO CUSTOMER_AUDIT
    SET action = 'update',
        cust_Name = OLD.cust_Name,
         changedon = NOW(); 
END$$
DELIMITER ;


UPDATE CUSTOMER
SET cust_Name = 'Frank'
WHERE cust_Street = 'North'

IMIT
SHOW TRIGGERS FROM Bank
WHERE `table` = 'CUSTOMER';


DROP TRIGGER table_name.trigger_name


SELECT * FROM Bank.triggers
WHERE trigger_schema = 'Bank';


DELIMITER $$
SELECT 'Comment';
 DROP PROCEDURE IF EXISTS FirstProc$$
 CREATE PROCEDURE FirstProc()
       BEGIN
               DECLARE g_inv_value  INT(15);
               DECLARE price   INT(5);
               DECLARE quantity  INT(4);
               set price = 100;
               set quantity = 4;
               SELECT concat('price is ', price);
   
       END$$
   DELIMITER ;




DELIMITER //
CREATE PROCEDURE GetCUSTOMERBySTREET(IN streetName VARCHAR(255))
 BEGIN
 SELECT * 
 FROM CUSTOMER
 WHERE cust_Street = streetName;
 END //
DELIMITER ;


CALL GetCUSTOMERBySTREET('North') //
