% To solve the Lorenz equations. It uses the file lorenz.m
clear all
% Clear all variables
t=[0 50];
% Time window
xinit=[0.1;0.1;0.1];
% Initial condition
[t,x]=ode45(@lorenz,t,xinit);
% Integrate in time
plot3(x(:,1),x(:,2),x(:,3))
% Plot solution
title('CFB'); xlabel('x'); ylabel('y'); zlabel('z');