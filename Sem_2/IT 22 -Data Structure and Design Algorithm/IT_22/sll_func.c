#include"sll.c"
int insert_beg(node ** head, int item)
{
	node *new=NULL;
	new=(node *)malloc(sizeof(node));
	if(NULL==new)
	{
		return -1;
	}
	new->data=item;
	if(NULL==*head)
	{
		new->next=NULL;
	}
	else
	{
		new->next=*head;
	}
	*head=new;
	return 1;
}
int insert_end(node **head,int item)
{
	node *new=NULL;
	node *p;
        new=(node *)malloc(sizeof(node));
        if(NULL==new)
        {
                return -1;
        }
        new->data=item;
	new->next=NULL;
	if(NULL==*head)
	{
		*head=new;
	}
	else
	{
		p=*head;
		while(NULL!=p->next)
		{
			p=p->next;
		}
		p->next=new;
	}
	return 1;
}
int insert_mid(node **head,int pos,int item)
{
	node *new=NULL;
        new=(node *)malloc(sizeof(node));
        if(NULL==new)
        {
                return -1;
        }
        new->data=item;
	if(item<(*head)->data)
		insert_beg(&head,item);
	else
	{
		

int del_beg(node **head)
{
	node *p;
	p=NULL;
	int item;
	if(NULL==*head)
	{
		return -1;
	}
	p=*head;
	if(NULL==p->next)
	{
		*head=NULL;
	}
	item=p->data;
	free(p);
	p=NULL;
	return item;
}
int del_end(node **head)
{
	node *p,*q;
	p=NULL;
	int item;
	if(NULL==*head)
	{
		return -1;
	}
	else
	{
		q=p;
		p=p->next;
	}
	q->next=NULL;
	item=p->data;
	free(p);
	p=NULL;
	return item;
}
int del_pos(node **head,int pos);
void free_list(node **head)
{
	node *p;
	while(NULL!=p)
	{
		*head=p->next;
		free(p);
		p=NULL;
	}
}
void disp(node **head)
{
	node *p;
	p=*head;
	while(NULL!=p)
	{
		printf("%d->",p->data);
		p=p->next;
	}
}
