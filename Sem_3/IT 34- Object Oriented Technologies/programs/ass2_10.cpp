#include <iostream>
using namespace std;

class code
{
	int a;
	public:
		code(int b)
		{
			a=b;
		}
		code(code & b)
		{
			a=b.a;
		}
		int *fun()
		{
			int *array= new int(a);
			cout<<a*2<<" Bytes of Memory allocated\n";
			delete [] array;
			cout<<a*2<<" Bytes of Memory deleted\n";
		}
};

int main()
{
	/* code */
	int n;
	cout<<"\nEnter the size that need to be allocated: ";
	cin>> n;
	code a1(n);
	code a2=a1;
	a1.fun();
	cout<<endl;
	a2.fun();
	return 0;
}
