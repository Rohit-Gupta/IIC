%
% Plotting distribution function
%
%Ex: Plot a distribution function of the following data:
%_____________________________________________________
%  x:  125  135  145  155  165  175  185  195  205
%_____________________________________________________
% f :   1    1    14  22    25  19   13    3   2
%_____________________________________________________
% Fit a normal distribution to the data
%


% To fit a normal distribution to the data, we need mean and standard
% deviation of the data.
%
% the normal density function is given by:
%
%         F(x) = (std_x * sqrt{2 * pi})^(-1)*exp[-{(x-mean_x)^2}/{2*std_x^2}]
%

x = [125  135  145  155  165  175  185  195  205];
f =  [ 1    1    14   22   25   19   13   3   2];
%
C_W = x(2)-x(1);
%
figure();
plot(x,f);
title('Normal  Frequency distribution');
xlabel('X');
ylabel('f');

hold on

s1 = 0.0;
s2 = 0.0;
N = length(x);
for i = 1: N
    s1 = s1 + f(i);
    s2 = s2 + x(i)*f(i);
end
sum_f = s1;
sum_fx = s2;
mean_x = sum_fx/sum_f

s3 = 0.0
for i = 1:N
    s3 = s3 + f(i)*x(i)*x(i);
end
std_x = sqrt(s3/sum_f - mean_x * mean_x)

%
% density function
%
c = std_x * sqrt(2*pi)
c_1 = 1/c

F =[];
for i = 1: N
    a = (x(i) -  mean_x)*(x(i) -  mean_x);
    b = a/(2*std_x*std_x);
    fac = c_1*exp(-b)
    F = [F;C_W*sum_f*fac];
end
f
F'
plot(x,F','*')
legend('x','F')
