.model small
.data
 n1 db 00h
 n2 db 00h
 n3 db 00h
 msg db 0dh,0ah,"Enter the 8 digit binary no. : $"
 msg1 db 0dh,0ah,"The input no. is : $"
 msg2 db 0dh,0ah,"The output is : $"
 array db 8 dup(?)
.code
.startup
mov ah,09h
mov dx,offset msg
int 21h

mov cx,8
mov bx,offset array
mov si,0

read:
     mov ah,01h
     int 21h
     sub al,30h
     mov [bx+si],al
     inc si
     mov dl,20h
     mov ah,2
     int 21h
     loop read
mov si,0
mov cx,8
mov bx,offset array
mov ah,09h
mov dx,offset msg1
int 21h

disp:
     mov dl,[bx+si]
     inc si
     add dl,30h
     mov ah,02h
     int 21h
     loop disp

mov n3,1
mov cx,8
mov bx,offset array
mov si,7
power2:
       mov al,[bx+si]
       mul n3
       add n2,al
       mov al,2
       mul n3
       mov n3,al
       dec si
       loop power2

mov ah,09h
mov dx,offset msg2
int 21h
mov dl,n2
mov ah,02h
int 21h

.exit
end