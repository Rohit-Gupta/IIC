clear all;
clc;

%Question 1
n=5000;
x=rand(n,1);

%a
h=hist(x,5000);  

%b mean
x_mean=0;
for i=1:n
    x_mean=x_mean+h(i)*x(i);
end
x_mean=x_mean/n

%b variance
va_x=0;
for i=1:n
    va_x=va_x+ ((x(i)-x_mean)^2);
end
var_x=va_x/n

%c
h=chi2gof(x)
if(h==1)
    S='the hypothesis is true as a uniform distribution on the interval (0,1)';
    disp(S)
else
    S='the hypothesis is false as a uniform distribution on the interval (0,1)';
    disp(S)
end

%Question 2 (a)
%Monte -Carlo Integration
b=2;
a=0;
sum=0;
y=rand(50000,1);
const=(b-a)/50000;
for i=1:50000
    sum=sum+exp(-y(i)*y(i));
end
int=const*sum

%Question 2 (b)
X=[];
Y=[];
nA=0;
for n=1:10
    x=1000*n;
    die1=(6*rand(1,x)); %simulation of die1
    die2=(6*rand(1,x)); %simulation of die2
    die3=(6*rand(1,x)); %simulation of die3
    dice_sum=die1+die2+die3;
    if(dice_sum==8)
        nA=nA+1;
    end
    pA=nA/x
    B=[x nA pA];
    X=[X x];
    Y=[Y pA];
end
plot(Y,X); %plot the given response
