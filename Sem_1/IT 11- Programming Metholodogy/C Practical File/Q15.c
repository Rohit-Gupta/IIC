#include<stdio.h>
#include<stdlib.h>

void main()
{	int *pi,n,i=0;
	printf("Enter no. of elements: ");
	scanf("%d",&n);
	pi=(int*)malloc(n*sizeof(int));
	if(pi==NULL)
	{	printf("Memory Allocation Failed!\n");
		exit(0);
	}
	else
	memset(pi,0,n*sizeof(int));
	printf("Value at allocated memory locations:\n");
	for(;i<n;i++)
	printf("p[%d]: %d\n",i,pi[i]);
	free(pi);
}
