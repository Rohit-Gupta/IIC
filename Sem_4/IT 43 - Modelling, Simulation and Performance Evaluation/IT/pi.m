% MC estimate of pi
clear all;
clc;

%% initialize random points
n=10000;
x=rand([1 n]);
y=rand ([1 n]);

%% compute using vectorization
radii = sqrt(x.^2+y.^2);
hits = sum(radii<=1);
misses = n-hits;
pi_mc = 4*(hits/n); 

fprintf('\nUsing Vectorization:: HITS = %d, MISSES = %d, PI = %f\n',hits,misses,pi_mc);