#include<stdio.h>

void main()
{
	int year;
	char ch='y';
	system("clear");
	printf("\n Leap Year Test Program. ");
	
	while(ch =='y'||ch =='Y')
	{
		printf("\n\n\t Enter the year : ");
		scanf("%d",&year);

		if((year%4==0) && (year%100 !=0))
			printf("\t %d is a leap year.",year);

		else if((year%4==0) && (year%100==0) && (year%400==0))
			printf("\t %d is a leap year. ",year);

		else
			printf("\t %d is not a leap year.",year);

		printf("\n\n\t Want to Test more...?? (y/n) : ");
		printf(" ");
		scanf(" %c",&ch);
	
	}
	printf("\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
}

/*	Output

xavier@Lunatic:~/lash/File$ 
xavier@Lunatic:~/lash/File$ gcc -o LeapY LeapY.c 
xavier@Lunatic:~/lash/File$ ./LeapY 

 Leap Year Test Program. 

	 Enter the year : 1900
	 1900 is not a leap year.

	 Want to Test more...?? (y/n) :  y


	 Enter the year : 1600
	 1600 is a leap year. 

	 Want to Test more...?? (y/n) :  y


	 Enter the year : 2012
	 2012 is a leap year.

	 Want to Test more...?? (y/n) :  n

 All rights are reserverd. Copyright © Abhilash Kant.

*/
