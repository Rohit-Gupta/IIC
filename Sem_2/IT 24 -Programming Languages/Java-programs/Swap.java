import java.util.Scanner;

public class Swap {
	private int x=10,y=20 ;
	public void swapByRef()
	{
		System.out.println("Swapping By reference");
		int temp;
		temp=this.x;
		this.x=this.y;
		this.y=temp;
	}
	public void set()
	{
		System.out.println("X = "+this.x +"\tY = "+this.y);
	}
	public void get(int a, int b)
	{
		this.x=a;
		this.y=b;
	}
	public void swapByVal(int a, int b)
	{
		System.out.println("Swapping By Value");
		int temp;
		temp=a;
		a=b;
		b=temp;
		System.out.println("X = "+a +"\tY = "+b);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Swap s = new Swap();
		System.out.println("Please Input no. to be Swapped i.e X and Y ");
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = sc.nextInt();
		s.get(a, b);
		s.set();
		s.swapByRef();
		s.set();
		s.swapByVal(a,b);
		sc.close();
	}

}
