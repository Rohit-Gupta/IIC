clc         %clear all the current variables with values
clear all   %clear the command screen
clf;        %clear the figure
n=500000;   %
s=0;
p=0;
for i=1:n
    x(i)=round(rand(1));    %generate random number and round the result, post that store that to a variable
    if(x(i)==1)             %if the value is greater than 0.5 then increment s
        s=s+1;
    else                    %if the value is greater than 0.5 then increment p
        p=p+1;
    end
end


prob_suc=p/(s+p)             %success  probability
prob_fail=s/(s+p)           %probibality of failure
y=binopdf(x,n,prob_fail);   %binomail distribustion of failure