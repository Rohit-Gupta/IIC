import java.time.*;
//import java.time.format.FormatStyle;
//import java.util.Formatter;



public class FirstExample {

	public FirstExample() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		
		//Current Zone Date and Time
		ZoneId z =ZoneId.of("Asia/Kolkata");
		ZonedDateTime mytime = ZonedDateTime.now(z);
		System.out.println("India:");		
		String d= mytime.toString();
		String date =d.substring(0, 10);
		String time = d.substring(11,19);
		System.out.println("Date:\t" + date +"\nTime:\t"+ time);
		
		//London Time and Date
		z =ZoneId.of("Europe/London");
		ZonedDateTime aTime = ZonedDateTime.now(z);
		System.out.println("\nLondon:");
		d= aTime.toString();
		date =d.substring(0, 10);
		time = d.substring(11,19);
		System.out.println("Date:\t" + date +"\nTime:\t"+ time);
		
		
		//NewYork Date and Time
		z =ZoneId.of("America/New_York");
		aTime = ZonedDateTime.now(z);
		System.out.println("\nNew York:");
		d= aTime.toString();
		date =d.substring(0, 10);
		time = d.substring(11,19);
		System.out.println("Date:\t" + date +"\nTime:\t"+ time);
		
		//Sydney Date and Time
		z =ZoneId.of("Australia/Sydney");
		aTime = ZonedDateTime.now(z);
		System.out.println("\nSydney:");
		d= aTime.toString();
		date =d.substring(0, 10);
		time = d.substring(11,19);
		System.out.println("Date:\t" + date +"\nTime:\t"+ time);
		
		//Johannesburg Date and Time
		z =ZoneId.of("Africa/Johannesburg");
		aTime = ZonedDateTime.now(z);
		System.out.println("\nJohannesburg:");
		d= aTime.toString();
		date =d.substring(0, 10);
		time = d.substring(11,19);
		System.out.println("Date:\t" + date +"\nTime:\t"+ time);
		
		//Niue Date and Time
		z =ZoneId.of("Pacific/Niue");
		aTime = ZonedDateTime.now(z);
		System.out.println("\nNiue:");
		d= aTime.toString();
		date =d.substring(0, 10);
		time = d.substring(11,19);
		System.out.println("Date:\t" + date +"\nTime:\t"+ time);
		
	
		
	}

}
