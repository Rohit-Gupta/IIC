#include<stdio.h>
void main()
{
	int arr[30],i=0,temp,n;
	char ch='y';
	system("clear");
	printf("\n Array adjacent element swap program. ");
	while(ch=='y'||ch=='Y')
	{
	
		printf("\n\n\t Enter size of  the array : ");
		scanf("%d",&n);
		printf("\n\t Enter element of the array.\n ");
		for(i=0;i<n;i++)
		{
			printf("\t Element %d : ",(i+1));
			scanf("%d",&arr[i]);
		}
		
		for(i=0;i<n-1;i=i+2)
		{
			temp=arr[i];
			arr[i]=arr[i+1];
			arr[i+1]=temp;
		}
		printf("\n\t Array after swapping. \n");
		
		for(i=0;i<n;i++)
		{
			printf("\t Element %d : %d\n",i+1,arr[i]);
		}

		printf("\n\n\t Want to test more ?? (y/n) :  ");
		scanf(" %c",&ch);
	}
	printf("\n\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
}

/*	Output

xavier@Lunatic:~/lash/File$ gcc -o swap Swap_2.c 
xavier@Lunatic:~/lash/File$ ./swap 

 Array adjacent element swap program. 

	 Enter size of  the array : 6

	 Enter element of the array.
 	 Element 1 : 10
	 Element 2 : 20
	 Element 3 : 30
	 Element 4 : 40
	 Element 5 : 50
	 Element 6 : 60

	 Array after swapping. 
	 Element 1 : 20
	 Element 2 : 10
	 Element 3 : 40
	 Element 4 : 30
	 Element 5 : 60
	 Element 6 : 50


	 Want to test more ?? (y/n) :  n


 All rights are reserverd. Copyright © Abhilash Kant.

*/
