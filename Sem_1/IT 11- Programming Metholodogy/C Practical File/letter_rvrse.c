/*		Q. WAP to only reverse the letters in the words of string. For Example if the string is “I have many
		books”, after reversing the letters of words it becomes “I evah ynam skoob”.		*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

void main()
{
	char str[100],tmp[100],i=0,j=0,k=0,ch='y';
	system("clear");
	printf("\n\n\t\t\t String Letter Reverse Program ");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\n\t Enter String : ");
		fgets(str,100,stdin);
		i=0,j=0,k=0;
		
		while(str[i]!='\n')
		{
			while(str[i]!=' ' && str[i]!='\n')
				tmp[j++]=str[i++];
			while(j>0)
				str[k++]=tmp[--j];
			while(str[i]==' ' && str[i]!='\n')
				i++,k++;
		}
		
		printf("\n\n\t String Reversion taking place \n");
		sleep(1);
		printf("\n\n\t String now : %s",str);
		printf("\n\n\t Want to continue more..(y/n) ?? : ");
		scanf(" %c",&ch);
	}
}
