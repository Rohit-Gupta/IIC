% To solve the equation dx/dt=x-x^2; It uses the file tuto1.m
t=[0 10];
% Time span
xinit=[0.1];
% Initial condition
[t,x]=ode45(@tuto1,t,xinit);
% Integrate equation
plot(t,x)
% Plot solution
title('CFB'); xlabel('t'); ylabel('x(t)');