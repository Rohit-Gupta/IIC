a = 20 ;
b = 30 ;
N = 25 ;
X = -log(rand(1,N))/a;
X = cumsum(X);
Y = -log(rand(1,N))/b;
serv_start = X(1);
Z(1) = serv_start + Y(1);
for k = 2:N
    serv_start = max([Z(k-1), X(k)]) ;
    Z(k) = serv_start + Y(k) ;
end
xaxis = [0, X(1)];
yaxis =[0, 0];
qs = 1;
X = X(2:length(X));
while length(X) > 0 
    if X(1) < Z(1)
        qs = qs +1;
        xaxis =[ xaxis xaxis(length(xaxis)) X(1)];
        yaxis =[yaxis qs qs];
        X = X(2:length(X));
    else
        qs = qs -1 ;
        xaxis = [ xaxis xaxis(length(xaxis)) Z(1)];
        yaxis = [ yaxis qs qs];
        Z = Z(2:length(Z));
    end
end
figure()
plot(xaxis, yaxis)
xlabel('time (hours)');
ylabel('queue size');