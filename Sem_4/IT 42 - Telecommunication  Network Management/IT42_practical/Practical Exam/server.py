import socket
from threading import Thread
from SocketServer import ThreadingMixIn

f = open("textfile.txt","rw+")
# Multithreaded Python server : TCP Server Socket Thread Pool
class ClientThread(Thread):

    def __init__(self,ip,port):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        print "thread started for " + ip + ":" + str(port)

    def run(self):
            next = f.readline()
            conn.send(next)

# Multithreaded Python server : TCP Server Socket Program Stub
IP = '0.0.0.0'
PORT = 2004
BUFFER = 20  # Usually 1024, but we need quick response

tcpServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpServer.bind((IP, PORT))
threads = []

while True:
	tcpServer.listen(10)
	#print "Multithreaded Python server : Waiting for connections from TCP clients..."
	(conn, (ip,port)) = tcpServer.accept()
	newthread = ClientThread(ip,port)
	newthread.start()
	threads.append(newthread)

for t in threads:
    t.join()
