#include<iostream>
using namespace std;

float volume(int a)
{
	return a*a*a;
}

float volume(int a, int b)
{
	return (4/3)*3.14*a*b;
}

float volume(int a, int b, int c)
{
	return a*b*c;
}

int main()
{
	int ch,a,b,c;
	char choice;
	float ans;

	cout<<"Menu ";
	cout<<"\n1. Cube ";
	cout<<"\n2. Sphere ";
	cout<<"\n3. Cuboid ";
	do
	{
		cout<<"\nPlease select one ";
        	cin>>ch;
		switch(ch)
		{
			case 1:	cout<<"\nEnter the side of Cube :";
				cin>>a;
				ans=volume(a);
				cout<<"\nVolume is :"<<ans;
				break;
			case 2:	cout<<"\nEnter the Height and Radius of the Sphere :";
				cin>>a>>b;
				ans=volume(a,b);
				cout<<"\nVolume is :"<<ans;
				break;
			case 3: cout<<"\nEnter the Length, Breath and Height of the cuboid :";
				cin>>a>>b>>c;
				ans=volume(a,b,c);
				cout<<"\nVolume is :"<<ans;
				break;
		}
		cout<<"\nDo you want to continue :";
		cin>>choice;
	}while(choice=='y');
	return 0;
}
