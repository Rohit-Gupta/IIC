#include<iostream>
using namespace std;

class emp
{
	int emp_id;
	char emp_nm[50];
	char emp_dob[30];
	double emp_sal;
	public:
	void add()
	{
		cout<<"\nEnter the Employee ID: ";
		cin>>emp_id;
		cout<<"\nEnter the Employee Name: ";
		cin>>emp_nm;
		cout<<"\nEnter the Employee Date of Birth: ";
		cin>>emp_dob;
		cout<<"\nEnter the Employee Salary: ";
		cin>>emp_sal;

		cout<<"\n\nThe Employee Details are :"<<endl;
		display();	
	}
	void display()
	{
		cout<<"ID :"<<emp_id;
		cout<<"\nName :"<<emp_nm;
		cout<<"\nDate of Birth :"<<emp_dob;
		cout<<"\nSalary :"<<emp_sal;
	}
};

int main()
{
	emp e;
	
	e.add();
	return 0;
}