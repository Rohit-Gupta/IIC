import socket           #import the  socket library
import sys              #import the  system library
import platform         #import the platform library

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('localhost', 10000)
print >>sys.stderr, 'connecting to %s port %s' % server_address
sock.connect(server_address) #connect to localhost and port 100000
try:

    # Send data
    Uname=platform.uname()      #getch the system information in form of list
    message=''.join(Uname)      #converting list to string
    print >>sys.stderr, 'sending "%s"' % message
    sock.sendall(message)       #send all the data

finally:
    print >>sys.stderr, 'closing socket'
    #finally closing the socket
    sock.close()
