#include<stdio.h>
#include<stdlib.h>
int min(int[],int);
int max(int[],int);
void merge(int[],int,int[],int,int[]);
void rot(int[],int,int);
void rev(int[],int);
int chk(int[],int);
void main(void)
{
	int ch,a[100],b[100],n,m,i,c,d[200];
	printf("Main Menu:");
	printf("\n1. Minimum of array:");
	printf("\n2. Maximum of array:");
	printf("\n3. Merging 2 arrays:");
	printf("\n4. Rotating an array:");
	printf("\n5. Revesing an array:");
	printf("\n6. Checking whether array is sorted:");
	printf("\n7. Exit");
	printf("\nEnter your choice");
        scanf("%d",&ch);
	printf("\nEnter the size of the array:");
	scanf("%d",&n);
	printf("\nEnter the Array:");
	for(i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
	switch(ch)
	{
		case 1: c=min(a,n);	
			printf("\nMinimum value:%d and Location:%d\n",a[c],c+1);
			break;
		case 2: c=max(a,n);	
			printf("\nMaximum value:%d and Location:%d\n",a[c],c+1);
			break;
		case 3: printf("\nEnter the size of the array:");
			scanf("%d",&m);
			printf("\nEnter the Array:");
			for(i=0;i<m;i++)
			{
				scanf("%d",&b[i]);
			}
			merge(a,n,b,m,d);
			printf("\nMerged Array:");
			for(i=0;i<m+n;i++)
			{
				printf("%d\t",d[i]);
			}
			printf("\n");
			break;
		case 4: printf("\nEnter the postions to be rotated:");
			scanf("%d",&m);
			printf("\nRotated array is:");	
			rot(a,m,n);
			for(i=0;i<n;i++)
			{
				printf("%d",a[i]);
			}
			printf("\n");
			break;
		case 5: rev(a,n);
			printf("\n");
			break;	
		case 6: if(chk(a,n))
				printf("\nArray is Sorted");
			else
				printf("\nArray is not sorted");
			printf("\n");
			break;
		case 7: exit(0);
		default: printf("\nWrong choice");	
	}
}
int min(int a[], int s)
{
	int min=a[0], i=0;
	for(int c=1;c<s;c++)
	{
		if(a[c]<min)
		{
			i=c;
			min=a[c];
		}
	}
	return i;
}
int max(int a[],int n)
{
	int max=a[0], i=0;
	for(int c=1;c<n;c++)
	{
		if(a[c]>max)
		{
			i=c;
			max=a[c];
		}
	}
	return i;
}
void merge(int a[],int m,int b[],int n,int c[])
{
	int i,j,k;
	j=k=0;
	for(i=0;i<m+n;)
	{
		if(j<m && k<n)
		{
			if(a[j]<b[k])
			{
				c[i]=a[j];
				j++;
			}
			else
			{
				c[i]=b[k];
				k++;
			}
			i++;
		}
		else if(j==m)
		{
			for(;j<m+n;)
			{
				c[i]=a[j];
				k++;
				i++;
			}
		}
		else
		{
			for(;j<m+n;)
			{
				c[i]=a[j];
				j++;
				i++;
			}
		}
	}
}
/*void rot(int a[],int d,int n)
{
	for(int i=0;i<d;i++)
	{
		int b,temp;
		temp=a[0];
		for(b=0;b<n-1;b++)
		{
			a[i]=a[i+1];
		}
		a[i]=temp;
	}
	for(int i=0;i<n;i++)
	{
		printf("%d",a[i]);
	}
}*/
void reverse(int a[], int start, int end)
{
  int i;
  int temp;
  while(start++ < end--)
  {
    temp = a[start];
    a[start] = a[end];
    a[end] = temp;
  }
}
 
// function that will rotate array by d elements
void rot(int a[], int d, int n)
{
  reverse(a, 0, d-1);
  reverse(a, d, n-1);
  reverse(a, 0, n-1);
}
void rev(int a[],int n)
{
	printf("Reverse of 1-D array is:");
	for(int i=n-1;i>=0;i--)
	{
		printf("%d\t",a[i]);
	}
}
int chk(int a[],int s)
{
	for(int i=0;i<s-1;i++)
	{
		if(a[i]>a[i+1])
			return -1;
	}
	return 1;
}
