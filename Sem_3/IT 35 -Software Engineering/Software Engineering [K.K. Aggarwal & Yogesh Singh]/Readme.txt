README file for book titled "Software Engineering",2007


Introduction
******************************************************************
A complete, take-home CD that will help you with Software
Engineering studies. 
You can easily transport all of the valuable information contained
in the book on your PC while the printed version of the book 
remains in your office or home.
You can easily work your way through the examples and experiments
with the many concepts illustrated in this book. 


Minimum System Requirements
******************************************************************
Operating System: Microsoft Windows 98, ME, 2000 or XP

RAM: 64MB

Free Hard Disk Space: 8MB

Optical Drive: CD or DVD drive

Software: Power Point Presentations are in PDF(Portable Document
	  Format). Download Adobe Acrobat Reader to view files. 
	  http://www.adobe.com/products/acrobat/readstep2.html

Viewing
******************************************************************
1. Insert the CD-ROM into the CD drive of your computer. 
2. Locate and launch the folder Softawre Engineering from your
   CD drive. 


Author Contact information 
******************************************************************

Prof. K.K. Aggarwal
Vice Chancellor,
Guru Gobind Singh Indraprastha University,
Kashmere Gate, Delhi-110006
India
email: kka@ipu.edu

Prof. Yogesh Singh
University School of Information Technology,
Guru Gobind Singh Indraprastha University,
Kashmere Gate, Delhi-110006
India
email: ys66@rediffmail.com

