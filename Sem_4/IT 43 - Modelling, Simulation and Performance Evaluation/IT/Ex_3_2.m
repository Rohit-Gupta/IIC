a=0.2; w=2;
t=linspace(-12,12,1000);
x=cos(w*t)./(sqrt(1+a^2*t.^2));
y=sin(w*t)./(sqrt(1+a^2*t.^2));
z=(-a*t)./(sqrt(1+a^2*t.^2));
grid
comet3(x,y,z);
plot3(x,y,z);
title('EX 3.2');
xlabel('x');
ylabel('y');
zlabel('z');