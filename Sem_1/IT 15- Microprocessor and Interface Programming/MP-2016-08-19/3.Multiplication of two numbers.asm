
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

num1 equ 0104h
num2 equ 1E01h
result equ [200h]
mov bx,2000h
mov ds,bx
mov cx,00h
mov bx,01h
mov ax,num1
mov dx,num2
mul dx
mov result,al
mov result+[bx],ah
inc bx
mov result+[bx],dl
inc bx
mov result+[bx],dh
hlt

ret




