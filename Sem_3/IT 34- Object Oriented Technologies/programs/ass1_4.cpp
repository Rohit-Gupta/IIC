#include<iostream>
using namespace std;

void sort(int *f, int *s)
{
	int t;
	if(*f>*s)
	{
		t=*f;
		*f=*s;
		*s=t;
	}	
}

int main()
{
	int f,s;

	cout<<"Enter the Two numbers :";
	cin>>f>>s;
	
	sort(&f,&s);
	
	cout<<"Numbers after sorting are :"<<endl<<f<<endl<<s;
	return 0;
}
