/*	boundary fill 4 connected algorithm*/
#include <stdio.h>
#include <graphics.h>
void boundaryfill4(int x,int y,int fcolor,int bcolor);
int main()
{
	int gd=DETECT,gm;
	int left,top,right,bottom,fill;  
	printf("Lets draw a rectangle first\n"); 
    printf("Enter rectangle left upper corner (left,top) : ");
    scanf("%d%*c%d",&left,&top);
    printf("Enter rectangle right bottom corner(right,bottom) : ");
    scanf("%d%*c%d",&right,&bottom);
	printf("Enter the color to be filled : ");
    scanf("%d",&fill);
	initgraph(&gd,&gm,NULL);
	rectangle(left,top,right,bottom);
	boundaryfill4((int)(left+right)/2,(int)(top+bottom)/2,fill,getpixel(left,top));
	getch();
	closegraph();
	return 0;
}
void boundaryfill4(int x,int y,int fill,int boundary)
{
	int current;
	current = getpixel (x, y ) ;
	if ((current != boundary) && (current != fill)){
		putpixel(x,y,fill);
		boundaryfill4 (x+1, y, fill, boundary);
		boundaryfill4 (x-1, y, fill, boundary) ;
		boundaryfill4 (x, y+1, fill, boundary);
		boundaryfill4 ( x , y-1, fill, boundary) ;
	}
	return;
}
