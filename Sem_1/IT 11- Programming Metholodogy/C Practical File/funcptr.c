// Q. Swapping using func ptr.

#include<stdio.h>
void swap(int *a, int *b)
{
	int temp;
	temp=*a;
	*a=*b;
	*b=temp;
}

void main()
{
	int a,b;
	void (*p)(int *x,int *y);
	p=swap;
	printf("\n Enter first integer :");
	scanf("%d",&a);
	printf("\n Enter second integer :");
	scanf("%d",&b);
	p(&a,&b);
	printf("\n After swap ");
	printf("a= %d | b=%d \n",a,b);
}

/*	Output

Enter first integer :1

 Enter second integer :2

 After swap 2 | 1 

*/
