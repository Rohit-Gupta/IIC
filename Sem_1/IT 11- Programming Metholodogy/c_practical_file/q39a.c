#include<stdio.h>
char *udf_strncpy(char *dest,char *src,size_t n)
{
	size_t i;
	for (i = 0; i < n && src[i] != '\0'; i++)
                   dest[i] = src[i];
        for ( ; i < n; i++)
                   dest[i] = '\0';
        return dest;
}	
void main()
{	char str1[30]={"\0"},str2[30];
	int n;
	printf("Enter the String to be copied: ");
	gets(str2);
	printf("Enter the no. of characters to be copied: ");	
	scanf("%d",&n);
	udf_strncpy(str1,str2,n);
	printf("\nThe first string is: ");
	puts(str1);
	printf("\nThe second string is: ");
	puts(str2);
}

