import socket

host = socket.gethostname()
port = 2004
BUFFER_SIZE = 2000
MESSAGE = raw_input("ClientC: Enter message")
 
tcpClientB = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpClientB.connect((host, port))

tcpClientB.send(MESSAGE)
data = tcpClientB.recv(BUFFER_SIZE)
print " ClientB received data:", data

tcpClientB.close()
