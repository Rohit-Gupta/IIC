%
% MEDIAN
%
% It is the middlemost point or the central value of the variable in a set
% of observations arranged either in ascending order or in descending order
% of their magnitudes.
%
%
%Individual Series:
%
% To find the value of median in this case, the terms are arranged in
% ascending or descending order first; then the middle term taken is
% median.
%
% When the number of terms is odd
%
% The terms are arranged in ascending order or descending order and then
% (n+1)/2 th term value of the order is taken MEDIAN.
%

%EX: Find median from the following data.
%
% 17 19 21 13 16 18 24 22 20
%
x = [17 19 21 13 16 18 24 22 20];
N = length(x);
[rows, columns] = size(x)
%sorted_X = reshape(sort(x(:), 'descend'), [columns, rows])'
sorted_Y = reshape(sort(x(:), 'ascend'), [columns, rows])'
%
% In this example N is odd
%
% Median = (N+1)/2 th value of the sorted array
%
n = (N+1)/2
Median = sorted_Y(n)

% When the number of terms is even
%
% The terms are arranged in ascending order or descending order and then
% mean of the two middle terms is taken as median .
%
% Median = [(N/2) + ((N/2)+1)]/2 th value of the sorted array
%
%Ex: Find the median of the following data
%
% 87 71 32 28 69 85 53 90 60 56
%
x = [87 71 32 28 69 85 53 90 60 56];
N = length(x);
[rows, columns] = size(x)
%sorted_X = reshape(sort(x(:), 'descend'), [columns, rows])'
sorted_Y = reshape(sort(x(:), 'ascend'), [columns, rows])'
if mod(N,2)==0
    Median_1 = 0.5*(sorted_Y(N/2)+ sorted_Y((N/2)+1))
else
    Median_1 = sorted_Y((N+1)/2)
end

%                            Discrete series:

% the discrete series involves frequencies. In order to find out median in
% such a case , it is necessary to divide the total frequency into two
% equal parts.

%1. Arrange the data either in ascending order or in descending order
%
%2. Calculate cumulative frequencies
%
%3. Find out the value of the middle item by applying the formula
%
%             Median = size of (N+1)/2 th item
%
%4. Find out the total in the cumulative frequency column which is either
%
%   equal to (N+1)/2 th or next higher than that.
%
% 5. Locate the value of the variable corresponding to the cumulative
% frequency . this value of the variable is the value of the median.
%

% Determine the median from the following data
%______________________________________________________________
% Size:       5   10   15  20  25  30  35
%______________________________________________________________
% Frequency:  2    3    4   6   10  5   2
%______________________________________________________________

% Note: Here the size taken as variable x is already in ascending order
%
x = [ 5 10 15 20 25 30 35];
f = [ 2 3 4 6 10 5 2];
n = length (x);

s = 0;
CF = [];
for i = 1:n
    s = s + f(i);
    cf(i) = s;
end
CF = [cf];

N = CF(end);

% (N+1)/2 th entry  in CF

Index = (N + 1)/2;

for i = 1:n
    if (cf(i) <= Index && cf(i+1) >= Index)
        item = i+1 ;
    end
end
Median_x = x(item)

%
%Ex: Calculate the median for the following data
%
%________________________________________________________________
%No. of Students :    6  4  16  7  8  2
%________________________________________________________________
% Marks          :    20 9  25  50 40 80
%________________________________________________________________

% Let the number of students be denoted by : f - frequency
%
% Marks by the variable x
%
x = [6 4 16 7 8 2 ; 20 9 25 50 40 80]';
%
% x needs to be sorted as per step:1 listed earlier
%
% further Marks need to be sorted
%
y_sorted = sortrows(x,2);
%
%Revised sorted table becomes
%
Rev_x = y_sorted';
%
% x- marix
%
x_new = Rev_x(2,:);
f_new = Rev_x(1,:);
cfn = zeros(1,6);
n1 = length (x_new);

s = 0;
CF = [];
for i = 1:n1
    s = s + f_new(i);
    cfn(i) = s;
end
CF = [cfn]

N = CF(end)

% (N+1)/2 th entry  in CF

Index = (N + 1)/2

for i = 1:n1
    if (cfn(i) <= Index && cfn(i+1) >= Index)
        item = i+1 ;
    end
end
Median_x = x_new(item)

%
%                            Continuous case :
%
% In continuous series, median cannot be located in a straight forward
% method. In this case , the median lies in a class interval , i.e.,
% between the lower and upper limit of a class interval. for exact value,
% we have to interpolate the median with the help of a formula:
%
%        Median = L + [((N/2) - m)/f)*C ]
%
% where N = total frequency
% L = Lower limit of the class in which median lies
% m = Cumulative frequency of the class preceding the median class
% f =  frequency of the class in which the median lies
% C = Width of the class interval of the class in which the median lies
%
%
%Ex: Calculate the median from the following data:
%
%___________________________________________________________________
% Class interval :  0-10  10-20  20-30  30-40  40-50  50-60  60-70
%___________________________________________________________________
% Frequency:         7      18     34     50    35      20      6
%____________________________________________________________________

%
 x1 = [ 0 10 20 30 40 50 60]; % lower values of the class interval
 x2 = [ 10 20 30 40 50 60 70];% higher values of the class interval
 f = [ 7 18 34 50 35 20 6]; % frequency values
 N1 = length(f);
 cf = zeros(1,N1);
 CF =[];
 s = 0.0;
 for i = 1: N1
     s = s + f(i);
     cf(i) = s;
 end
 CF = [cf]

 N = CF(end)
 %
 % expected size of the (N/2) th item
 %
 H = N/2
 %
 %
 % C -- the width of the class interval
 %
 C = 10
 %
 % L - Lower limit of the class in which median lies
 %
 for i = 1: N1
     if ( cf(i) <= H && cf(i+1) >= H)
         item = i+1;
     end
 end
 lower = item;
 x2_high = x2(lower-1);
 x2_lower = x1(lower);
 L = 0.5*(x2_high + x2_lower);

 %
 % m ---Cumulative frequency of the class preceding the median class
 %
 m = CF(lower-1);
 %
 %Finally using the formula for the median
 %

 Median_new = L + ((H - m)/f(item))*C
