#include<stdio.h>
#include<stdlib.h>

void heapsort(int[],int);
void heapify(int[],int);
void adjust(int[],int);
void inst_sort(int [],int);
void merge_sort(int x[],int, int);
int findmax(int [], int k);
void selct(int [], int k);
void quicksort(int [],int,int);
void shellsort(int [], int);
void display(int [],int);

void main()
{
	int c,a[50],n,i,ch;
	printf("Main Menu\n");
	printf("1.Insertion Sort\n");
	printf("2.Quick Sort\n");
	printf("3.Merge Sort\n");
	printf("4.Selection Sort\n");
	printf("5.Shell Sort\n");
	printf("6.Heap Sort\n");
	printf("7.Exit\n");
	printf("Enter your choice\n");
	scanf("%d",&c);
	printf("\nEnter the size of the array:");
	scanf("%d",&n);
	printf("\nEnter the Array:");
	for(i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
	do{
		switch(c)
		{
			case 1: inst_sort(a,n);
				display(a,n);
				break;
			case 2: quicksort(a,0,n-1);
				display(a,n);
				break;
			case 3: merge_sort(a,0,n-1);
				display(a,n);
				break;
			case 4: selct(a,n);
				display(a,n);
				break;
			case 5: shellsort(a,n);
				display(a,n);
				break;
			case 6:	heapsort(a,n);
				display(a,n);
				break;
			case 7: exit(0);
			default:printf("Please enter a valid choice");
		}
		printf("\ndo you want to sort more(0/1)....");
		scanf("%d",&ch);
	}while(ch==1);	
}

void display(int arr[],int n)
{
	printf("\nThe Sorted array is:");
	for(int i=0;i<n;i++)
		printf(" %d",arr[i]);
}
void shellsort(int arr[], int num)
{
    int i, j, k, tmp;
    for (i = num / 2; i > 0; i = i / 2)
    {
        for (j = i; j < num; j++)
        {
            for(k = j - i; k >= 0; k = k - i)
            {
                if (arr[k+i] >= arr[k])
                    break;
                else
                {
                    tmp = arr[k];
                    arr[k] = arr[k+i];
                    arr[k+i] = tmp;
                }
            }
        }
    }
}
void quicksort(int x[],int first,int last){
    int pivot,j,temp,i;
     if(first<last){
         pivot=first;
         i=first;
         j=last;
         while(i<j){
             while(x[i]<=x[pivot]&&i<last)
                 i++;
             while(x[j]>x[pivot])
                 j--;
             if(i<j){
                 temp=x[i];
                  x[i]=x[j];
                  x[j]=temp;
             }
         }
         temp=x[pivot];
         x[pivot]=x[j];
         x[j]=temp;
         quicksort(x,first,j-1);
         quicksort(x,j+1,last);
    }
}
int findmax(int b[], int k)
{
    int max = 0, j;
    for (j = 1; j <= k; j++)
    {
        if (b[j] > b[max])
        {
            max = j;
        }
    }
    return(max);
}
void selct(int b[], int k)
{
    int  temp, big, j;
    for (j = k - 1; j >= 1; j--)
    {
        big = findmax(b, j);
        temp = b[big];
        b[big] = b[j];
        b[j] = temp;
    }
    return;
}
void merge_sort(int x[], int end, int start) {
	int j = 0;
	const int size = start - end + 1;
	int mid  = 0;
	int mrg1 = 0;
	int mrg2 = 0;
	int executing[50];
	if(end == start)
	  return;
	mid  = (end + start) / 2;
	merge_sort(x, end, mid);
	merge_sort(x, mid + 1, start);
	for (j = 0; j < size; j++)
	  executing[j] = x[end + j];
	mrg1 = 0;
	mrg2 = mid - end + 1;
	for (j = 0; j < size; j++) {
		if(mrg2 <= start - end)
		   if(mrg1 <= mid - end)
		    if(executing[mrg1] > executing[mrg2])
		     x[j + end] = executing[mrg2++]; else
		     x[j + end] = executing[mrg1++]; else
		    x[j + end] = executing[mrg2++]; else
		   x[j + end] = executing[mrg1++];
	}
}
void inst_sort(int num[],int n) {
	int i,j,k;
	for (j=1;j<n;j++) {
		k=num[j];
		for (i=j-1;i>=0 && k<num[i];i--)
		   num[i+1]=num[i];
		num[i+1]=k;
	}
}
void heapsort(int a[],int n) {
	int i,t;
	heapify(a,n);
	for (i=n-1;i>0;i--) {
		t = a[0];
		a[0] = a[i];
		a[i] = t;
		adjust(a,i);
	}
}
void heapify(int a[],int n) {
	int k,i,j,item;
	for (k=1;k<n;k++) {
		item = a[k];
		i = k;
		j = (i-1)/2;
		while((i>0)&&(item>a[j])) {
			a[i] = a[j];
			i = j;
			j = (i-1)/2;
		}
		a[i] = item;
	}
}
void adjust(int a[],int n) {
	int i,j,item;
	j = 0;
	item = a[j];
	i = 2*j+1;
	while(i<=n-1) {
		if(i+1 <= n-1)
		   if(a[i] <a[i+1])
		    i++;
		if(item<a[i]) {
			a[j] = a[i];
			j = i;
			i = 2*j+1;
		} else
		   break;
	}
	a[j] = item;
}
