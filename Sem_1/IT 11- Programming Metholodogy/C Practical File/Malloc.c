#include<stdio.h>
#include<stdlib.h>

void main()
{       int row,col,(*pi)[col],i=0,j=0;
	printf("Enter no. of rows: ");
	scanf("%d",&row);
	printf("Enter no. of column: ");
	scanf("%d",&col);
	pi=malloc(row*sizeof(int));
        if(pi==NULL)
        {       printf("Memory Allocation Failed!\n");
                exit(0);
        }
        for(i=0;i<row;i++)
	   memset(pi+i,0,col*sizeof(int));
        printf("Value at allocated memory locations:\n");
        for(i=0;i<row;i++)
	   for(j=0;j<col;j++)
		printf("p[%d][%d]: %d\n",i,j,*(pi+i)[j]);
        free(pi);
}

