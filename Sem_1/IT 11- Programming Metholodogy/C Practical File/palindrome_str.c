/*		Program to test whether the string is palindrome or not. A string is palindrome if it remains the
		same when reserved. For example: radar and madam.				*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

void main()
{
	char ch='y',str[50],rev[50],ch1;
	int i=0,len,flag=0,mid,end;
	system("clear");
	printf("\n\t\t\t\t PALINDROME CHECKER");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\t\t Enter the String to check if it is Palindrome : ");
		scanf(" %s",str);
		len=strlen(str);
		mid=len/2;
		end=len-1;
		printf("\n\t\t Now Checking the String. \n");
		sleep(1);
		for(i=0;i<mid;i++)
		{
			if(str[i]!=str[end])
			{
				printf("\n\n\t\t %s is a Not Palindrome.",str);
				break;
			}	
			end--;		
				}		
			if(i==mid)
				printf("\n\n\t\t %s is a Palindrome.",str);

		printf("\n\n\t\t Want to test more strings..?? (y/n) : ");
		scanf(" %s",&ch);
	}
	printf("\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
}

