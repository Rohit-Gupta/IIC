data = randn(1,1000); %// example data
num_bars = 15; %// specify number of bars

%numel(data)=length(data)

[n x] = hist(data,num_bars); %// use two-output version of hist to get values



n_normalized = n/numel(data)/(x(2)-x(1)); %// normalize to unit area
bar(x, n_normalized, 1); %// plot histogram (with unit-width bars)
hold on
plot(x, n_normalized, 'r') %// plot line, in red (or change color)


