//(a.)Using Double Pointers
		//Displays indices sum as value.
#include<stdio.h>
#include<strings.h>
#include<stdlib.h>
void main()
{
	int rows,cols,i,j;
	printf("Please enter the no. and collums of the matrix:");
	scanf("%d\n%d",&rows,&cols);
	int **ary = (int**)malloc(rows*sizeof(int *));
	for (i=0;i<rows;i++)
	{
		ary[i]=(int *)malloc(cols*sizeof(int));
	}//Array allocation ends.
	printf("\nThe Matrix is\t:\n");
	for(i=0;i<rows;i++)
	{
		for(j=0;j<cols;j++)
		{
			*(*(ary+i)+j)=i+j+2;
			printf("%d\t",*(*(ary+i)+j));
		}
		printf("\n");
	}
	printf("\n");
}
