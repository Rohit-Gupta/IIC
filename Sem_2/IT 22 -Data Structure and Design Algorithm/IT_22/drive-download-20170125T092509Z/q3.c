#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int main(int argc,char *argv[])
{
if(argc!=4)
{
printf("Wrong number of arguments\n");
exit(0);
}
char str1[10],str2[30],str3[40];
FILE *f1,*f2,*f3;
if((f1=fopen(argv[1],"r"))==NULL)
{
printf("Error in opening file '%s'\n",argv[1]);
exit(0);
}
printf("Contents of file '%s' :\n",argv[1]);
while(fgets(str1,9,f1)!=NULL)
{
int l=strlen(str1);
if('\n'==str1[l-1])
str1[l-1]='\0';
puts(str1);
}
if((f2=fopen(argv[2],"r"))==NULL)
{
printf("Error in opening file '%s'\n",argv[2]);
exit(0);
}
printf("Contents of file '%s' :\n",argv[2]);
while(fgets(str2,29,f2)!=NULL)
{
int l=strlen(str2);
if('\n'==str2[l-1])
str2[l-1]='\0';
puts(str2);
}
if((f3=fopen(argv[3],"w+"))==NULL)
{
printf("Error in opening file '%s'\n",argv[3]);
exit(0);
}
fseek(f1,0l,0);
fseek(f2,0l,0);
while((fgets(str1,9,f1)!=NULL)&&(fgets(str2,29,f2)!=NULL))
{
int l=strlen(str1);
if('\n'==str1[l-1])
str1[l-1]='\0';
fputs(str1,f3);
fputs(" ",f3);
fputs(str2,f3);
}
printf("Task done\n");
fclose(f1);
fclose(f2);
fseek(f3,0l,0);
printf("Contents of file '%s' :\n",argv[3]);
while(fgets(str3,39,f3)!=NULL)
{
int l=strlen(str3);
if('\n'==str3[l-1])
str3[l-1]='\0';
puts(str3);
}
fclose(f3);
}
