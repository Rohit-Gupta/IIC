#include<stdio.h>
#include<string.h>
char *udf_strncat(char *dest,const char *src,size_t n)
{
        size_t dest_len = strlen(dest);
        size_t i;
        for (i = 0 ; i < n && src[i] != '\0' ; i++)
      		dest[dest_len + i] = src[i];
      	dest[dest_len + i] = '\0';
               	return dest;
}
void main()
{       char str1[30],str2[30];
	int n;
        printf("Enter the First String: ");
        gets(str1);
        printf("Enter the second string: ");
	gets(str2);
	printf("Enter the no. of characters to be concatenated: ");	
	scanf("%d",&n);
        udf_strncat(str1,str2,n);
        printf("\nThe concatenated string is: ");
        puts(str1);
}

