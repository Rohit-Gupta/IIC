clc;        %clear all the Existing data
clear;      %clear the command window
clf;        %clear the figure
N=10000;
mu=5;
sigma=2;
z=randn(1,N);   %generate 10000 random numbers
figure
bins=20;        %set teh value of bins to be 20
hist(z,bins);   %plot the histogram
figure
bins=(-4.5:1:20)    %set teh value of bins to be 25 witha gap of 1
x=mu+sigma*z;
[yvalues,xvalues]=hist(x,bins);     %plot the histogram
yvalues=yvalues/(N);
bar(xvalues,yvalues)
z=(-4.5-1/2:1/10:20+1/2);
Pdf=exp((-(z-mu).^2)/(2*sigma.^2));
Pdf=Pdf/sqrt(2*pi*sigma.^2);
hold on
plot(z,Pdf,'-')
xlabel('x')
ylabel('j-X(x)')