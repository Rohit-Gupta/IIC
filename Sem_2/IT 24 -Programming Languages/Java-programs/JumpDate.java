import java.time.*;
public class JumpDate {

	public static void main(String[] args) {
		
		LocalDate today = LocalDate.now();
		System.out.println("Today's Date:\t" + today);
		LocalDate prev=LocalDate.now().minusDays(5);
		System.out.println("Five Days Back Date:\t" + prev);
		LocalDate fut=LocalDate.now().plusDays(5);
		System.out.println("Five Days Hence Date:\t" + fut);
	}
}
