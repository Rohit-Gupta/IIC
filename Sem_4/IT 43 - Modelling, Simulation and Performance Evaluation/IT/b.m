clc         %clear all the exixting data
clear all   %clear the command screen
n=10;
m=100;
X=round(rand(n,m));     %GENERATE 1000 RANDOM NUMBERS AND ROUNDING THEM TO EITHER 0 OR 1
Y=sum(X);       
Rel_freq=hist(Y,[0:n])/m;          %GENERATE THE HISTORGRAM
for k=0:n
    PMF(k+1)=nchoosek(n,k)*(2^(-n));
end

figure
plot([0:n],Rel_freq,'*',[0:n],PMF,'+')      %PLOT THE GIVEN DATA