#include <iostream>
using namespace std;

class matrix{
 	int r,c;
 	int **p;
 public:
 	matrix (int n1, int n2)
 	{
 		r=n1;
		c=n2;
		p=new int *[r];
		for(int i=0 ; i<r ; i++)
		{
			p[i] = new int[c];
		}
 	}
 	int getdata(int i,int j)
 	{
 		int value;
		value=p[i][j];
		return (value);
 	}
	void getelement(int i,int j,int value)
	{
		p[i][j]=value;
	}
	void showdata(int n1,int n2)
	{
		r=n1;
		c=n2;
		for(int i=0 ; i<n1 ; i++ )
		{
			cout<<endl;
			for(int j=0 ; j<n2 ; j++)
			{
				cout<<p[i][j]<<"\t";
			}
		}
		cout<<endl;
	}
	matrix operator*(matrix m1)
	{
		matrix c1(r,c);
		for (int i = 0; i < r; ++i)
		{
			for (int j = 0; j < c; ++j)
			{
				c1.p[i][j]+=p[i][i]*m1.p[j][i];
			}
		}
		return c1;
	}
	matrix operator+(matrix m1)
	{
		matrix c1(r,c);
		for (int i = 0; i < r; ++i)
		{
			for (int j = 0; j < c; ++j)
			{
				c1.p[i][j]=p[i][i]+m1.p[j][i];
			}
		}
		return c1;
	}
	matrix operator-(matrix m1)
	{
		matrix c1(r,c);
		for (int i = 0; i < r; ++i)
		{
			for (int j = 0; j < c; ++j)
			{
				c1.p[i][j]=p[i][i]-m1.p[j][i];
			}
		}
		return c1;
	}
};

int main()
{
	int n,m,value,determinant=0;
	cout<<"  Enter size of matrix."<<endl<<"  Rows: ";
	cin>>n;
	cout<<"  Columns: ";
	cin>>m;
	matrix m1(n,m),m2(n,m),m3(n,m);
	cout<<"  Please enter the values: "<<endl;
	for(int i=0 ; i<n ; i++){
		for(int j=0 ; j<m ; j++){
			cout<<"  Matrix1["<<i<<"]["<<j<<"] = ";
			cin>>value;
			m1.getelement(i,j,value);
		}
	}
	cout<<endl;
	for(int i=0 ; i<n ; i++)
	{
		for(int j=0 ; j<m ; j++)
		{
			cout<<"  Matrix2["<<i<<"]["<<j<<"] = ";
			cin>>value;
			m2.getelement(i,j,value);
		}
	}
	cout<<"  Matrix1 : "<<endl;
	m1.showdata(n,m);
	cout<<endl<<"  Matrix2 : "<<endl;
	m2.showdata(n,m);

	for(int i=0;i<n;i++)
	{
		for(int j=0;j<m;j++)
		{
			value=m1.getdata(i,j);
 			m3.getelement(j,i,value);
		}
	}
	
	cout<<"  Transpose of Matrix1 : "<<endl;
	m3.showdata(n,m);
	
	cout<<" Result of Multiplication of Matrix1 & Matrix2:"<<endl;
	m3=m1*m2;
	m3.showdata(n,m);
	
	cout<<" Result of Addition of Matrix1 & Matrix2:"<<endl;
	m3=m1+m2;
	m3.showdata(n,m);
	
	cout<<" Result of Subtraction of Matrix1 & Matrix2:"<<endl;
	m3=m1-m2;
	m3.showdata(n,m);
	return 0;
}