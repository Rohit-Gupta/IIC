%
% Measure of central tendency
%
% (1) Arithmatc mean (AM);(2) Median ; (3) Mode ; (4) Geometric Mean (GM)
%
% (5) Harmonic mean (HM)
%
% ARITHMATIC MEAN-----(A)
%
% Find the arithmatic mean of the marks obtained by 10 students of 10th
% class in mathematics. The marks obtained are :
%
% 25 , 30 , 21 , 55 , 47 , 10 , 15 , 17 , 45 , 35.
%
  N = 10;
  x=zeros(1,N);
  x = [25 30 21 55 47 10 15 17 45 35];
  l=length(x)
  s = 0.0;
  for i = 1:N
      s = s + x(i);
  end
  AM_1 = s/N;
  AM_2=sum(x)/N;

  d = [ AM_1  AM_2]

  %ARITHMATIC MEAN ------(B)
  %
  % Find the arithmatic mean from the following frequency table.
  %
  %______________________________________________________________
  %Marks             52   58   60   65   68   70   75
  %______________________________________________________________
  %No. of Students    7    5    4    6    3    3    2
  %______________________________________________________________

  N = 7;
  x = [52  58  60  65  68   70  75];
  f = [7  5  4  6  3  3  2];
  %
  %Formula used:  mean_x = sum(x.f)/sum(f)
  %
  P = dot(x , f);

  sum_f =sum(f);

  AM_1 = P/sum_f;

  s1 = 0.0;
  s2 = 0.0;
  for i = 1:7
      s1 = s1 + x(i)*f(i);
      s2 = s2 + f(i);
  end

  AM_2 = s1/s2;

  d_1 = [ AM_1 AM_2]

  % ARITHMATIC MEAN ---------(C)
  %
  %Calculate the arithmatic mean for the following data.
  %
  %______________________________________________________________
  %Class intervals      0-10   10-20   20-30  30-40  40-50  50-60
  %_______________________________________________________________
  %Frequency              4       8      14     10     9      5
  %_______________________________________________________________
  %
  %Create the middle value of the class interval
  %

  x1 = [0 10 20 30 40 50];
  x2 = [10 20 30 40 50 60];
  f = [ 4 8 14 10 9 5 ];
  N = length(x1);
  x = 0.5*(x1 + x2);

  s1 = 0.;
  s2 = 0.;
  for i  = 1: N
      s1 = s1 + x(i)*f(i);
      s2 = s2 + f(i);

  end
  AM_1 = s1/s2;

  P = dot(x,f);
  f_sum = sum(f);

  AM_2 = P/f_sum;

  d_2 = [AM_1 AM_2]

  % ARITHMATIC MEAN ------(D)
  %
  % Calculate the average income from the following data
  %__________________________________________________________________
  %Wages (in Rs.)   10-19  20-29  30-39  40-49   50-59   60-69  70-79
  %__________________________________________________________________
  % No. of Person     2       3      5      10      5      3       2
  %__________________________________________________________________
  %
  x1 = [10 20 30 40 50 60 70];
  x2 = [19 29 39 49 59 69 79];
  x = 0.5*(x1 + x2);
  N= length(x);
  f = [ 2 3 5 10 5 3 2];
  s1 = 0.;
  s2 = 0.;
  for i  = 1: N
      s1 = s1 + x(i)*f(i);
      s2 = s2 + f(i);

  end
  AM_1 = s1/s2;

  P = dot(x,f);
  f_sum = sum(f);

  AM_2 = P/f_sum;

  d_3 = [AM_1 AM_2]

  % Weighted Arithmatic Mean-------(E)
  %
  % W_mean = sum ( x(i)*w(i))/sum (w(i))
  %
  % A student gets the following percentage in B.A. in 2005:
  %
  % English- 70 ; hindi -65; Mathematics- 75 ; Physics- -70 ; Chemistry-66.
  %
  % Find the weighted arithmatic ,mean if weights are 2 , 2 , 4 , 3 and 4
  %
  % respectively

  x=[70 65 75 70 66];
  w=[2 2 4 3 4];
  N=length(x);
  s1 = 0.0;
  s2 = 0.0;
  for i = 1:N
      s1 = s1 + x(i)*w(i);
      s2 = s2 + w(i);
  end
  WM_1 = s1/s2

  w_x =dot(x,w);
  w_sum = sum(w);
  WM_2  = w_x/w_sum;

  d_4 = [WM_1 WM_2]

  % Combined Mean----------(F)

  % If we are given the mean of two series , and their size, then the
  % combined mean for the resultant series can be obtained by the formula
  %
  %     X_mean = (n1*x1_mean + n2*x2_mean)/(n1 + n2)
  %
  %  where X_mean- Combined mean of the two series
  %
  %  x1_mean - mean of the first series
  %
  %  x2_mean - mean of the second series
  %
  %  n1 - length of the first series
  %
  %  n2 - length of the second series

  %Ex: Average monthly production of a factory for the first 8 months is
  %2584 units and for the remaining four months it is 2416 units.
  %Calculate the average monthly production for the year.
  %
  x1_mean = 2584;
  x2_mean = 2416;
  n1 = 8;
  n2 = 4;
  X_mean = (n1*x1_mean + n2*x2_mean)/(n1 + n2)

  % Ex:  A firm of readymade garments makes both men's and women's shirts.
  % Its profit average is 6% of the sales. Its profits in men's shirts
  % average 8% of sales and women's shirts comprise 605 of output. what are
  % the average profits per sale rupee in women's shirts?

  X_mean = 6;
  x1_mean = 8;
  % x2_mean = ?
  n1 = 40;
  n2 = 60;
  % using the formula
  %
  % X_mean = (n1*x1_mean + n2*x2_mean)/(n1 + n2)
  %
  x2_mean = (6*100 -(40*8))/60
  %
  % average profit in women's shirt is x2_mean% of sales or
  %
  per_sale_rupee=x2_mean/100
