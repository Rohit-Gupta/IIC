#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cmath>

using namespace std;

class person
{
protected:
	char * name;
	int id;
public:
	person(const char *n, int i):id(i)
	{
		int len = strlen(n);
		name = new char[len+1];
		strcpy(name,n);
	}
	~person()
	{
		cout<<"  Person is deleted."<<endl;
	}
	void display_p()
	{
		cout<<"  Name : "<<name<<endl;
		cout<<"  ID : "<<id<<endl;
	}
};
class account:virtual protected person
{
protected:
	int pay;
public:
	account(const char * n, int i, int p):person(n,i),pay(p){};
	~account()
	{
		cout<<"  Account is deleted."<<endl;
	}
	void display_a()
	{
		display_p();
		cout<<"  Pay : "<<pay<<endl;
	}
};

class admin:virtual protected person
{
protected:
	int experience;
public:
	admin(const char * n, int i, int e):person(n,i)
	{
		experience = e;
	}
	~admin()
	{
		cout<<"  Admin is deleted."<<endl;
	}
	void display_ad()
	{
		display_p();
		cout<<"  Experience : "<<experience<<endl;
	}
};
class master:protected account, protected admin
{
public:
	master(const char * n, int i, int p, int e):account(n,i,p),admin(n,i,e),person(n,i){};
	~master()
	{
		cout<<"  Master is deleted."<<endl;
	}
	friend ostream & operator << (ostream &out, const master &c)
	{
		out<<"  Name : "<<c.name<<endl;
		out<<"  ID : "<<c.id<<endl;
		out<<"  Pay : "<<c.pay<<endl;
		out<<"  Experience : "<<c.experience<<endl;
		return out;
	}
};

int main(int argc, char const *argv[])
{
	master m("Rohit Gupta",14,100,15000);
	cout<<m;
	return 0;
}