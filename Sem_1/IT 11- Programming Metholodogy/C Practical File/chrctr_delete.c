/*	Q. Write a function which deletes all the occurrence of a character from a string. If should take two
		arguments a string and character (whose occurrence will be deleted from a string).	*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void delete_chr(char *str,char ltr)
{
	int i=0,j,len;
	len=strlen(str);
	while(str[i]!='\n')
	{
		if(str[i]==ltr)
		{
			for(j=i;j<=len;j++)
				str[j]=str[j+1];
			str[j]='\0';
		}
		
		else
			i++;
	}
}

void main()
{
	char ch='y',str_inpt[100],chr;
	system("clear");
	printf("\n\n\t\t\t Letter Deletion From a String Program.");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\t Enter String : ");
		fgets(str_inpt,100,stdin);
		printf("\n\n\t Enter Character to be deleted : ");
		scanf(" %c",&chr);
		delete_chr(str_inpt,chr);
		printf("\n\n\t Letter Deletion going on.. \n");
		sleep(1);
		printf("\n\n\t String after Deletion : %s",str_inpt);
		printf("\n\n\n\t Want to Delete more..(y/n) ?? : ");
		scanf(" %c",&ch);
	}
}
