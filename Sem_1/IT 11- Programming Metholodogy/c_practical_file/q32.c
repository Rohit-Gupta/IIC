#include<stdio.h>
long GCD(long,long);
void main()
{
	long x,y,h=0,l=0;
	printf("Enter the Two Integers:");
	scanf("%ld%ld",&x,&y);
	h=GCD(x,y);
	l=(x*y)/h;
	printf("Greatest Common Divisor of %ld and %ld = %ld\n",x,y,h);
	printf("Lowest Common Multiple of %ld and %ld = %ld\n",x,y,l);
}
long GCD(long x, long y)
{
	if(y==0)
		return x;
	while(y!=0)
	{
		if(x>y)
			x=x-y;
		else
			y=y-x;
	}
	return x;
}	
