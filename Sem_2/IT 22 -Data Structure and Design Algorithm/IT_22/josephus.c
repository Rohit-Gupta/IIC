#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct node
{
    struct node *next;//stroes address to next node
    char name[10];//stroes the name
}*start;//points to beginning of the list
typedef struct node *Nodeptr;

void add(char namePassed[])//add a node
{
	Nodeptr temp=(struct node *)malloc(sizeof(struct node));//newly declared node
	Nodeptr move;
	move=start;//points to the start
	if(temp==NULL)
	{
	    printf("OverFlow\n");
	    return;//can not allocate space so return
	}
	while((move->next)!=NULL)
		move=move->next;//move to appropriate place to add new node to end
	strcpy(temp->name,namePassed);
	temp->next=NULL;
	move->next=temp;
}
void main()
{
	char choice[20],begin[20];
	int count,ctr=0,pass=1;
	start=(Nodeptr)malloc(sizeof(struct node));
	printf("Enter the name \n");//Enter the begining node
	scanf("%s",start->name);
	start->next=NULL;
	printf("Enter end to stop entering data otherwise enter the name\n");
	scanf("%s",choice);
	while(strcmp(choice,"end")!=0)
	{
		add(choice);
		printf("Enter end to stop entering data otherwise enter the name\n");
		scanf("%s",choice);
	}
	Nodeptr move=start;
	while((move->next)!=NULL)
	{
		move=move->next;
	}	
	move->next=start;//Make the list circular by making the end point to start
	move=start;
	printf("Enter the count after which deletion is to happen and Enter the name from which we need to start\n");
	scanf("%d%s",&count,begin);
	while(strcmp(move->name,begin)!=0)
	{
		move=move->next;//move to the soldier which user told to start with
	}
	while(move->next!=move)//when move->next==move than we have only one soldier left
	{
		ctr++;
		if(ctr==count-1)//delete next node
		{
    			Nodeptr temp=move->next;
			move->next=temp->next;
			printf("The name eliminated in pass number %d is %s\n",pass,temp->name);
			free(temp);//free space
    			ctr=0;
    			pass++;
		}
		move=move->next;
	}
	printf("The Winner is %s\n",move->name);
}
