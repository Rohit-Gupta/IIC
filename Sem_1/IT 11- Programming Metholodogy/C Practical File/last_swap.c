#include<stdio.h>
void main()
{
	int arr[30],i=0,temp,n;
	char ch='y';
	system("clear");
	printf("Left Shifting of Array.");
	
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\t Enter size of array : ");
		scanf("%d",&n);
		printf("\n\t Enter Array. \n");
		for(i=0;i<n;i++)
		{
			printf("\t Element %d : ",(i+1));
			scanf("%d",&arr[i]);
		}
		
		temp=arr[n-1];
		arr[n-1]=arr[0];
		arr[0]=temp;
	
		for(i=0;i<n-2;i++)
		{
			temp=arr[i];
			arr[i]=arr[i+1];
			arr[i+1]=temp;
		}	
	
		printf("\n\t Array after swapping.\n");
		for(i=0;i<10;i++)
		{
				printf("\n\t Element %d : %d",i+1,arr[i]);
		}

		printf("\n\n\t Want to Test more ?? (y/n) : ");
		scanf(" %c",&ch);
	}
	printf("\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
}

/*	Output

Left Shifting of Array.

	 Enter size of array : 10

	 Enter Array. 
	 Element 1 : 1
	 Element 2 : 2
	 Element 3 : 3
	 Element 4 : 4
	 Element 5 : 5
	 Element 6 : 6
	 Element 7 : 7
	 Element 8 : 8
	 Element 9 : 9
	 Element 10 : 10

	 Array after swapping.

	 Element 1 : 2
	 Element 2 : 3
	 Element 3 : 4
	 Element 4 : 5
	 Element 5 : 6
	 Element 6 : 7
	 Element 7 : 8
	 Element 8 : 9
	 Element 9 : 10
	 Element 10 : 1

	 Want to Test more ?? (y/n) : n

 All rights are reserverd. Copyright © Abhilash Kant.

*/
