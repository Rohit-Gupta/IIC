#include <iostream>
#include <math.h>
using namespace std;

class RECTANGLE;

class POLAR
{
	int x;
	int y;
public:	
	POLAR();
	int getX()
	{
		return x;
	}
	int getY()
	{
		return y;
	}
	POLAR(int a, int b)
	{
		x=a;
		y=b;
	}
	void show()
	{
		cout<<"POLAR coordinates:"<<endl;
		cout<<x<<endl<<y<<endl;
	}
	POLAR(RECTANGLE);
};

class RECTANGLE
{
	int r;
	int a;
public:
	RECTANGLE();
	int getR()
	{
		return r;
	}
	int getA()
	{
		return a;
	}
	RECTANGLE(int c, int d)
	{
		r=c;
		a=d;
	}
	void show()
	{
		cout<<"RECTANGLE coordinates:"<<endl;
		cout<<r<<endl<<a<<endl;
	}
	RECTANGLE(POLAR);
};

POLAR::POLAR(RECTANGLE e)
{
	x=e.getR()*cos(e.getA());
	y=e.getR()*sin(e.getA());
}
RECTANGLE::RECTANGLE(POLAR p)
{
	a=atan(p.getY()/p.getX());
	r=sqrt(p.getX()*p.getX() + p.getY()*p.getY());
}

int main(int argc, char const *argv[])
{
	/* code */
	RECTANGLE R(5,3);
	POLAR P=R;
	P.show();

	POLAR P1(7,30);
	RECTANGLE R1=P1;
	R1.show();
	return 0;
}