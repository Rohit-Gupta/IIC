#include<iostream>
using namespace std;

struct a
{
	char name[30];
	float units;
	float amount;
}stu[5];

float cal(float un)
{
	float a;
	if(un<101)
		a=un*0.6;
	else if(un>101 && un<301)
		a=60+(un-100)*0.8;
	else if(un>300)
		a=220+(un-300)*0.9;

	if(a<51)
		return(50.0);
	else if(a>300)
		return(a*1.15);
	else
		return a;
}

int main()
{
	int i;
	for(i=0;i<5;i++)
	{
		cout<<"Enter the details of Consumer "<<i+1<<" :"<<endl;
		cout<<"Name :";
		cin>>stu[i].name;
		cout<<"No. of Units Consumed :";
		cin>>stu[i].units;
		stu[i].amount=cal(stu[i].units);
	}
	for(i=0;i<5;i++)
	{
		cout<<"\n\nThe Details are as follows:"<<endl;
		cout<<"Name :"<<stu[i].name<<endl;
		cout<<"No. of units consumed :"<<stu[i].units<<endl;
		cout<<"Amount :"<<stu[i].amount<<endl;
	}
	return 0;
}
