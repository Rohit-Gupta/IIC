/*	Q. WAP to input the date using scanf function. Print the month name.
	Ex: Date entered like 24/09/2013
	Output should be September
	Hint: Use array of pointers to strings to store month name.		*/

#include<stdio.h>
void main()
{
	char *p[12]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"},ch='y';
	int day,mnth,yr;
	system("clear");
	printf(" Month Display ");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\t Plz Enter Date(dd/mm/yyyy) : ");
		scanf("%d/%d/%d",&day,&mnth,&yr);
		printf("\n\t Month is : %s",p[mnth-1]);
		printf("\n\n\t Wish to check more..? (y/n) : ");
		scanf(" %c",&ch);
	}
	printf("\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
}

/*	Output

 Month Display 
	 Plz Enter Date(dd/mm/yyyy) : 09/03/1993

	 Month is : Mar

	 Wish to check more..? (y/n) : y

	 Plz Enter Date(dd/mm/yyyy) : 24/04/1996

	 Month is : Apr

	 Wish to check more..? (y/n) : n

 All rights are reserverd. Copyright © Abhilash Kant.

*/
