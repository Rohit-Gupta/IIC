#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int argc,char *argv[])
{
   if(3 != argc)
   {
	printf("Wrong no. of arguments\n");
	exit(0);
   }
   int max , i=0 , j=0 , l , fl ;
   max=atoi(argv[1]);
   char **s , str[100];
   s=(char **)malloc(max*sizeof(char *));
   if(NULL==s)
   {
	printf("Memory not available\n");
	exit(0);
   }
   memset(s,0,max);
   while( (i<max) && (0!=strcmp(str,"end")) )
   {
	printf("Enter the string%d : ",i+1);
	fgets(str,99,stdin);
	l=strlen(str);
	if('\n'==str[l-1])
		str[l-1]='\0';
	if(NULL!=strstr(str,argv[2]))
	{
		s[j]=(char *)malloc(l*sizeof(char));
		if(NULL==s[j])
		{
			printf("Memory not available\n");
			exit(0);
		}
		memset(s[j],0,l);
		strncpy(s[j],str,l);
		j++;
	}
	i++;
   }
   if(0==j)
   {
	printf("There are no strings with the given pattern '%s'\n",argv[2]);
	free(s);
	s=NULL;
	exit(0);
   }
   fl=j;
   printf("The strings with the given pattern '%s' are :\n",argv[2]);
   for(i=0;i<j;i++)
   {
	printf("%d. %s\n",i+1,s[i]);
	fl=fl+strlen(s[i]);
   }
   char *finalstr;
   finalstr=(char *)malloc(fl*sizeof(char));
   if(NULL==finalstr)
   {
	printf("Memory not available\n");
	exit(0);
   }
   memset(finalstr,0,fl);
   for(i=j-1;i>=0;i--)
   {
	strcat(finalstr,s[i]);
	strcat(finalstr,",");
   }
   printf("The final string concatenated backwards is :\n");
   printf("%s\n",finalstr);
   free(finalstr);
   finalstr=NULL;
   for(i=0;i<j;i++)
   {
	free(s[i]);
	s[i]=NULL;
   }
   free(s);
   s=NULL;
   return 0;
}
