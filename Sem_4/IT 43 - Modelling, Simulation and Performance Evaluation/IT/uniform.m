%pd1 = makedist('Uniform');
%pd2 = makedist('Uniform','lower',-2,'upper',2);
%pd3 = makedist('Uniform','lower',-2,'upper',1);


% Compute the pdfs
x = -3:.01:3;
pdf1 = pdf(x,1);
pdf2 = pdf(x,2);
pdf3 = pdf(x,3);

% Plot the pdfs
figure;
stairs(x,pdf1,'r','LineWidth',2);
hold on;
stairs(x,pdf2,'k:','LineWidth',2);
stairs(x,pdf3,'b-.','LineWidth',2);
ylim([0 1.1]);
legend({'lower = 0, upper = 1','lower = -2, upper = 2',...
    'lower = -2, upper = 1'},'Location','NW');
hold off;

