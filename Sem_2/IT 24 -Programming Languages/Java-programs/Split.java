import java.util.*;

public class Split {
	String s ;
	
	
	Split(){
	 this.s = "Split-It";
 		}
	
	public void get(String a)
	{
		this.s=a;
	}
	
	
	
	public void stringSplit()
	{
		System.out.println("\"SPLIT METHOD\"");
		String[] ss = this.s.split("-");
		for(String sss : ss)
		System.out.println(sss);
	}
	
	public void stringToken()
	{
		System.out.println("\"Tokenizer METHOD\"");
		//StringTokenizer token = new StringTokenizer(this.s,"!*^-");//Multiple Strings
		StringTokenizer token = new StringTokenizer(this.s,"-");
		while(token.hasMoreElements())
		{
			System.out.println(token.nextElement().toString());
		}
	}
	
	
	public static void main(String[] args)
	{
		Split a = new Split();
		System.out.println("Enter the string to split");
		Scanner sc = new Scanner(System.in);
		String g = sc.next();
		sc.close();
		a.get(g);
		a.stringSplit();
		a.stringToken();
		
		
	}
}
