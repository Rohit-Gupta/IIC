import java.time.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

      
       public class TimeDateApplet extends JApplet {
    	  private TimeZoneList l = new TimeZoneList(); 
    	  private JTextField see = new JTextField(15);
    	  private JTextField zdate = new JTextField(15);
    	  private JTextField ztime = new JTextField(15);
    	  private JComboBox tz = new JComboBox();
    	  private JButton b = new JButton("Search");
    	  private String chk = "";
    	   
         public void init() {
        	setBackground(Color.black);
        	l.getList();
        	for(String item :l.ids)
        		tz.addItem(item);
        	b.addActionListener(new ActionListener() {
        	      public void actionPerformed(ActionEvent e) {
        	       chk = tz.getSelectedItem().toString();
        	  
        	ZoneId z =ZoneId.of(chk);
    		ZonedDateTime mytime = ZonedDateTime.now(z);
    		String d= mytime.toString();
    		String date =d.substring(0, 10);
    		String time = d.substring(11,19);
    		see.setText("Zone:" + chk);
    		zdate.setText(date);
    		ztime.setText(time);
        	      }
    	    });
        	
        	Container cp = getContentPane();
            cp.setLayout(new FlowLayout());
            cp.add(tz);
            cp.add(b);
            cp.add(see);
            cp.add(zdate);
            cp.add(ztime);
          
         }
         
         public static void main(String[] args) {
      		run(new TimeDateApplet(),400,100);
      	  }
       
       public static void run(JApplet applet, int width, int height) {
      	    JFrame frame = new JFrame();
      	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      	    frame.getContentPane().add(applet);
      	    frame.setSize(width,height);
      	    applet.init();
      	    applet.start();
      	    frame.setVisible(true);
     	  }
       }
       
