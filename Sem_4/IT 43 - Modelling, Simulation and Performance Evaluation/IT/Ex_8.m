%
%
% Four coins are tossed 160 times. The number of times x heads occur is
% given below:
%___________________________________________________________
% X : 0  1  2  3  4  
%___________________________________________________________
% f : 8  34  69  43  6
%____________________________________________________________
%
%Find a binomial distribution to this data on the hypothesis that coins are
%unbiased.

%Here we are given:

p = 1/2; q = 1/2; n = 4 ; 

x = [ 0  1  2  3  4 ];
f = [8  34  69  43  6];

s = 0.0;
N = length (f);
for i = 1:N
    s = s + f(i);
end
sum_f = s;

% By binomial distribution : P(x) = nCx p^x q^(n-x)

f_0 = s*(q^n);
F =[];
for k = 1: n
    P = 160*nchoosek(n,k)*(p^k)*(q^(n-k));
    F = [F;P]
end
%
F_final = [f_0 F(1) F(2) F(3) F(4)]
%
f
%
%
% Five coins are tossed 100 times. The number of times x heads occur is
% given below:
%___________________________________________________________
% x : 0  1  2  3  4  5
%___________________________________________________________
% f : 2 14 20 34 22  8
%____________________________________________________________
%
%Find a binomial distribution to this data.

x = [0  1  2  3  4  5];
f = [2 14 20 34 22  8];

L = length(x);

% 
% Given that     n = 5 ;
%

n =5;

s1 = 0.0;
s2 = 0.0;
for i = 1:L
    s1 = s1 + f(i);
    s2 = s2 + f(i)*x(i);
end
sum_f = s1
sum_fx = s2
mean_x = sum_fx/sum_f

N = sum_f;

%
% To find the binomial distribution , we need p
%
% Top determine p , use the relation np = mean_x
%
p = mean_x/n;

q = 1 - p;
%
%Expected frequency is f_ex = N * nCk * p^k * q^(n-k)
%
ex_f =[];

for k = 0:n
    c = nchoosek(n,k)
    fex = N *c*(p^k)*(q^(n-k))
    ex_f = [ex_f;fex];
end
final_f = [ ex_f(1) ex_f(2) ex_f(3) ex_f(4) ex_f(5)]

f

%
%                      POISSON DISTRIBUTION
%
% Find Poisson distribution for the following data and calculate the
% expected frequencies
%____________________________________________________________
% x  :  0    1  2   3  4
%____________________________________________________________
% f  : 109  65  22  3  1
%____________________________________________________________
x = [ 0 1 2 3 4];
f = [ 109 65 22 3 1];

s1 = 0.0;
s2 = 0.0;
n = length(f);

for i = 1 : n
    s1 = s1 + f(i);
    s2 = s2 + f(i) * x(i);
end
sum_f = s1;
sum_fx = s2;
lambda = sum_fx/sum_f;

N = sum_f;

%
%Expected frequency ex_f = N * [exp(-lambda)*lambda^x]/x!
%
f_0 = N * (e^(-lambda) * lambda^x )/x