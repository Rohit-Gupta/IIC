/*	Bresenham's line*/
#include <stdio.h>
#include <graphics.h>
#define ABS(x) (x>0?x:-(x))
#define ROUND(x) ((int) (x+.5))
#define VALIDX(x) ((x)>=0 && (x)<=getmaxx())
#define VALIDY(y) ((y)>=0 && (y)<=getmaxy())
void myline(int x1,int y1,int x2,int y2);
int main()
{
	int gd=DETECT,gm;
	int x1,y1,x2,y2;   
    printf("Enter initial point (x,y) : \n");
    scanf("%d%*c%d",&x1,&y1);
    printf("Enter end point (x,y) : \n");
    scanf("%d%*c%d",&x2,&y2);
	initgraph(&gd,&gm,NULL);
	myline(x1,y1,x2,y2);
	getch();
	//delay(5000);
	closegraph();
	return 0;
}
void myline(int x1,int y1,int x2,int y2)
{
	int dy=ABS(y2-y1), dx=ABS(x2-x1),p,x=x1,y=y1;
	if (dx > dy) {
		int y_inc,xend;
		if(x1>x2 && y1>y2) x=x2,y=y2,xend=x1,y_inc=1;
		else if(x1>x2 && y1<y2) x=x2,y=y2,xend=x1,y_inc=-1;
		else if(x1<x2 && y1>=y2) x=x1,y=y1,xend=x2,y_inc=-1;
		else x=x1,y=y1,xend=x2,y_inc=1;
		
		for(p=2*dy-dx;x<xend;x++) {
			if(VALIDX(x) && VALIDY(y))putpixel(x,y,GREEN);
			if (p<0) p+=2*dy; 
			else y+=y_inc,p+=2*(dy-dx); 
		}
		if(VALIDX(x) && VALIDY(y))putpixel(x,y,GREEN);
	}
	else {
		int x_inc,yend;
		if(y1>y2 && x1>x2) x=x2,y=y2,yend=y1,x_inc=1;
		else if(y1>y2 && x1<x2) x=x2,y=y2,yend=y1,x_inc=-1;
		else if(y1<y2 && x1>=x2) x=x1,y=y1,yend=y2,x_inc=-1;
		else x=x1,y=y1,yend=y2,x_inc=1;

		for(p=2*dx-dy;y<yend;y++) {
			if(VALIDX(x) && VALIDY(y))putpixel(x,y,14);
			if (p<0) p+=2*dx; 
			else x+=x_inc,p+=2*(dx-dy); 
		}
		if(VALIDX(x) && VALIDY(y))putpixel(x,y,14);
	}
}
