
#include <iostream>
#include <cstring>

using namespace std;

class Employee
{
	int emp_ID;
	char emp_nm[30];
	int emp_sal;
public:
	Employee(int id, char *nm, int s)
	{
		emp_ID = id;
		strcpy(emp_nm,nm);
		emp_sal = s;
	}
	Employee & greater(Employee & E)
	{
		if(E.emp_sal >= emp_sal)
			return E;
		else
			return *this;
	}
	void display()
	{
		cout<<"\n  Employee ID : "<<emp_ID;
		cout<<"\n  Employee name : "<<emp_nm;
		cout<<"\n  Employee Salary : "<<emp_sal;
	}
};

int main()
{
	Employee E1(1,"Rohit",3000);
	E1.display();
	Employee E2(2,"Uma",3500);
	E2.display();
	Employee E3 = E1.greater(E2);
	cout<<"  Following has greater Salary "<<endl;
	E3.display();
	cout<<endl;
	return 0;
}