/*	Q. Write a program to count the number of words in a string and print each word on a separate
		line.		*/

#include<stdio.h>
#include<ctype.h>

void main()
{
	char str[100],ch='y';
	int i=0,count=0;

	system("clear");
	printf("\n\t\t\t Word Breaker & Counter ");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\t Plz enter String : ");
		fgets(str,100,stdin);
		i=0;
		while(str[i]!='\n')
		{
			if(isspace(str[i]))
			{
				str[i++]='\n';
				count++;
			}
			else
				i++;
		}

		printf("\n\n\t Word Counting going on ..\n");
		sleep(1);
		printf("\n\t Now String is : %s",str);
		printf("\n Want to Break more Strings.. (y/n) ?? : ");
		scanf(" %c",&ch);
	}
}

