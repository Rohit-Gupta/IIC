n=1;
d=1;
xnew=1;
while d>0.0001 & n<1000
  n=n+1;
  xold=xnew;
  xnew=cos(xold);
  d=abs(xnew-xold);
end
[x,xnew,xold]