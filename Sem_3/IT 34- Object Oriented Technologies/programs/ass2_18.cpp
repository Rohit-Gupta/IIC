#include <iostream>
using namespace std;

class base
{
protected:
	int x;
	int y;
};

class derived:protected base
{
	int area;
	int a,b;
public:
	derived(int x, int y): a(x), b(y)
	{
		area=x*y;
		cout<<"\nArea:"<<area;
	}
};

int main(int argc, char const *argv[])
{
	/* code */
	derived d(10,12);
	return 0;
}