#include<iostream>
using namespace std;

class bank
{
	char cust_nm[30];
	static long acc_no;
	char acc_type[30];
	float bal;
	public:
	void create()
	{
		cout<<"\nEnter the Name of the Account Holder :";
		cin>>cust_nm;
		cout<<"\nEnter the Account type you want to create :";
		cin>>acc_type;
		bal=1000;
		acc_no++;
		display();	
	}
	void deposit()
	{
		float d;
		display();
		cout<<"\nEnter the amount to be deposited :";
		cin>>d;
		bal+=d;
		display();
	}
	void withdraw()
	{
		float e;
		display();
        cout<<"\nEnter the amount to withdraw :";
        cin>>e;
        bal-=e;
		display();
	}
	void display()
	{
		cout<<"\nName :"<<cust_nm;
		cout<<"\nAccount Number :"<<acc_no;
		cout<<"\nAccount Type :"<<acc_type;
		cout<<"\nBalance :"<<bal;
	}
};

long bank::acc_no=32200000;

int main()
{
	bank b;
	int ch;
	char choice;

	do
	{
		cout<<"MENU ";
		cout<<"\n1. Create Account ";
		cout<<"\n2. Deposit Cash ";
		cout<<"\n3. Withdraw Cash ";
		cout<<"\n\nPlease select one ";
		cin>>ch;
		
		switch(ch)
		{
			case 1:b.create();
				break;
			case 2:b.deposit();
				break;
			case 3:b.withdraw();
				break;
			default:cout<<"\nWrong Choice ";
		}
		cout<<"\nDo you want to continue :";
		cin>>choice;
	}while(choice=='y');
	return 0;
}
