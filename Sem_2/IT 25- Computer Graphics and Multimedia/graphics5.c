/*	flood fill 4 connected algorithm*/
#include <stdio.h>
#include <graphics.h>
void floodfill4(int x,int y,int fcolor,int bcolor);
int main()
{
	int gd=DETECT,gm;
	int left,top,right,bottom,fill;  
	printf("Lets draw a rectangle first\n"); 
    printf("Enter rectangle left upper corner (left,top) : ");
    scanf("%d%*c%d",&left,&top);
    printf("Enter rectangle right bottom corner(right,bottom) : ");
    scanf("%d%*c%d",&right,&bottom);
	printf("Enter the color to be filled : ");
    scanf("%d",&fill);
	initgraph(&gd,&gm,NULL);
	rectangle(left,top,right,bottom);
	right=(int)(left+right)/2,bottom=(int)(top+bottom)/2;
	//boundaryfill4((int)(left+right)/2,(int)(top+bottom)/2,30,getpixel(left,top));
	floodfill4(right,bottom,fill,getpixel(right,bottom));
	getch();
	closegraph();
	return 0;
}

void floodfill4(int x,int y,int fill,int oldcolor)
{
	if (getpixel (x, y ) == oldcolor){
		putpixel(x,y,fill);
		floodfill4 (x+1, y, fill, oldcolor);
		floodfill4 (x-1, y, fill, oldcolor) ;
		floodfill4 (x, y+1, fill, oldcolor);
		floodfill4 ( x , y-1, fill, oldcolor) ;
	}
	return;
}
