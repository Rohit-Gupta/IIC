#include<iostream>
using namespace std;

void ref(int &x, int &y, int &z)
{
	y=x-1;
	z=x+1;
}

int main()
{
	int x=10,y=0,z=0;
	
	cout<<"Values of :\nx="<<x<<"\ny="<<y<<"\nz="<<z<<endl;
	ref(x,y,z);
	cout<<"Values of :\nx="<<x<<"\ny="<<y<<"\nz="<<z<<endl;
	return 0;
}
