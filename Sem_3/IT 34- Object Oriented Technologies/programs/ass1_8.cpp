#include <iostream>

 using namespace std;

int main()
{
        cout << "Enter the Length of Array: ";
        int length,value,i;
        cin >> length;
        int *array1 = new int[length];
        int *array2 = new int[length];
        cout << "Array allocated " << length <<endl<<endl;
        for(i=0;i<length;i++)
        {
                cout<<"array1["<<i<<"]=";
                cin>>value;
                array2[i]=value;
        }
	cout<<endl;
        for(int j=0;j<length;j++)
        {
                cout<<"array2["<<j<<"]="<<array2[j]<<endl;
        }
        delete[] array1;
        delete[] array2;
        array1 = 0;
        array2 = 0;
        return 0;
}
