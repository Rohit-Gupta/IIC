import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
class CopyFile 
{
	public static void main(String[] args) 
	{
		FileInputStream fromFile;
		FileOutputStream toFile;
		try 
		{
			fromFile = new FileInputStream(args[0]);
			toFile = new FileOutputStream(args[1]);
		} 
		catch(FileNotFoundException e) 
		{
			System.err.println("File could not be copied: " + e);
			return;
		} 
		catch(ArrayIndexOutOfBoundsException e) 
		{
			System.err.println("Usage: CopyFile <from_file> <to_file>");
			return;
		}	
		try 
		{
			while (true) 
			{
				int i = fromFile.read();
				if(i == -1) 
					break;
				toFile.write(i);
			}
			System.out.println("Success");
		} 
		catch(IOException e) 
		{
			System.err.println("Error reading/writing.");
		}
		try 
		{
			fromFile.close();
			toFile.close();
		} 
		catch(IOException e) 
		{
			System.err.println("Error closing file.");
		}
	}
}
