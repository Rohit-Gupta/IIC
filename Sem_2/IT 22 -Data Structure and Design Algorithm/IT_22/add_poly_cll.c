//#include<iostream.h>                                    //HEADER FILES
#include<ctype.h>
#include<stdio.h>

struct term
{
float coeff;
int header,a,b,c;
struct term *next;
}*plast,*qlast,*temp;

FILE *file,*file1;

term * makelist(term *ptr)                                //MAKE CIRCULAR LINKED LIST
{
int i,j,frac=0,ind=-1,sign=1,power[3]={0,0,0};
char ch;
float dec=1.0,pre=0;
ptr=new term;
ptr->header=0;                                        //FIRST NODE (the header)
ptr->next=ptr;
do
{
j=fgetc(file);
if(j!=-1)
ch=j;
else
break;                                        //READ CHARACTER
if(isdigit(ch)&&(!frac)&&(ind==-1))                //MAKE COEFFICIENT
{
if(!pre)
pre=ch-'0';
else
pre=pre*10+ch-'0';
continue;
}
if(ch=='.')                                        //CHECK FOR A DECIMAL IN COEFFICIENT
{
frac=1;
continue;
}
if(isdigit(ch)&&(frac)&&(ind==-1))                //GET DECIMAL PART OF COEFFICIENT
{
dec/=10;
pre=pre+(ch-'0')*dec;
continue;
}
if(ch=='x')                                        //VARIABLE x APPEARS
{
ind=0;
}
else if(ch=='y')                                //VARIABLE y APPEARS
{
if((ind==0)&&(power[0]==0))
power[0]=1;
ind=1;
}
else if(ch=='z')                                //VARIABLE z APPEARS
{
if((ind==0)&&(power[0]==0))
power[0]=1;
else if((ind==1)&&(power[1]==0))
power[1]=1;
ind=2;
}
if(ch=='^'||ch==' ')                            //IDENTIFY POWER
continue;
if(isdigit(ch)&&(ind!=-1))                        //GET THE POWER OF PRECEEDING VARIABLE
{
if(!power[ind])
power[ind]=ch-'0';
else
power[ind]=power[ind]*10+ch-'0';
continue;
}
if(ch=='+'||ch=='-'||ch=='\n')                           //TERM ENDS HERE
{
if((ind==0)&&(power[0]==0))
power[0]=1;
else if((ind==1)&&(power[1]==0))
power[1]=1;
else if((ind==2)&&(power[2]==0))
power[2]=1;
if(pre==0)
for(i=0;i<3;i++)
if(power[i]!=0)
{
pre=1;
break;
}
if(pre==0)
{
sign=-1;
continue;
}
pre*=sign;
sign=1;
if(ch=='-')
sign=-1;
term *temp=new term;                                //MAKE A NEW NODE IN CLL
temp->coeff=pre;
temp->a=power[0];
temp->b=power[1];
temp->c=power[2];
temp->header=1;
temp->next=ptr->next;
ptr->next=temp;
ptr=temp;
pre=0;                                                //REINITIALIZE ALL VARIABLES
frac=0;
dec=1.0;
ind=-1;
for(i=0;i<3;i++)
power[i]=0;
continue;
}
}while(ch!='\n');
return(ptr);
}

void showlist(term *ptr)                                        //DISPLAY THE CONTENTS OF CLL
{
file1=fopen("putdata.txt","wtr->next;
cout<<endl;
while(temp->header)
{
if(temp->coeff>0)
fprintf(file1,"%s%.2f"," + ",temp->coeff);
else
fprintf(file1,"%c%.2f",' ',temp->coeff);
if(temp->a)
{
fprintf(file1,"%c",'x');
if(temp->a!=1)
fprintf(file1,"%c%d",'^',temp->a);
}
if(temp->b)
{
fprintf(file1,"%c",'y');
if(temp->b!=1)
fprintf(file1,"%c%d",'^',temp->b);
}
if(temp->c)
{
fprintf(file1,"%c",'z');
if(temp->c!=1)
fprintf(file1,"%c%d",'^',temp->c);
}
temp=temp->next;
}
fclose(file1);
cout<<"\n";
}

int compare(term *p,term *q)                             //COMPARE TWO TERMS
{
if(q->header==0)
return -2;
if(p->a+p->b+p->c==q->a+q->b+q->c)
{
if(p->a==q->a)
{
if(p->b==q->b)
{
if(p->c==q->c)
return 0;
else if(p->c>q->c)
return 1;
else
return -1;
}
else if(p->b>q->b)
return 1;
else
return -1;
}
else if(p->a>q->a)
return 1;
else
return -1;
}
else if(p->a+p->b+p->c>q->a+q->b+q->c)
return 1;
else
return -1;
}

void add()                                                //ADD TWO POLYNOMIALS
{
term *p,*q,*q1,*q2;
p=plast;
p=p->next;

l1:
q=qlast->next;
q1=qlast;
while(compare(p,q)==-1)
{
q1=q;
q=q->next;
}

if(compare(p,q)==0)                                    //TERMS ARE EQUAL
{
q->coeff+=p->coeff;
if(q->coeff==0)
{
q2=q;
q1->next=q;
q=q->next;
delete(q2);
p=p->next;
}
else
{
q1=q;
p=p->next;
q=q->next;
}
if(p->header!=0)
goto l1;
else
return;
}

if((compare(p,q)==1)||(compare(p,q)==-2))            //TERM OF P NOT FOUND IN Q
{
q2=new term;
q2->coeff=p->coeff;
q2->a=p->a;
q2->b=p->b;
q2->c=p->c;
q2->header=1;
q2->next=q;
q1->next=q2;
q1=q2;
p=p->next;
if(p->header!=0)
goto l1;
else
return;
}
}

void main()                                    //EXECUTION STARTS HERE
{
file=fopen("getdata.txt","rmakelist(plast");
plast=plast->next;

qlast=makelist(qlast);
qlast=qlast->next;
add();

showlist(qlast);
cout<<"\n READ p AND q FORM getdata.txt"
<<"\n AND WROTE RESULT IN putdata.txt"
<<"\n OPEN FILE TO SEE RESULT\n";
fclose(file);
}
