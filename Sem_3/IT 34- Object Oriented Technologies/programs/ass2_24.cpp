
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cmath>

using namespace std;

class shape
{
	float len;
	float hei;
protected:
	float area;
public:
	shape()
	{
		len=0;
		hei=0;
		area=0;
	}
	virtual void get_data()
	{
		cout<<"  Enter length : ";
		cin>>len;
		cout<<"  Enter height : ";
		cin>>hei;
	}
	virtual int area_sh()
	{
		return len*hei;
	}
	virtual void display_area()
	{
		cout<<area<<endl;
	}
};

class triangle:public shape
{
public:
	void get_data()
	{
		shape::get_data();
	}
	void display_area()
	{
		area=0.5*area_sh();
		cout<<"\nArea of triangle is : "<<area;
	}
};

class rectangle:public shape
{
public:
	void get_data()
	{
		shape::get_data();
	}
	void display_area()
	{
		area=area_sh();
		cout<<"\nArea of triangle is : "<<area;
	}
};

int main(int argc, char const *argv[])
{
	int ch;
	rectangle r;
	triangle t;
	cout<<"\nMenu";
	cout<<"\n1.rectangle ";
	cout<<"\n2. triangle ";
	cout<<"\n3. Exit ";
	cout<<"\nEnter your choice : ";
	cin>>ch;
	if (ch==1)
	{
		r.get_data();
		r.display_area();
	}
	else if (ch==2)
	{
		t.get_data();
		t.display_area();
	}
	else if (ch==3)
	{
		exit(0);
	}
	else
		cout<<"\nPlease Enter correct choice ";
	return 0;
}