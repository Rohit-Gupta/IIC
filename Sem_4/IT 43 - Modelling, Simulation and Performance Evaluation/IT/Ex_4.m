%
% MODE
%
% Mode is the value in a series which occurs most frequently i.e., has the
% maximum frequency
%
% In other words , mode represents that value which is most frequent or
% typical or predominant.
%
% The mode of a distribution is the value at that point around which the
% items tends to be most heavily concentrated. It may be regarded as the
% most typical of a series of values.

%                         INDIVIDUAL SERIES
%
% If each term of the series occurs only once, then there is no mode.,
% otherwise the value that occurs the maximum number of times is known as
% mode.

%                               UNIMODAL
%
%Ex: Find the mode from the following data
%___________________________________________________________
% 2 4 5 7 7 7 9 6 8
%___________________________________________________________

x = [ 2 4 5 7 7 7 9 6 8];
M_1 = mode(x); % using the MATLAB command

count = 1;

N = length(x);


[rows, columns] = size(x);
%sorted_X = reshape(sort(x(:), 'descend'), [columns, rows])'
sorted_Y = reshape(sort(x(:), 'ascend'), [columns, rows])';

S = sorted_Y;
MX = [];

for i = 2:N
    if(S(i-1) == S(i))
        count = count + 1;
        Item = i ;
    end
end

M_2 = S(Item);
MX = [ M_1 M_2]

%                               BIMODAL
%
%Ex: Find the mode from the following data
%________________________________________________________________
% 3 8 11 34 9 8 8 3 4 6 3
%________________________________________________________________

x = [ 3 8 11 34 9 8 8 3 4 6 3];

M_1 = mode(x); % using the MATLAB command


N = length(x);


[rows, columns] = size(x);
%sorted_X = reshape(sort(x(:), 'descend'), [columns, rows])'
sorted_Y = reshape(sort(x(:), 'ascend'), [columns, rows])'

S = sorted_Y;

C = [];

for i = 2:N
    if(S(i-1) == S(i))
           
        C = [C;i];  %selecting the Item no. = i in sorted matrix S  
    end
  
end

M = S(C); % matrix containing duplicate elements

M = M([true diff(M)~=0]); % resulting matrix after eliminating duplicate 
                         % elements

M_2 = M % Two modal values ---- BIMODALITY


%                            MULTIMODAL CASE


%EX:  Find the mode from the following data
%________________________________________________________________
% 13  11  24  15  17  11  26  24  11  34  17  17  24  12  20  11  17
%____________________________________________________________________


x = [ 13  11  24  15  17  11  26  24  11  34  17  17  24  12  20  11  17 ];



M_1 = mode(x); % using the MATLAB command


N = length(x);


[rows, columns] = size(x);
%sorted_X = reshape(sort(x(:), 'descend'), [columns, rows])'
sorted_Y = reshape(sort(x(:), 'ascend'), [columns, rows])'

S = sorted_Y;

C = [];

for i = 2:N
    if(S(i-1) == S(i))
           
        C = [C;i];  %selecting the Item no. = i in sorted matrix S  
    end
  
end

M = S(C) % matrix containing duplicate elements

M = M([true diff(M)~=0]); % resulting matrix after eliminating duplicate 
                         % elements

                         
M_2 = M % 3 modal values -------- MULTIVALUED CASE


%                            DISCRETE SERIES

%
% INSPECTION METHOD
%
% Here the variable that occur with the maximum frequency is the mode.
%
%Ex: Find the mode for the following data.
%___________________________________________________________
% x:     4   7   11    16    25
%___________________________________________________________
% f:     3   9   14   21    13
%___________________________________________________________

x = [4   7   11    16    25 ];

f =  [3   9   14   21    13 ];

[v,ind]=max(f)
M_1 = x(ind)


%                              CONTINUOUS SERIES

%
%The modal class can be determined either by inspection.
%After finding the modal class , we calculate the mode by the following
%formula


%       Mode = L + ((f - f1)/(2f - f1 - f2))*C
%
% L : Lower limit of the modal class
%
% f : Maximum frequency 
%
% f1: Maximum frequency of the class preceding value
%
% f2: Maximum frequency of the class succeeding value
%
%  C : Class interval size

%Ex: From the following table, calculate the mode
%____________________________________________________________
% Class interval: 0-10  10-20  20-30  30-40  40-50  50-60  60-70
%_______________________________________________________________
% Frequency:       9      11    14      20     15     10     8
%________________________________________________________________

%Here
L = 30; % Lower limit of the modal class
f = 20; % Maximum frequency 
f1 = 14;% Maximum frequency of the class preceding value
f2 = 15; %Maximum frequency of the class succeeding value
C = 10;  %Class interval size

% Using the formula

M_1 = L + ((f-f1)/(2*f-f1-f2))*C

% In the foregoing example note that the difference is 0 between the upper
% limit of an interval and the lower limit of the next interval. In case
% the difference is 1, we have to deduct  0.5 from the lower and add 0.5 to
% the upper limit of the next interval.

%EX: Find the mode from the following data
%___________________________________________________________________
% Class Freq. : 0-4  5-9  10-14  15-19  20-24  25-29  30-34
%____________________________________________________________________
%Frequency:      3    11    21     32     25     17     9
%____________________________________________________________________

%Here
L = 14.5; 
f = 32 ;
f1 = 21 ;
f2 = 25 ;
C = 5 ; % Why?
M_1 = L + ((f-f1)/(2*f-f1-f2))*C




