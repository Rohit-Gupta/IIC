/* Q. String library function sprintf is used to convert variables of any data type to strings.
	WAP to convert integer and float values to string ?
*/

#include<stdio.h>
#include<string.h>
void main()
{
	char str[100],ch='y',ch1;
	int i,len;
	float f;
	system("clear");
	printf(" Sprintf Test, Convert int & float into String ");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\t Menu..!!\n\n\t 1. Int conversion to String.\n\n\t 2. Float conversion to String.\n\n\t Plz eneter your choice : ");
		scanf(" %c",&ch1);
		switch(ch1)
		{
			case '1'  :	printf("\n\n\t Plz enter int value that need to be convert into string : ");
					scanf("%d",&i);
					sprintf(str,"%d",i);
					break;
			case '2'  : 	printf("\n\n\t Plz enter float value that need to be convert into string : ");
					scanf("%f",&f);
				 	sprintf(str,"%f",f);
					break;
			default :	printf("\n\n\t Sorry wrong input..!! ");
					break;
		}
		
		len=strlen(str);
		printf("\n\n\t Value in string are : ");
		for(i=0;i<len;i++)
			printf("\t %c",str[i]);
		printf("\n\n\t Wish to continue more..?? (y/n) : ");
		scanf(" %c",&ch);
	}
	printf("\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
}

