#include<iostream>
#include <string.h>
using namespace std;

class emp
{
	int emp_id;
	char emp_nm[50];
	char emp_dob[30];
	double emp_sal;
	public:
		emp()
		{
			emp_id=0;
			strcpy(emp_nm,"Null");
			strcpy(emp_dob,"1/1/1900");
			emp_sal=1000;
		}
		emp(emp &e)
		{
			emp_id=e.emp_id;
			strcpy(emp_nm,e.emp_nm);
			strcpy(emp_dob,e.emp_dob);
			emp_sal=e.emp_sal;
		}
		emp(int i)
		{
			cout<<"\nEnter the Employee ID: ";
			cin>>emp_id;
			cout<<"\nEnter the Employee Name: ";
			cin>>emp_nm;
			cout<<"\nEnter the Employee Date of Birth: ";
			cin>>emp_dob;
			cout<<"\nEnter the Employee Salary: ";
			cin>>emp_sal;

			cout<<"\n\nThe Employee Details are :"<<endl;
		}
		void display()
		{
			cout<<"\nID :"<<emp_id;
			cout<<"\nName :"<<emp_nm;
			cout<<"\nDate of Birth :"<<emp_dob;
			cout<<"\nSalary :"<<emp_sal;
		}
};

int main()
{
	emp e();
	emp e1(1);
	e1.display();
	emp e2(e1);
	e2.display();
	return 0;
}