class Reverse
{
	public static void main(String args[])
	{
		try
                {
			int a=Integer.parseInt(args[0]);
			int c=0,x;
			while(a>0)
			{
				x=a%10;
				c=c*10+x;
				a/=10;
			}
			System.out.println(c);
		}
		catch(Exception e) {}
	}
}