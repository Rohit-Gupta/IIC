N=10000;
x=rand(1,N);        %generate 10000 random numbers
bins=(0.05:0.1:0.95);       %define 10 bins with gape of 0.1
[y,xvalues]=hist(x,bins);   %generate the histogram
y=y/N;
bar(xvalues,y);             %generate the bar graph
xlabel('x');
ylabel('Relative Frequency')