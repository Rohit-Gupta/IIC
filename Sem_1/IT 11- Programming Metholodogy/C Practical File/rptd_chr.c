/*	Q. Write a function that returns the index of first non repeated character in a string. If all the
		characters are repeated then the function returns -1.		*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int rpt_chckr(char *str)
{
	int flag=0,pos=0,i=0;
	while(str[i])
	{
		if(str[i]==str[i+1] || str[i]=='\n' || str[i]=='\0')
			flag=0,i++;
		else
		{	pos=i;
			flag=1;
			break;
		}
	}
	
	if(flag==0)
		return -1;
	else
		return pos;
}

void main()
{
	char str_inpt[100],ch='y';
	int chck=0;
	system("clear");
	printf("\n\t\t\t Character Repeatition Checker");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\t Enter the String : ");
		fgets(str_inpt,100,stdin);
		chck=rpt_chckr(str_inpt);
		if(chck==-1)
			printf("\n\n\t Function rpt_chckr() returns %d, it means all the character in string %s are Repeated",chck,str_inpt);

		else
			printf("\n\n\t Function rpt_chckr() returns %d, it means it means in string %s, first character change is at %d postion.",chck,str_inpt,chck+1);

		printf("\n\n\t Want to check more.. (y/n) ?? : ");
		scanf(" %c",&ch);
	}
}
