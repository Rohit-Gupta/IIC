#include<stdio.h>
void main()
{
	int num,rev=0,copy;
	char ch='y';
	system("clear");
	printf(" Number Reverse. ");
	while(ch=='y'||ch=='Y')
	{
		printf("\n\n\t Enter the number : ");
		scanf("%d",&num);
		copy=num;
		rev=0;
		while(num>0)
		{
			rev+=num%10;
			rev*=10;
			num=num/10;
		}
		rev/=10;
		printf("\n\t Reverse of the number %d is %d.",copy,rev);
		printf("\n\n\t Want to continue more ?? (y/n) : ");
		scanf(" %c",&ch);
	}
	printf("\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
}

/*	Output

Number Reverse. 

	 Enter the number : 12345

	 Reverse of the number 12345 is 54321.

	 Want to continue more ?? (y/n) : y


	 Enter the number : 54321

	 Reverse of the number 54321 is 12345.

	 Want to continue more ?? (y/n) : y


	 Enter the number : 87694536

	 Reverse of the number 87694536 is 63549678.

	 Want to continue more ?? (y/n) : n

 All rights are reserverd. Copyright © Abhilash Kant.

*/
