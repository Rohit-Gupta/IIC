#include <iostream>
#include <math.h>
using namespace std;

class RECTANGLE
{
	int r;
	int a;
public:
	int getR()
	{
		return r;
	}
	int getA()
	{
		return a;
	}
	friend istream & operator >>(istream &in, RECTANGLE &r1)
	{
		cout<<"\nEnter the rectangular componenets :";
		in>>r1.r>>r1.a;
	}
};

class POLAR
{
	int x;
	int y;
public:
	POLAR()
	{
		x=y=0;
	}	
	int getX()
	{
		return x;
	}
	int getY()
	{
		return y;
	}
	friend ostream & operator <<(ostream &out, const POLAR &p)
	{
		out<<"Polar components:";
		out<<p.x<<endl<<p.y<<endl;
		return out;
	}
	POLAR(RECTANGLE e)
	{
		x=e.getR()*cos(e.getA());
		y=e.getR()*sin(e.getA());
	}
};

int main(int argc, char const *argv[])
{
	/* code */
	RECTANGLE R;
	cin>>R;
	POLAR P=R;
	cout<<P;
	return 0;
}