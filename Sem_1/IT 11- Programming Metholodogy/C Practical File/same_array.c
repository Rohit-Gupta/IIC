#include<stdio.h>
void main()
{
	int arr[30],size,flag=0,i=0;
	char ch='y';
	system("clear");
	printf(" Program to Check all the elements of an Array are Distinct.");
	while(ch=='y'|| ch=='Y')
	{
		printf("\n\n\t Enter size of the array : ");
		scanf("%d",&size);
		printf("\n\t Enter Array. \n");
		for(i=0;i<size;i++)
		{
			printf("\t Element %d : ",i+1);	
			scanf("%d",&arr[i]);
		}
		
		for(i=1;i<size;i++)
		{
			if(arr[i]==arr[0])
				continue;
			else
			{	flag=1;
				break;
			}
		}

		if(flag==0)
			printf("\n\t All the elements present in array are Distinct. \n");
		else
			
			printf("\n\t All the elements present in array are not distinct. \n");

		printf("\n\n\t Want to chechk more ? (y/n) : ");
		scanf(" %c",&ch);

	}
	printf("\n All rights are reserverd. Copyright © Abhilash Kant.\n\n");
}

/*	Output

 Program to Check all the elements of an Array are Distinct.

	 Enter size of the array : 5

	 Enter Array. 
	 Element 1 : 2
	 Element 2 : 2
	 Element 3 : 2
	 Element 4 : 2
	 Element 5 : 2

	 All the elements present in array are Distinct. 


	 Want to chechk more ? (y/n) : y


	 Enter size of the array : 2

	 Enter Array. 
	 Element 1 : 2
	 Element 2 : 1

	 All the elements present in array are not distinct. 


	 Want to chechk more ? (y/n) : n

 All rights are reserverd. Copyright © Abhilash Kant.

*/
