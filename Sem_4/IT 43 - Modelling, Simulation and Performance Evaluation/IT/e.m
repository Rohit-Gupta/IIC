clc;
clear;
a=[10,29,20,17,9,16,17,4,8,8,8,20,18,10,24,5,11,12,15,47];
b=[7,31,1,5,38,3,3,21,16,16,3,38,81,5,14,1,20,12,94,44];

figure
cdfplot(a)
hold on
cdfplot(b)

r=rand(1,20);
t=(-1/22.65)*log(r);
t1=(-1/15.4)*log(r);

figure
cdfplot(t)
hold on
cdfplot(t1)