import socket           #import the  socket library
import sys              #import the  system library

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('localhost', 10000)
print >>sys.stderr, 'connecting to %s port %s' % server_address
sock.connect(server_address) #connect to localhost and port 100000
try:

    # Send data
    message = 'This is the message.  It will be repeated.'
    print >>sys.stderr, 'sending "%s"' % message
    sock.sendall(message)           #send all the data

    # Look for the response
    amount_received = 0         #number of bytes recieved from server
    amount_expected = len(message)  #expected length of message

    #recieve data from server
    while amount_received < amount_expected:
        data = sock.recv(16)
        amount_received += len(data)
        #to show what data is recieved
        print >>sys.stderr, 'received "%s"' % data

finally:
    print >>sys.stderr, 'closing socket'
    #finally closing the socket
    sock.close()
