import java.io.*;
class Reading
{
	public static void main(String args[])
	{
		DataInputStream in=new DataInputStream(System.in);
		int inN=0;
		float floN=0.0f;
		try
		{
			System.out.println("enter an integer:");
			inN=Integer.parseInt(in.readLine());
			System.out.println("enter an float no.");
			floN=Float.valueOf(in.readLine()).floatValue();
		}
		catch(Exception e) {};
		System.out.println("inN="+inN);
		System.out.println("floN="+floN);
	}
}