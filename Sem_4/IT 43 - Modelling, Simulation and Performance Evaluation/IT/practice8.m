% Aim: to solve the Lorenz equations
% dx/dt=sigma*(y-x); dy/dt=-x*z+r*x-y; dz/dt=x*y-b*z
sig=10.0; b=8/3; r=20;
% Parameters
t(1)=0.0;
% Initial t
x(1)=0.1; y(1)=0.1; z(1)=0.1;
% Initial x,y,z
dt=0.005;
% Time step
nn=10000;
% Number of time steps
for k=1:nn
% Time loop
fx=sig*(y(k)-x(k));
% RHS of x equation
fy=-x(k)*z(k)+r*x(k)-y(k);
% RHS of y equation
fz=x(k)*y(k)-b*z(k);
% RHS of z equation
x(k+1)=x(k)+dt*fx;
% Find new x
y(k+1)=y(k)+dt*fy;
% Find new y
z(k+1)=z(k)+dt*fz;
% Find new z
t(k+1)=t(k)+dt;
% Find new t
end
% Close time loop
plot(t,x)
% Plot x vs t
title('CFB')
% Title
xlabel('t'); ylabel('x');
% Label axes