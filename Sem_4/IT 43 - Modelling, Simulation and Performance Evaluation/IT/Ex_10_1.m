% To solve the system of equations dx1/dt=x2; dx2/dt=-x11
% It uses the file tuto2.m
clear all
% Clear all
t=[0 10];
% Time span
xinit=[1;0];
% Initial condition
[t,x]=ode45(@tuto2,t,xinit);
% Integrate equations
subplot(131)
plot(t,x(:,1))
% Plot x1 vs t
title('CFB'); xlabel('t'); ylabel('x1');
subplot(132)
plot(t,x(:,2))
% Plot x2 vs t
title('CFB'); xlabel('t'); ylabel('x2');
subplot(133)
plot(x(:,1),x(:,2))
% Plot x2 vs x1
title('CFB'); xlabel('x1'); ylabel('x2');