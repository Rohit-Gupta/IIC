/*	Midpoint cicle bresenham algorithm*/
#include <stdio.h>
#include <graphics.h>
#define VALIDX(x) ((x)>=0 && (x)<=getmaxx())
#define VALIDY(y) ((y)>=0 && (y)<=getmaxy())
void mycircle(int h,int k,int r);
int main()
{
	int gd=DETECT,gm;
	int h,k,r;   
    printf("Enter midpoint (h,k) of circle : \n");
    scanf("%d%*c%d",&h,&k);
    printf("Enter radius : \n");
    scanf("%d",&r);
	initgraph(&gd,&gm,NULL);
	//printf("%d,%d\n",getmaxx(),getmaxy());
	mycircle(h,k,r);
	getch();
	//delay(5000);
	closegraph();
	return 0;
}
void mycircle(int h,int k,int r)
{
	int x=0,y=r,d=1-r;
	for(;x<=y;x++){
		if (d<0)
			d+=(2*x +3);
		else
			d+=(2*(x-y)+1),y--;
		if(VALIDX(x+h) && VALIDY(y+k)) putpixel(x+h,y+k,GREEN);
		if(VALIDX(y+h) && VALIDY(x+k)) putpixel(y+h,x+k,GREEN);
		if(VALIDX(-x+h) && VALIDY(y+k))	putpixel(-x+h,y+k,GREEN);
		if(VALIDX(y+h) && VALIDY(-x+k))	putpixel(y+h,-x+k,GREEN);
		if(VALIDX(x+h) && VALIDY(-y+k))	putpixel(x+h,-y+k,GREEN);
		if(VALIDX(-y+h) && VALIDY(x+k))	putpixel(-y+h,x+k,GREEN);
		if(VALIDX(-x+h) && VALIDY(-y+k))	putpixel(-x+h,-y+k,GREEN);
		if(VALIDX(-y+h) && VALIDY(-x+k))	putpixel(-y+h,-x+k,GREEN);
	}
}
