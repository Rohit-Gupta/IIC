
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100
EXAMPLE     SEGMENT
    OPR1    DW  0000H
    OPR2    DW  0005H
    RESULT  DW  ?
    ASSUME  CS:EXAMPLE,DS:EXAMPLE
    START   : MOV AX,CS
              MOV DS,AX
              MOV AX,OPR1  
              MOV DX,OPR2
              ADD AX,DX  
              MOV RESULT,AX
       
              HLT
EXAMPLE   ENDS
END           START

ret




