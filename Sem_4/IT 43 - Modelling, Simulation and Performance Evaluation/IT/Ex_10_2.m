% To solve the Lorenz equations and show the butterfly effect
% It uses the file lorenz.m
clear all
% Clear all variables
t=[0 50];
% Time window
x1init=[0.1;1.2;1.2];
% Initial condition for x1
[t,x1]=ode45(@lorenz,t,x1init);
% Find first trajectory x1
x2init=[0.101;1.2;1.2];
% Initial condition for x2
[t,x2]=ode45(@lorenz,t,x2init);
% Find second trajectory x2
nn=length(t);
% Number of time steps used
for n=1:nn
% Loop to find the separation d
dx=x2(n,1)-x1(n,1);
% between x1 and x2
dy=x2(n,2)-x1(n,2);
dz=x2(n,3)-x1(n,3);
d(n)=sqrt(dx*dx+dy*dy+dz*dz);
end
% Plot x1 and x2 vs t
subplot(131)
plot(t,x1(:,1),'b')
hold on
subplot(132)
plot(t,x2(:,1),'r')
title('CFB'); xlabel('t'); ylabel('x1 and x2');
% Plot log(d) vs t
subplot(133)
plot(t,log10(d))
% Plot log(d) vs time
title('CFB'); xlabel('t'); ylabel('log10(d)');