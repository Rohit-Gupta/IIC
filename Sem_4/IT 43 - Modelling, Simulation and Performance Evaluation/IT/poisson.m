x = 0:15;
y = poisspdf(x,5);    %lambda=5
plot(x,y,'b*')
hold on
y = poisspdf(x,10);   %lambda=10
plot(x,y,'r*')
hold on
y = poisspdf(x,1);   %lambda=1
plot(x,y,'g*')    
legend({'lambda = 5','lambda = 10','lambda = 1'})
hold off