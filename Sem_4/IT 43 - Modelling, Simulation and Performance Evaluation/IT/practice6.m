% To solve dx/dt=-xt with Euler method
clear all;
% Clear all variables
nn=10;
% Number of time steps
h=0.1;
% Time step
x(1)=1;
% Initial x
t(1)=0;
% Initial t
for n=1:nn-1
% Time loop
fun=-x(n)*t(n);
% Get RHS at old time
x(n+1)=x(n)+h*fun;
% Get new x
t(n+1)=t(n)+h;
% get new t
end
plot(t,x)
% Plot x vs t
title('CFB'); xlabel('t'), ylabel('x')