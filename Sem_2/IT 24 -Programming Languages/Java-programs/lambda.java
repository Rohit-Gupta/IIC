import java.util.Scanner;
interface NumericTest 
{
	boolean test(int n);
}
class lambda 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		int p;
		System.out.println("\nLambda Function for Even-Odd ");
		NumericTest isEven = (n) -> (n % 2)==0;
		System.out.println("\nEnter no. to test");
		p=sc.nextInt();
		if(isEven.test(p)) 
			System.out.println(p+" is even");
	    	if(!isEven.test(p)) 
			System.out.println(p+" is odd");
    		System.out.println("\nAnonymous class implementation for Even-Odd ");
    		NumericTest anonIsEven = new NumericTest()
		{
    			public boolean test(int n)
		    	{
    				if(0==n%2)
    					return true;
    				else
    					return false;
			}
    		};
   		System.out.println("\nEnter no. to test");
    		p=sc.nextInt();
    		if (anonIsEven.test(p))
    			System.out.println(p+" is even");
    		if (!anonIsEven.test(p))
    			System.out.println(p+" is odd");
    		System.out.println("Lambda Function for Non-Negative ");	
    		NumericTest isNonNeg = (n) -> n >= 0;
    		System.out.println("\nEnter no. to test");
    		p=sc.nextInt();
    		if(isNonNeg.test(p)) 
			System.out.println(p+" is non-negative");
    		if(!isNonNeg.test(p)) 
			System.out.println(p+" is negative");
    		System.out.println("\nAnonymous Class Implementation For Non-Negative");
    		NumericTest anonPosNeg = new NumericTest()
		{
	    		public boolean test(int n)
    			{
    				if(0<=n)
					return true;
    				else
    				return false;
			}
	    	};
    		System.out.println("\nEnter no. to test");
    		p=sc.nextInt();
    		if (anonPosNeg.test(p))
    			System.out.println(p+" is Non-Negative");
    		if (!anonPosNeg.test(p))
    			System.out.println(p+" is Negative");
    		sc.close();
	}
}
