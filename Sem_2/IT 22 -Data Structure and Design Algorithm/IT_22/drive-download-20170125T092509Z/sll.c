#include<stdio.h>
#include<stdlib.h>

typedef struct node
{
  int data;
  struct node *next;
}node;

void display(node *);
void insert_beg(node **,int);
void insert_end(node **,int);
void insert_p(node **,int);
void delete_beg(node **);
void delete_end(node **);
void delete_p(node **,int);
void freelist(node **);

int main()
{
  node *head=NULL;
  int ch,item;
  while(1)
  {
	printf("*****MENU*****\n1.Insert at beg\n2.Insert at end\n3.Insert at proper place\n4.Delete from beg\n5.Delete from end\n6.Delete particular\n7.Display the list\n8.Free the list\n9.Exit\n");
	scanf("%d",&ch);
	switch(ch)
	{
		case 1:
		printf("Enter the item to be added : ");
		scanf("%d",&item);
		insert_beg(&head,item);
		break;
		case 2:
		printf("Enter the item to be added : ");
		scanf("%d",&item);
		insert_end(&head,item);
		break;
		case 3:
		printf("Enter the item to be added : ");
		scanf("%d",&item);
		insert_p(&head,item);
		break;
		case 4:
		delete_beg(&head);
		break;
		case 5:
		delete_end(&head);
		break;
		case 6:
		printf("Enter the item to be deleted : ");
		scanf("%d",&item);
		delete_p(&head,item);
		break;
		case 7:
		display(head);
		break;
		case 8:
		freelist(&head);
		break;
		case 9:
		exit(0);
		default:
		printf("Wrong option\n");
		break;
	}
  }
  return 0;
}

void display(node *head)
{
  if(NULL==head)
  {
	printf("List empty\n");
	return ;
  }
  node *p=head;
  printf("The list is :\n");
  while(p!=NULL)
  {
	printf("-> %d ",p->data);
	p=p->next;
  }
  printf("\n");
}

void insert_beg(node **head,int item)
{
  node *new=(node *)malloc(sizeof(node));
  if(NULL==new)
  {
	printf("Node not created, memory not available\n");
	return ;
  }
  new->data=item;
  if(NULL==(*head))
  {
	new->next=NULL;
  }
  else
  {
	new->next=(*head);
  }
  (*head)=new;
  printf("Item added successfully at beg\n");
}

void insert_end(node **head,int item)
{
  node *new=(node *)malloc(sizeof(node));
  if(NULL==new)
  {
        printf("Node not created, memory not available\n");
        return ;
  }
  new->data=item;
  new->next=NULL;
  if(NULL==(*head))
  {
	(*head)=new;
  }
  else
  {
	node *p=(*head);
	while(p->next!=NULL)
	p=p->next;
	p->next=new;
  }
  printf("Item added successfully at end\n");
}

void insert_p(node **head,int item)
{
  node *new=(node *)malloc(sizeof(node));
  if(NULL==new)
  {
        printf("Node not created, memory not available\n");
        return ;
  }
  new->data=item;
  if(NULL==(*head))
  {
	new->next=NULL;
	(*head)=new;
	printf("Item added successfully at proper place\n");
	return ;
  }
  if(item<=(*head)->data)
  {
	insert_beg(head,item);
	return ;
  }
  node *n=(*head);
  while(n->next!=NULL)
  {
	n=n->next;
  }
  if(item>=n->data)
  {
        insert_end(head,item);
        return ;
  }
  node *p=(*head)->next , *q=(*head);
  while(p->data<=item&&p->next!=NULL)
  {
	q=p;
	p=p->next;
  }
  new->next=p;
  q->next=new;
  printf("Item added successfully at proper place\n");
}

void delete_beg(node **head)
{
  int i;
  if(NULL==(*head))
  {
	printf("Cant delete, list empty\n");
	return;
  }
  if((*head)->next==NULL)
  {
	i=(*head)->data;
	free(*head);
	(*head)=NULL;
	printf("%d deleted successfully from beg\n",i);
	return;
  }
  node *p=(*head);
  (*head)=(*head)->next;
  i=p->data;
  free(p);
  p=NULL;
  printf("%d deleted successfully from beg\n",i);
}

void delete_end(node **head)
{
  int i;
  if(NULL==(*head))
  {
	printf("Cant delete, list empty\n");
	return;
  }
  if((*head)->next==NULL)
  {
	i=(*head)->data;
	free(*head);
	(*head)=NULL;
	printf("%d deleted successfully from end\n",i);
	return;
  }
  node *p=(*head)->next , *q=(*head);
  while(p->next!=NULL)
  {
	q=p;
	p=p->next;
  }
  q->next=NULL;
  i=p->data;
  free(p);
  p=NULL;
  printf("%d deleted successfully from end\n",i);
}

void delete_p(node **head,int item)
{
  if(NULL==(*head))
  {
	printf("Cant delete, list empty\n");
	return;
  }
  if((*head)->data==item)
  {
	delete_beg(head);
	return ;
  }
  node *n=(*head);
  while(n->next!=NULL)
  {
	n=n->next;
  }
  if(n->data==item)
  {
        delete_end(head);
        return ;
  }
  node *p=(*head)->next , *q=(*head);
  while(p->next!=NULL)
  {
	if(p->data==item)
	{
		q->next=p->next;
		p->next=NULL;
		free(p);
		p=NULL;
		printf("%d deleted successfully\n",item);
		return;
	}
	q=p;
	p=p->next;
  }
  printf("Item not found\n");
  return;
}

void freelist(node **head)
{
  if(NULL==(*head))
  {
	printf("List already empty\n");
	return ;
  }
  if(NULL==(*head)->next)
  {
	free(*head);
	*head=NULL;
	printf("List freed\n");
	return ;
  }
  node *p=(*head);
  while(p!=NULL)
  {
	(*head)=(*head)->next;
	free(p);
	p=NULL;
	p=(*head);
  }
  printf("List freed\n");
}
