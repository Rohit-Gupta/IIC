clc;
clear;
clf;

N= 10000;
a = 0.85 ;
k = [0:10]; 
Poisson = zeros(size(k)) ;
for m = k
Poisson(m + 1) = a.^m * exp(-a)./factorial(m);
end
queue(1) = 0;
for n = 1:N
x = rand(1);
arrivals = sum(x > cumsum(Poisson));
departures = queue(n) > 0;
queue(n+1) = queue(n) + arrivals - departures;
end
mean_queue_length=sum(queue)/length(queue) 
bins =[0:25];
y = hist(queue,bins);
PMF = y/N;
bar(bins,PMF)
axis ([min(bins)-1 max(bins)+1 0 1.1*max(PMF)])