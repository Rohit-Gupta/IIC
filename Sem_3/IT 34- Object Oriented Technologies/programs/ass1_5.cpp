#include<iostream>
using namespace std;

class resource
{
	int code;
	static int count;
	public:
	void setdata()
	{
		code=++count;
	}
	void display()
	{
		cout<<" Object number :"<<code<<endl;
	}
	static void showcount(void)
	{
		cout<<" Count:"<<count<<endl;
	}
};

int resource::count;

int main()
{
	resource r1,r2;
	r1.setdata();
	r2.setdata();
	resource::showcount();
	
	resource r3,r4,r5;
	r3.setdata();
	r4.setdata();
	r5.setdata();
	r5.setdata();
	resource::showcount();

	r1.display();
	r2.display();
	r3.display();
	r4.display();
	r5.display();
	return 0;
}
